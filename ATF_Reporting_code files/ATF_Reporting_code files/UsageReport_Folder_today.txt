//#Project Name: ATF- Selenium Framework
// #Author: Krutika Lingarkar


package Reporting;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.openqa.selenium.support.ui.Select;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class UsageReport_Folder_today {

	/**
	 * @param args
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws BiffException 
	 */
	public static void main(String[] args) throws InterruptedException, AWTException, BiffException, IOException {
		// TODO Auto-generated method stub
		
		// firefox browser for downloding
		
		
		
		// to get input data from excel sheet
		
				String FilePath = "C:\\Users\\A589740\\Desktop\\Te.xls";
				
				FileInputStream fs = new FileInputStream(FilePath); 
				
				Workbook wb = Workbook.getWorkbook(fs);
				
				Sheet sh = wb.getSheet(0);
				
				String URL = sh.getCell(2,1).getContents();
				
				System.out.println(URL);

				String AdminLogin = sh.getCell(0,1).getContents();
				
				System.out.println(AdminLogin);
				
				String AdminPass = sh.getCell(1,1).getContents();
				
				System.out.println(AdminPass);
				
				String check = sh.getCell(3,6).getContents();
				
				System.out.println(check);
				
				String dwpath = sh.getCell(4,7).getContents();
				
				System.out.println(dwpath);
				
				FirefoxProfile profile = new FirefoxProfile(); 
				
				profile.setPreference("browser.download.dir", dwpath); 
				
				profile.setPreference("browser.download.folderList", 2);
				
				WebDriver driver = new FirefoxDriver(profile);
		
		driver.get(URL);
		
		driver.manage().window().maximize();
		
		driver.switchTo().frame(1);
		
	    // administrator login
		
		driver.findElement(By.xpath("//*[@id='Email']")).sendKeys(AdminLogin);
		
		driver.findElement(By.xpath("//*[@id='Password']")).sendKeys(AdminPass);
		
		driver.findElement(By.xpath("//*[@id='ctl00_ContentHolder_LoginButton']")).click();
		
		System.out.println(driver.findElement(By.xpath("//*[@id='ctl00_ContentHolderWrapper']/div[1]/div[2]/div/div[2]/div[1]/div[1]/h1")).getText());
			
		// click on admin tab
		
		driver.findElement(By.xpath("//*[@id='ctl00_linkAccountAdmin']/a")).click();
		
		System.out.println(driver.findElement(By.xpath("//*[@id='ctl00_ContentHolderWrapper']/h1")).getText());
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[@id='ctl00_SidebarContent_SideMenu_AdminUL']/li[9]/a/span[2]")).click();
		
		System.out.println(driver.findElement(By.xpath("//*[@id='ctl00_ContentHolder_pageTitle']")).getText());
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		// generate an usage report on folder for today
		
		// step 1
		
		driver.findElement(By.xpath("//*[@id='ctl00_ContentHolder_divDetails']/div/div/a/span[2]")).click();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		new Select(driver.findElement(By.id("ddReportType"))).selectByVisibleText("Usage Report");

		driver.findElement(By.xpath("//*[@id='btnNextStep0']")).click();
		
		System.out.println("Report Info");
		
		// step 2
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[@id='ctl00_FormBottomContent_txtTitle']")).sendKeys(" Folder today");
		
		new Select(driver.findElement(By.id("ddObjectType"))).selectByVisibleText("Folder");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[@id='foc2f0aa-b5bd-4d45-8165-17ace133af2d']")).click();
	
		new Select(driver.findElement(By.id("ctl00_FormBottomContent_ddDateOption"))).selectByVisibleText("Today");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[@id='btnNextStep']")).click();
		
		System.out.println("scheduling");
		
		
		// step 3
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[@id='ctl00_FormBottomContent_btnCreateReport']")).click();
		
		System.out.println(driver.findElement(By.xpath("//*[@id='ctl00_ContentHolder_pageTitle']")).getText());
		
		System.out.println("Report generated");
		
		// to see the report
		
					Thread.sleep(7000);

					driver.findElement(By.xpath("//*[@id='ctl00_ContentHolder_rptReports_ctl00_rptReportRecords_ctl00_ViewReports']/a[1]")).click();
					
					 for(String winHandle : driver.getWindowHandles())
			            {
			               
			            	driver.switchTo().window(winHandle);
			            }
			            
			            Thread.sleep(9000L);

						System.out.println(driver.getCurrentUrl());
						
						int aCount = driver.findElements(By.xpath("//tbody/tr")).size();
						
						System.out.println(aCount);
						
						//for loop - 
						
						Thread.sleep(5000L);
						
						for(int y=1; y <=aCount; y++)
							
						{
							
						String text = driver.findElement(By.xpath("//*[@id='dgData']/tbody/tr["+y+"]/td[4]")).getText();
							
							if (text.equals(check))
							{
								System.out.println("generation successful");
							}
						}
						
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						
						driver.findElement(By.xpath("//*[@id='btnPdf']")).click();
						
						Thread.sleep(10000); 
					
						

                        Robot robot = new Robot();

                        robot.keyPress(KeyEvent.VK_ENTER);

                        robot.keyRelease(KeyEvent.VK_ENTER); 
						
					
	}

}
