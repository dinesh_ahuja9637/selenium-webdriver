/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : TestBase Class
 Date : 26/02/2016
 Test Case Name : NA
 **************************************************************
 */
package tca.com.canopy.base;

import java.io.FileInputStream;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Number;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import tca.com.canopy.util.ErrorUtil;
import tca.com.canopy.util.TestLog;
import tca.com.canopy.util.xls_reader;

public class TestBase {

	public  WebDriver driver =null;
	public static Properties OR=null;
	public static boolean isInitalized=false;
	public static xls_reader test_data_xls=new xls_reader(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls");
	public static boolean isBrowserOpened=false;
	public static boolean isURLOpened=false;
	public static int machine_id = 0 ;
	public static int row = 0;
	public static int columns=0;
	public String time;
	public String screenshotPath;

	public void openBrowser()
	{
		if(!isBrowserOpened)
		{
			if(fn_getproperty("browserType").equals("MOZILLA"))
				driver = new FirefoxDriver();
			else if (fn_getproperty("browserType").equals("IE"))
				driver = new InternetExplorerDriver();
			else if (fn_getproperty("browserType").equals("CHROME"))
			{
				System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\chromedriver.exe" );
				driver = new ChromeDriver();
			}

			isBrowserOpened=true;
			String waitTime=fn_getproperty("default_implicitWait");
			driver.manage().timeouts().implicitlyWait(Long.parseLong(waitTime), TimeUnit.SECONDS);
			driver.manage().window().maximize();
		}

	}

	// close browser
	public void closeBrowser()
	{
		driver.quit();
		isBrowserOpened=false;
	}

	public static String fn_getproperty(String propertyname)
	{
		Properties prop = new Properties();
		InputStream input = null;
		try 
		{
			input = new FileInputStream(System.getProperty("user.dir")+"//src//tca//com//canopy//config//config.properties");
			// load a properties file
			prop.load(input);
		} 

		catch (IOException ex) 
		{
			ex.printStackTrace();
		} 

		finally 
		{
			if (input != null) 
			{
				try 
				{
					input.close();
				}

				catch (IOException e) 
				{
					e.printStackTrace();
				}

			}
		}

		//Return the property value
		return prop.getProperty(propertyname);
	}


	public static String or_getproperty(String propertyname)
	{
		Properties prop = new Properties();
		InputStream input = null;

		try 
		{
			input = new FileInputStream(System.getProperty("user.dir")+"//src//tca//com//canopy//config//OR.properties");
			// load a properties file
			prop.load(input);
		}

		catch (IOException ex) 
		{
			ex.printStackTrace();
		} 

		finally 
		{
			if (input != null)
			{
				try 
				{
					input.close();
				}

				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}

		//Return the property value
		return prop.getProperty(propertyname);
	}

	public void initialize() throws Exception
	{
		if(!isInitalized)
		{
			test_data_xls = new xls_reader(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls");
			isInitalized=true;
		}
	}

	public void writeVmDetails(double req_id,String template_name) throws IOException, RowsExceededException, WriteException, BiffException
	{
		Workbook workbook1 = Workbook.getWorkbook(new File(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls"));
		WritableWorkbook copy = Workbook.createWorkbook(new File(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls"), workbook1);
		WritableSheet sheet2 = copy.getSheet(template_name) ;
		int excelRows = 1 , excelCols = 14;
		WritableCell cell = sheet2.getWritableCell(excelRows,excelCols);
		Number number1 = new Number(excelCols, excelRows, req_id);
		sheet2.addCell(number1);
		copy.write();
		copy.close();
	}

	public  WebElement getObject(String xpathKey)
	{
		try
		{
			new FluentWait<WebDriver>(driver)
			.withTimeout(900, TimeUnit.SECONDS)
			.pollingEvery(1, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(or_getproperty(xpathKey))));
			WebElement x = driver.findElement(By.xpath(or_getproperty(xpathKey)));
			return x;
		}

		catch(Throwable t)
		{
			ErrorUtil.addVerificationFailure(t);                   
			return null;
		}
	}

	public static boolean compareText(String expectedVal, String actualValue)
	{
		try
		{
			Assert.assertEquals(actualValue,expectedVal);
		}

		catch(Throwable t)
		{
			ErrorUtil.addVerificationFailure(t);                   
			return false;
		}

		return true;
	}


	public boolean checkElementPresence(String string)
	{
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + string + "')]"));

		try
		{
			Assert.assertTrue(list.size() > 0, "Text not found!");
		}

		catch(Throwable t)
		{
			ErrorUtil.addVerificationFailure(t);                   
			return false;
		}

		return true;
	}

	public  boolean isClickable(WebElement webE)        
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(webE));
			return true;
		}

		catch (Exception e)
		{
			return false;
		}
	}

	public WebElement Dropdown(String idkey)
	{
		WebElement DropDown = driver.findElement(By.id(idkey));
		return DropDown;
	}

	public boolean openURL() 
	{
		driver.get(TestBase.fn_getproperty("TCA"));

		try 
		{
			TestLog.info("URL Opening");
		}

		catch (Throwable t) 
		{
			TestLog.info("URL NOt Opening");
			return false;
		}

		isURLOpened=true;
		return isURLOpened;
	}

	public String CaptureScreenshot(String Error) throws IOException

	{

		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss_");

		Calendar cal = Calendar.getInstance();

		time = "" + dateFormat.format(cal.getTime());


		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		// Now you can do whatever you need to do with it, for example copy somewhere

		screenshotPath = System.getProperty("user.dir")+"\\src\\Screenshots\\"+Error+"_"+time+".png";


		FileUtils.copyFile(scrFile, new File(screenshotPath));

		return screenshotPath;
	}
	

	public boolean retryingFindClick(By by) {
		boolean result = false;
		int attempts = 0;
		while(attempts < 2) {
			try {
				driver.findElement(by).click();
				result = true;
				break;
			} catch(StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}  
	
	




}
