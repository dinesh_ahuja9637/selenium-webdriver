/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : Base class for verification of all CIS single tenant templates.
 Date : 26/02/2016
 Test Case Name : vmVerification
 **************************************************************
 */

package tca.com.canopy.itemverify;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;

import org.testng.annotations.AfterSuite;

import org.testng.annotations.BeforeSuite;

import org.testng.asserts.SoftAssert;
import tca.com.canopy.base.TestBase;

import tca.com.canopy.util.ExcelReadAndWrite;
import tca.com.canopy.util.TestLog;
import tca.com.canopy.util.TestUtil;

public class itemVerification extends TestBase {
	public String vmName;
	public String machinesRequested;
	public String cpuRequested;
	public String leaseDurationRequested;
	public String memoryRequested;
	public String storageRequested;
	public String vmRequester;
	public String vmDescriptionRequester;
	public boolean result;
	public String systemTime;
	public int excelRowCount=1;
	public String excelScreenshotPath=null;
	public static SoftAssert softassert = new SoftAssert();

	@BeforeSuite
	public void checkSuiteSkip() throws Exception
	{
		DOMConfigurator.configure("log4j.xml");

		if(!TestUtil.isSuiteExecutable("CIS_Template_Verification"))
		{
			TestLog.info("Skipping the execution of CIS_Template_Verification Suite as RunMode is Set to No");
			throw new SkipException("Runmode of CIS_Template_Verification suite set to no. So Skipping all tests in CIS_Template_Verification suite");
		}




	}

	@AfterSuite
	public void generateBackupReport() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls");
	}

	public void vmVerification(
			String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,


			String MaintainenaceBackupSchedule, 


			String backupDate,
			String ReconfigureStorage,

			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, RowsExceededException, WriteException, BiffException, IOException{

		result=false;
		excelScreenshotPath=null;
		TestLog.startTestCase(SRS);
		if(RequestID.trim().isEmpty())
		{
			Assert.assertTrue(false,"Request ID is not Present in Excel for the current template");

		}
		openBrowser();
		driver.get(TestBase.fn_getproperty("TCA"));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(6000L);
		driver.findElement(By.id("username")).sendKeys(TestBase.fn_getproperty("requestor_username"));//--
		driver.findElement(By.id("password")).sendKeys(TestBase.fn_getproperty("requestor_passwd"));//--
		Thread.sleep(6000L);
		driver.findElement(By.id("submit")).click();
		Thread.sleep(6000L);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement requesterRequestTab = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("request_tab"))));
		requesterRequestTab.click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Thread.sleep(3000L);
		List<WebElement> lstRequestTabFrames = driver.findElements(By.tagName("iframe"));
		driver.switchTo().frame(lstRequestTabFrames.get(lstRequestTabFrames.size()-1));
		Thread.sleep(5000L);
		String txtRequestTabTotalPages = driver.findElement(By.xpath(TestBase.or_getproperty("total_pages_requests"))).getText();
		String[] requestTabTotalPagesParts = txtRequestTabTotalPages.split(" ");
		String totalPagesSecontPart = requestTabTotalPagesParts[1];
		int requestTabTotalPages = Integer.parseInt(totalPagesSecontPart);
		outer:for(int j=1;j<=requestTabTotalPages;j++)
		{
			int requestsPageRowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();

			for(int i=1;i<=requestsPageRowCount;i++)
			{
				String requestsPageRequestId = driver.findElement(By.xpath(TestBase.or_getproperty("requestid_drop1")+ i + TestBase.or_getproperty("requestid_drop2"))).getText();
				double req_id = Integer.parseInt(requestsPageRequestId);
				if(requestsPageRequestId.equals(RequestID))  //req id which we need to read from excel
				{

					String requestTabRequestStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ i +TestBase.or_getproperty("status_drop2")))).getText();
					TestLog.info("Request Status is:"+requestTabRequestStatus);
					if(requestTabRequestStatus.equals("Successful"))
					{ //vmstatus
						TestLog.info("Request Submitted Succesfully");
						WebElement weRequestPageRequestId = driver.findElement(By.xpath(TestBase.or_getproperty("request_drop4")+ i + TestBase.or_getproperty("request_drop5")));
						weRequestPageRequestId.click();
						weRequestPageRequestId.findElement(By.xpath(TestBase.or_getproperty("requestid_drop1")+ i + TestBase.or_getproperty("requestid_drop3"))).click();
						Thread.sleep(10000L);
						driver.switchTo().frame("__gadget_2");
						driver.switchTo().frame("innerFrame");
						Thread.sleep(5000L);
						WebElement weMachinesRequested = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(TestBase.or_getproperty("testdata_machines")))));
						machinesRequested = weMachinesRequested.getAttribute("value");
						if(!SRS.contains("2008"))
						{
							WebElement weLeaseDurationRequested = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(TestBase.or_getproperty("testdata_leaseDays")))));
							leaseDurationRequested =weLeaseDurationRequested.getAttribute("value");
						}
						WebElement weCpuRequested = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(TestBase.or_getproperty("testdata_cpu")))));
						cpuRequested =weCpuRequested.getAttribute("value");
						WebElement weMemoryRequested = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(TestBase.or_getproperty("testdata_memory")))));
						memoryRequested=weMemoryRequested.getAttribute("value");
						WebElement weStorageRequested = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(TestBase.or_getproperty("testdata_storage")))));
						storageRequested = weStorageRequested.getAttribute("value");
						WebElement weRequester = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(TestBase.or_getproperty("testdata_owner")))));
						vmRequester =weRequester.getAttribute("value");
						WebElement weVmDescriptionRequester = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(TestBase.or_getproperty("testdata_descr")))));
						vmDescriptionRequester =weVmDescriptionRequester.getAttribute("value");
						driver.switchTo().defaultContent();
						Thread.sleep(3000L);
						List<WebElement> lstRequestPageFrames = driver.findElements(By.tagName("iframe"));
						driver.switchTo().frame(lstRequestPageFrames.get(lstRequestPageFrames.size()-1));//--6-2
						Thread.sleep(3000L);
						String requestPageTemplateStatusDetail = driver.findElement(By.xpath(TestBase.or_getproperty("status_detail"))).getText();
						String[] token = requestPageTemplateStatusDetail.split(" ");
						vmName= token[3];
						vmName.trim();

						if (vmName.endsWith("."))
							vmName= vmName.substring(0,vmName.length()-1);

						TestLog.info("Machine Name is:"+vmName);
						try 
						{
							Workbook workbook1 = Workbook.getWorkbook(new File("src\\tca\\com\\canopy\\xls\\TestData.xls"));
							WritableWorkbook copy = Workbook.createWorkbook(new File("src\\tca\\com\\canopy\\xls\\TestData.xls"), workbook1);
							WritableSheet sheet2;
							int row = 1 , column = 15;
							WritableCell cell;
							Label mn1;
							Label mn2;

							switch (SRS) 
							{
							case "Windows Server 2008 DE R2 64 bit":
								

								sheet2 = copy.getSheet("Windows_Server_2008_DE_R2_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
							    
								copy.write();

								
								copy.close();
								
								/*
								sheet2 = copy.getSheet("Suite"); 
								cell = sheet2.getWritableCell(2,11); 
								mn1 = new Label(column, row, vmName); 
								sheet2.addCell(mn1); 
								copy.write();
								copy.close();
								*/
								
								
								result=true;

								break;

							case "Windows Server 2008 DE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_DE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "Windows Server 2008 EE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_EE_R2_64");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "Windows Server 2008 EE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_EE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;
								break;

							case "Windows Server 2008 SE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_SE_R2_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "Windows Server 2008 SE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_SE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "Windows Server 2012 DE 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_DE_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "Windows Server 2012 SE 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_SE_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "Windows Server 2012 DE R2 U1 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_DE_R2_U1_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "Windows Server 2012 SE R2 U1 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_SE_R2_U1_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "Red Hat Enterprise Linux 6.x 64 bit":
								sheet2 = copy.getSheet("Red_Hat_Enterprise_Linux");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;
								
							case "Red Hat Enterprise Linux 6.x 32 bit":
								sheet2 = copy.getSheet("Red_Hat_Enterprise_Linux_32");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "SuSE Linux Enterprise Server 11.x 64-bit":
								sheet2 = copy.getSheet("SuSE_Linux_Enterprise_Server"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "Y");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;


							}
						}
						catch(IOException e)
						{
							TestLog.info("Worksheet Creation Failed");
							e.printStackTrace();
						}
						break outer;
					}
					else if(requestTabRequestStatus.equals("In Progress"))
					{
						TestLog.info("Request is still in progress");
						excelScreenshotPath=CaptureScreenshot ("Request In Progress_");
						closeBrowser();
						try 
						{
							Workbook workbook1 = Workbook.getWorkbook(new File("src\\tca\\com\\canopy\\xls\\TestData.xls"));
							WritableWorkbook copy = Workbook.createWorkbook(new File("src\\tca\\com\\canopy\\xls\\TestData.xls"), workbook1);

							WritableSheet sheet2;
							int row = 1 , column = 15;
							WritableCell cell;
							Label mn1;
							Label mn2;

							switch (SRS) 
							{
							case "Windows Server 2008 DE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_DE_R2_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 DE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_DE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 EE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_EE_R2_64");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 EE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_EE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;
								break;

							case "Windows Server 2008 SE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_SE_R2_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 SE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_SE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 DE 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_DE_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 SE 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_SE_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 DE R2 U1 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_DE_R2_U1_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 SE R2 U1 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_SE_R2_U1_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Red Hat Enterprise Linux 6.x 64 bit":
								sheet2 = copy.getSheet("Red_Hat_Enterprise_Linux");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Red Hat Enterprise Linux 6.x 32 bit":
								sheet2 = copy.getSheet("Red_Hat_Enterprise_Linux_32");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;
							case "SuSE Linux Enterprise Server 11.x 64-bit":
								sheet2 = copy.getSheet("SuSE_Linux_Enterprise_Server"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;


							}

						}
						catch(IOException e)
						{
							TestLog.info("Worksheet Creation Failed");
							e.printStackTrace();
						}
						Assert.assertTrue(false,"Request is not in successful state. Cannot move ahead");
					}

					else if(requestTabRequestStatus.equals("Unsubmitted"))
					{
						TestLog.info("Request is not submitted succesfully");
						excelScreenshotPath = CaptureScreenshot ("Request Unsubmitted_");
						closeBrowser();
						try 
						{
							Workbook workbook1 = Workbook.getWorkbook(new File("src\\tca\\com\\canopy\\xls\\TestData.xls"));
							WritableWorkbook copy = Workbook.createWorkbook(new File("src\\tca\\com\\canopy\\xls\\TestData.xls"), workbook1);

							WritableSheet sheet2;
							int row = 1 , column = 15;
							WritableCell cell;
							Label mn1;
							Label mn2;

							switch (SRS) 
							{
							case "Windows Server 2008 DE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_DE_R2_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 DE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_DE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 EE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_EE_R2_64");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 EE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_EE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;
								break;

							case "Windows Server 2008 SE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_SE_R2_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 SE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_SE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 DE 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_DE_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 SE 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_SE_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 DE R2 U1 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_DE_R2_U1_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 SE R2 U1 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_SE_R2_U1_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Red Hat Enterprise Linux 6.x 64 bit":
								sheet2 = copy.getSheet("Red_Hat_Enterprise_Linux");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;
								
							case "Red Hat Enterprise Linux 6.x 32 bit":
								sheet2 = copy.getSheet("Red_Hat_Enterprise_Linux_32");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "SuSE Linux Enterprise Server 11.x 64-bit":
								sheet2 = copy.getSheet("SuSE_Linux_Enterprise_Server"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;


							}
						}
						catch(IOException e)
						{
							TestLog.info("Worksheet Creation Failed");
							e.printStackTrace();
						}
						Assert.assertTrue(false,"Request is not in successful state. Cannot move ahead");
					}

					else if(requestTabRequestStatus.equals("Pending Approval"))
					{
						TestLog.info("Request is pending for approval");
						excelScreenshotPath= CaptureScreenshot ("Request Pending Approval_");
						closeBrowser();
						try
						{
							Workbook workbook1 = Workbook.getWorkbook(new File("src\\tca\\com\\canopy\\xls\\TestData.xls"));
							WritableWorkbook copy = Workbook.createWorkbook(new File("src\\tca\\com\\canopy\\xls\\TestData.xls"), workbook1);

							WritableSheet sheet2;
							int row = 1 , column = 15;
							WritableCell cell;
							Label mn1;
							Label mn2;

							switch (SRS) 
							{
							case "Windows Server 2008 DE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_DE_R2_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 DE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_DE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 EE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_EE_R2_64");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 EE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_EE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;
								break;

							case "Windows Server 2008 SE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_SE_R2_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 SE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_SE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 DE 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_DE_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 SE 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_SE_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 DE R2 U1 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_DE_R2_U1_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 SE R2 U1 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_SE_R2_U1_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Red Hat Enterprise Linux 6.x 64 bit":
								sheet2 = copy.getSheet("Red_Hat_Enterprise_Linux");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;
								
							case "Red Hat Enterprise Linux 6.x 32 bit":
								sheet2 = copy.getSheet("Red_Hat_Enterprise_Linux_32");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "SuSE Linux Enterprise Server 11.x 64-bit":
								sheet2 = copy.getSheet("SuSE_Linux_Enterprise_Server"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;


							}

						}
						catch(IOException e)
						{
							TestLog.info("Worksheet Creation Failed");
							e.printStackTrace();
						}
						Assert.assertTrue(false,"Request is not in successful state. Cannot move ahead");
					}

					else if(requestTabRequestStatus.equals("Failed"))
					{
						TestLog.info("Request is failed to submit");
						/**********************
						 
						 */
						
						WebElement weRequestPageRequestId = driver.findElement(By.xpath(TestBase.or_getproperty("request_drop4")+ i + TestBase.or_getproperty("request_drop5")));
						weRequestPageRequestId.click();
						weRequestPageRequestId.findElement(By.xpath(TestBase.or_getproperty("requestid_drop1")+ i + TestBase.or_getproperty("requestid_drop3"))).click();
						Thread.sleep(10000L);
						driver.switchTo().frame("__gadget_2");
						driver.switchTo().frame("innerFrame");
						Thread.sleep(5000L);
						WebElement weMachinesRequested = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(TestBase.or_getproperty("testdata_machines")))));
						machinesRequested = weMachinesRequested.getAttribute("value");
						
						driver.switchTo().defaultContent();
						Thread.sleep(3000L);
						List<WebElement> lstRequestPageFrames = driver.findElements(By.tagName("iframe"));
						driver.switchTo().frame(lstRequestPageFrames.get(lstRequestPageFrames.size()-1));//--6-2
						Thread.sleep(3000L);
						String requestPageTemplateStatusDetail = driver.findElement(By.xpath(TestBase.or_getproperty("status_detail"))).getText();
						String[] token = requestPageTemplateStatusDetail.split(" ");
						vmName= token[2];
						vmName.trim();

						if (vmName.endsWith("."))
							vmName= vmName.substring(0,vmName.length()-1);

						TestLog.info("Machine Name is:"+vmName);
						
					
						/*  */
						excelScreenshotPath=CaptureScreenshot ("Request Failed"); 
						closeBrowser();
						try 
						{
							Workbook workbook1 = Workbook.getWorkbook(new File("src\\tca\\com\\canopy\\xls\\TestData.xls"));
							WritableWorkbook copy = Workbook.createWorkbook(new File("src\\tca\\com\\canopy\\xls\\TestData.xls"), workbook1);

							WritableSheet sheet2;
							int row = 1 , column = 15;
							WritableCell cell;
							Label mn1;
							Label mn2;

							switch (SRS) 
							{
							case "Windows Server 2008 DE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_DE_R2_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 DE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_DE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 EE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_EE_R2_64");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 EE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_EE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;
								break;

							case "Windows Server 2008 SE R2 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_SE_R2_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2008 SE SP2 32 bit":
								sheet2 = copy.getSheet("Windows_Server_2008_SE_SP2_32"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 DE 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_DE_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 SE 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_SE_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 DE R2 U1 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_DE_R2_U1_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Windows Server 2012 SE R2 U1 64 bit":
								sheet2 = copy.getSheet("Windows_Server_2012_SE_R2_U1_64"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;

							case "Red Hat Enterprise Linux 6.x 64 bit":
								sheet2 = copy.getSheet("Red_Hat_Enterprise_Linux");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;
								
							case "Red Hat Enterprise Linux 6.x 32 bit":
								sheet2 = copy.getSheet("Red_Hat_Enterprise_Linux_32");
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=true;

								break;

							case "SuSE Linux Enterprise Server 11.x 64-bit":
								sheet2 = copy.getSheet("SuSE_Linux_Enterprise_Server"); 
								row= sheet2.findCell(requestsPageRequestId).getRow();
								cell = sheet2.getWritableCell(row,column); 
								mn1 = new Label(column, row, vmName); 
								mn2 = new Label(column+1, row, "N");
								sheet2.addCell(mn1); 
								sheet2.addCell(mn2); 
								copy.write();
								copy.close();
								result=false;

								break;


							}
						}
						catch(IOException e)
						{
							TestLog.info("Worksheet Creation Failed");
							e.printStackTrace();
						}
						excelScreenshotPath=CaptureScreenshot ("Request Failed"); 
						Assert.assertTrue(false,"Request is not in successful state. Cannot move ahead");
					
					}
				}

				else
				{
					TestLog.info("Request id " +req_id+" is not matching with "+RequestID);
				}
			}
			driver.findElement(By.xpath(TestBase.or_getproperty("next_page_req"))).click();
			Thread.sleep(10000L);
		}


		driver.switchTo().defaultContent();
		Thread.sleep(2000L);
		WebElement weItemsTab = driver.findElement(By.xpath(TestBase.or_getproperty("items")));
		weItemsTab.click();
		Thread.sleep(10000);
		List<WebElement> lstItemsTabFrames = driver.findElements(By.tagName("iframe"));

		for(WebElement el : lstItemsTabFrames){
			TestLog.info("Frame Id :" + el.getAttribute("id"));
			TestLog.info("Frame name :" + el.getAttribute("name"));
		}  
		driver.switchTo().frame("__gadget_7");//--
		Thread.sleep(10000L);
		WebElement machine_tab = driver.findElement(By.xpath(TestBase.or_getproperty("machine_tab")));
		machine_tab.click();//--or property
		Thread.sleep(10000L);
		Thread.sleep(15000L);
		String txtApvInboxTotalPages = driver.findElement(By.xpath(TestBase.or_getproperty("total_pages_items"))).getText();
		String[] totalPagesParts = txtApvInboxTotalPages.split(" ");
		String totalPagesSecondPart = totalPagesParts[1];
		Thread.sleep(15000);
		int apvInboxTotalPages = Integer.parseInt(totalPagesSecondPart);

		Outer:for(int k=1;k<=apvInboxTotalPages;k++)
		{
			int row_count1 = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_itmes"))).size();
			for(int i=1;i<=row_count1;i++)
			{
				String name = driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2"))).getText();
				if(name.equals(vmName))
				{
					TestLog.info("Name is:"+name);
					WebElement descr = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("descr_drop1")+ i + TestBase.or_getproperty("descr_drop2"))));
					descr.click();
					Thread.sleep(3000L);
					WebElement requestedItem = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2"))));   
					requestedItem.click();
					driver.switchTo().frame("__gadget_2");
					Thread.sleep(5000L);
					driver.switchTo().frame("innerFrame");
					Thread.sleep(5000L);
					String itemsTabMachineName = driver.findElement(By.xpath(TestBase.or_getproperty("items_machine_name"))).getText();
					TestLog.info("Machine Name in items tab : "+itemsTabMachineName);
					String itemsTabMachineStatus =driver.findElement(By.xpath(TestBase.or_getproperty("items_status"))).getText();
					TestLog.info("Machine status is : "+itemsTabMachineStatus);
					String itemsTabCpu =driver.findElement(By.xpath(TestBase.or_getproperty("itmes_cpu"))).getAttribute("value");
					TestLog.info("Machine CPU is : "+itemsTabCpu);
					String itemsTabMemory=driver.findElement(By.xpath(TestBase.or_getproperty("itmes_memory"))).getAttribute("value");
					TestLog.info("Machine memory is : "+itemsTabMemory);
					String itemsTabStorage = driver.findElement(By.xpath(TestBase.or_getproperty("itmes_storage"))).getAttribute("value");
					TestLog.info("Machine storage is : "+itemsTabStorage);
					String itemsDescription =driver.findElement(By.xpath(TestBase.or_getproperty("itmes_descr"))).getAttribute("value");
					TestLog.info("Machine description is : "+itemsDescription);
					String itemRequester =driver.findElement(By.xpath(TestBase.or_getproperty("itmes_owner"))).getText();
					String itemBlueprintName =driver.findElement(By.xpath(TestBase.or_getproperty("itmes_bpname"))).getText();
					TestLog.info("BluePrint name is : "+itemBlueprintName);

					if (itemsTabMachineStatus.equalsIgnoreCase("On")) 
					{
						result=true;
						TestLog.info("Machine Status is ON ");
						softassert.assertTrue(true, "Machine Status is ON");
					} 
					else 
					{   
						result=false;

						excelScreenshotPath=CaptureScreenshot ("Machine Status is OFF ");
						TestLog.info("Machine Status is OFF");
						Assert.assertTrue(false, "Machine is not ON****");
					}

					if (cpuRequested.equalsIgnoreCase(itemsTabCpu)) 
					{
						result=true;
						TestLog.info("VM configuration for CPU is matching");
						softassert.assertTrue(true, "VM configuration for CPU is matching");
					} 
					else 
					{
						result=false;

						excelScreenshotPath=CaptureScreenshot ("VM configuration for CPU is not matching");
						TestLog.info("VM configuration for CPU is not matching****");
						Assert.assertTrue(false, "VM configuration for CPU is not matching****");
					}

					if (memoryRequested.equalsIgnoreCase(itemsTabMemory)) 
					{
						result=true;
						TestLog.info("VM configuration for Memory is matching ***");
						softassert.assertTrue(true, "VM configuration for Memory is matching");
					} 
					else 
					{
						result=false;
						TestLog.info("VM configuration for Memory is not matching****");

						excelScreenshotPath= CaptureScreenshot("VM configuration for Memory is not matching");
						Assert.assertTrue(false, "VM configuration for Memory is not matching****");
					}


					if (storageRequested.equalsIgnoreCase(itemsTabStorage)) 
					{
						TestLog.info("VM configuration for Storage is matching");
						result=true;
						softassert.assertTrue(true, "VM configuration for Storage is matching");
					} 
					else 
					{
						TestLog.info("VM configuration for Storage is not matching****");
						result=false;

						excelScreenshotPath=CaptureScreenshot ("VM configuration for Storage is not matching****");
						Assert.assertTrue(false, "VM configuration for Storage is not matching****");
					}

					if (vmDescriptionRequester.equalsIgnoreCase(itemsDescription)) 
					{
						TestLog.info("Request text is matching with text data");
						result=true;
						softassert.assertTrue(true, "Request text is matching with text data");
					} 
					else 
					{
						TestLog.info("Request text is not matching with text data****");
						result=false;

						excelScreenshotPath=CaptureScreenshot ("Request text is not matching with text data****");
						Assert.assertTrue(false, "Request text is not matching with text data****");
					}

					if (vmRequester.equalsIgnoreCase(itemRequester))
					{
						TestLog.info("Requester is same");
						result=true;
						softassert.assertTrue(true, "Requester is same");
					} 
					else
					{
						TestLog.info("Requester is not matching****");
						result=false;

						excelScreenshotPath=CaptureScreenshot ("Requester is not matching");
						Assert.assertTrue(false, "Requester is not matching****");
					}
					break Outer;
				}    
			}

			driver.findElement(By.xpath(TestBase.or_getproperty("next_page_items"))).click();
			Thread.sleep(10000L);

		}


		Thread.sleep(1000L);
		closeBrowser();
		TestLog.endTestCase(SRS);
	}



}


