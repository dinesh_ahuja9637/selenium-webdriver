/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : Verification of Red_Hat_Enterprise_Linux_64_bit template.
 Date : 26/02/2016
 Test Case Name : TC_005
 **************************************************************
 */

package tca.com.canopy.itemverify;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import tca.com.canopy.util.TestLog;
import tca.com.canopy.util.TestUtil;

public class Red_Hat_Enterprise_Linux_7x_64_Bit extends itemVerification{
	boolean skip=false;

	@BeforeTest
	public void isTestExecutable(){
		if(!TestUtil.isExecutable("Red Hat Enterprise Linux 7.x 64 Bit")){
			TestLog.info("Skipping execution of test case as Run mode is set to No");
			skip=true;
			throw new SkipException("Skipping execution of test case as Run mode is set to No");
		}

	}
	@Test(dataProvider="getTestData")
	public void TC_014(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,



			String MaintainenaceBackupSchedule, 


			String backupDate,
			String ReconfigureStorage,

			String ReconfigureCPU,
			String ReconfigureMemory) throws RowsExceededException, BiffException, WriteException, InterruptedException, IOException
			{

		vmVerification( SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,



				MaintainenaceBackupSchedule, 


				backupDate,
				ReconfigureStorage,

				ReconfigureCPU,
				ReconfigureMemory);


			}
	@DataProvider(name="getTestData")
	public Object[][] testData() 
	{
		Object[][] arrayObject = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Red_Hat_Enterprise_Linux_7x_64");
		return arrayObject;                                                                                         
	}
	@AfterMethod(alwaysRun=true)
	public void generateReport()
	{
		if(driver!=null){
			closeBrowser();
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		systemTime=""+ dateFormat.format(cal.getTime());
		excelRowCount++;
		if(!skip)
		{
			if(!result)
			{
				test_data_xls.setCellData("TestCaseResult", "Verification Result", 15, "FAIL");
				test_data_xls.setCellData("TestCaseResult", "Verification Time", 15, systemTime);
				test_data_xls.setCellData("TestCaseResult", "Verification Screenshot", 15, excelScreenshotPath);
				test_data_xls.setCellData("Suite", "Result",  15, "FAIL");
				test_data_xls.setCellData("Suite", "VMName", 15,vmName);
				
			}
			else
			{
				test_data_xls.setCellData("TestCaseResult", "Verification Result", 15, "PASS");
				test_data_xls.setCellData("TestCaseResult", "Verification Time", 15, systemTime);
				test_data_xls.setCellData("Suite", "Result",15 ,"PASS");
				test_data_xls.setCellData("Suite", "VMName", 15,vmName);
			}
		}
		else
		{
			test_data_xls.setCellData("TestCaseResult", "Verification Result", 15, "SKIP");
			test_data_xls.setCellData("TestCaseResult", "Verification Time", 15, systemTime);
			test_data_xls.setCellData("Suite", "Result",  15, "SKIP");
		}
	}
}
