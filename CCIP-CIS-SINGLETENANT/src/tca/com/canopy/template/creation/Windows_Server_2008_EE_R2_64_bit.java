/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : TestCase For Creation of Windows_Server_2008_EE_R2_64_bit template.
 Date : 26/02/2016
 Test Case Name : TC_007
 **************************************************************
 */

package tca.com.canopy.template.creation;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import tca.com.canopy.util.TestLog;
import tca.com.canopy.util.TestUtil;

public class Windows_Server_2008_EE_R2_64_bit extends SingleTenant_CIS_Templates {
	boolean skip= false;

	@BeforeClass
	public void isTestExecutable(){
		if(!TestUtil.isExecutable("Windows Server 2008 EE R2 64 bit")){
			TestLog.info("Skipping execution of test case as Run mode is set to No");
			skip=true;
			throw new SkipException("Skipping execution of test case as Run mode is set to No");
		}

	}


	@Test(dataProvider ="getTestData")
	public void TC_007(
			String SRS,
			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory) throws RowsExceededException, BiffException, WriteException, InterruptedException, IOException
			{

		testCISTenant(  SRS,
				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,
				ReconfigureCPU,
				ReconfigureMemory);
			}


	@AfterMethod(alwaysRun=true)
	public void generateExcelReport()
	{

		if (driver!=null)
		{
			closeBrowser();
		}

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		systemTime = ""+dateFormat.format(cal.getTime());
		excelRowCount++;

		if(!skip)
		{
			if(!result)
			{
				test_data_xls.setCellData("TestCaseResult", "Creation Result", 8, "FAIL");
				test_data_xls.setCellData("TestCaseResult", "Creation Execution Time", 8, systemTime);
				test_data_xls.setCellData("TestCaseResult", "Creation Screenshot", 8, excelScreenshotPath);
			}

			else
			{
				test_data_xls.setCellData("TestCaseResult", "Creation Result", 8, "PASS");
				test_data_xls.setCellData("TestCaseResult", "Creation Execution Time", 8, systemTime);
			}
		}

		else 
		{
			test_data_xls.setCellData("TestCaseResult", "Creation Result", 8, "SKIP");
			test_data_xls.setCellData("TestCaseResult", "Creation Execution Time", 8, systemTime);
		}

	}

	@DataProvider(name="getTestData")
	public Object[][] testData() 
	{
		Object[][] arrayObject = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2008_EE_R2_64");
		return arrayObject;                                                                                         
	}

}

