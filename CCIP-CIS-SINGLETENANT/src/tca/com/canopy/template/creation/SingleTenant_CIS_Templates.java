/* 
==============================================================
 Author : IaaS Test Automation Team
 Purpose : Base class for CIS Single Tenant Templates.
 Date : 26/02/2016
 Test Case Name : SingleTenant_CIS_Templates
 
 #======================================================================
 */


package tca.com.canopy.template.creation;
 



import java.util.concurrent.TimeUnit;
import java.io.IOException;
import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import tca.com.canopy.base.TestBase;
import tca.com.canopy.util.TestLog;
import tca.com.canopy.util.ExcelReadAndWrite; 
import tca.com.canopy.util.TestUtil;
 
public class SingleTenant_CIS_Templates extends TestBase
{
		static boolean result;
        static boolean skip;
        public static String templateRequestId;
        String templateName;
        String invalidLoginMsg;
        String systemTime;
        int excelRowCount =1;
        String excelScreenshotPath;
        @BeforeSuite
    	public void checkSuiteSkip() throws Exception
    	{
    		DOMConfigurator.configure("log4j.xml");
    		
    		if(!TestUtil.isSuiteExecutable("CIS_Template_Creation"))
    		{
    			TestLog.info("Skipping the execution of CIS_Template_Creation Suite as RunMode is Set to No");
    			throw new SkipException("Runmode of CIS_Template_Creation suite set to no. So Skipping all tests in CIS_Template_Creation suite");
    		}
    	
    	}
    	
    	 @AfterSuite
         public void generateBackupReports() throws IOException
         {
         	ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls");
         }
        
        public void testCISTenant(
        		String SRS,
    			String Machines,
                String LeaseDuration,
                String CPU,
                String Memory,
                String Storage,
                String Description,
                String Reason,
                String BackupRetention,
                String BackupSchedule,
                String MaintainanceWindow1,
                String MaintainanceWindow2,
                String VMNetworkprofile,
                String StoragePolicy,
                String RequestID,
                String MachineName,
                String VMCreated,
                String MaintainenaceBackupSchedule,
                String backupDate,
    			String ReconfigureStorage,
    			String ReconfigureCPU,
    			String ReconfigureMemory
            ) throws InterruptedException, BiffException, IOException ,RowsExceededException, WriteException{
                
			result = false;
            TestLog.startTestCase(SRS);
			//Function for Opening browser
            openBrowser();
            driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
            driver.get(TestBase.fn_getproperty("TCA"));
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
            Thread.sleep(15000);
            WebDriverWait wait = new WebDriverWait(driver, 60);
            WebElement requestorUsername= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("req_username"))));
            requestorUsername.sendKeys(TestBase.fn_getproperty("requestor_username"));
            WebElement requestorPassword= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("req_password"))));
            requestorPassword.sendKeys(TestBase.fn_getproperty("requestor_passwd"));
            Thread.sleep(15000);
            driver.findElement(By.id("submit")).click();
            Thread.sleep(3000L); 
          
            Thread.sleep(15000);
            driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            Thread.sleep(15000);
            driver.findElement(By.xpath(TestBase.or_getproperty("catalog"))).click();
            Thread.sleep(15000);
            driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
            Thread.sleep(15000);
            driver.switchTo().defaultContent();
            driver.switchTo().frame("__gadget_2");
            Thread.sleep(15000L);
            driver.findElement(By.id(TestBase.or_getproperty("cis_singleTenant"))).click();
            Thread.sleep(3000L);
            driver.switchTo().defaultContent();
            driver.switchTo().frame("__gadget_2");
            Thread.sleep(15000);
            driver.findElement(By.linkText(SRS)).click();
            Thread.sleep(15000);
            
            // Getting template name for Writing into excel sheet
            templateName=driver.findElement(By.xpath(TestBase.or_getproperty("Write_template_name"))).getText();
            TestLog.info("Template name is***"+templateName);
            Thread.sleep(15000);
            driver.findElement(By.xpath(TestBase.or_getproperty("submit_button_for_request_template"))).click();
            Thread.sleep(10000L);
            driver.switchTo().frame("__gadget_2");
            Thread.sleep(5000);
            ///List<WebElement> totalFrames = driver.findElements(By.tagName("iframe"));  
            driver.switchTo().frame("innerFrame");
            driver.findElement(By.cssSelector(TestBase.or_getproperty("storage"))).click();
            driver.switchTo().defaultContent();
            driver.switchTo().frame("__gadget_2");
            driver.switchTo().frame("__gadget_2");
            Thread.sleep(3000L);
            driver.switchTo().frame("innerFrame");
            Thread.sleep(3000L);
 
            try
            {
				if ( driver.findElement(By.xpath(TestBase.or_getproperty("new_storage"))).isDisplayed())
                {
                    WebElement templateStorage = driver.findElement(By.xpath(TestBase.or_getproperty("new_storage")));
                    templateStorage.sendKeys(Keys.CONTROL + "a");
                    templateStorage.sendKeys(Keys.DELETE);
                    templateStorage.sendKeys(Storage);
                    Thread.sleep(5000L);
                }
            }
             
			catch (Exception e)
			{
				driver.findElement(By.xpath(TestBase.or_getproperty("edit_storage0"))).click();
				WebElement editTemplateStorage = driver.findElement(By.xpath(TestBase.or_getproperty("new_storage")));
				editTemplateStorage.sendKeys(Keys.CONTROL + "a");
				editTemplateStorage.sendKeys(Keys.DELETE);
				editTemplateStorage.sendKeys(Storage);
                Thread.sleep(5000L);
            }     
 
            WebElement storagePolicy = driver.findElement(By.id(TestBase.or_getproperty("storage_policy")));
            ((JavascriptExecutor)driver).executeScript("scroll(0,400)");
            Thread.sleep(2000L);
            storagePolicy.click();
            Thread.sleep(2000L);
            storagePolicy.findElement(By.xpath((TestBase.or_getproperty("storage_policy_drop1")+ StoragePolicy +TestBase.or_getproperty("storage_policy_drop2")))).click();
            driver.findElement(By.xpath(TestBase.or_getproperty("edit_storage1"))).click();
            driver.findElement(By.cssSelector(TestBase.or_getproperty("request_information"))).click();
            Thread.sleep(5000L);
            WebElement machineCount = driver.findElement(By.xpath(TestBase.or_getproperty("machines")));
            machineCount.sendKeys(Keys.CONTROL + "a");
            machineCount.sendKeys(Keys.DELETE);
            driver.findElement(By.xpath(TestBase.or_getproperty("machines"))).sendKeys(Machines);
            Thread.sleep(3000);
 
            //Checking Lease duration condition for Windows 2008 Templates
            if(!SRS.contains("2008"))
            {
				WebElement leaseDuration = driver.findElement(By.xpath(TestBase.or_getproperty("lease_duration")));
				leaseDuration.sendKeys(Keys.CONTROL + "a");
				leaseDuration.sendKeys(Keys.DELETE);
                driver.findElement(By.xpath(TestBase.or_getproperty("lease_duration"))).sendKeys(LeaseDuration);
            }
                
            Thread.sleep(3000);
            WebElement cpuStorage = driver.findElement(By.xpath(TestBase.or_getproperty("cpu")));
            cpuStorage.sendKeys(Keys.CONTROL + "a");
            cpuStorage.sendKeys(Keys.DELETE);
            driver.findElement(By.xpath(TestBase.or_getproperty("cpu"))).sendKeys(CPU);
            Thread.sleep(3000);
            WebElement vmMemory = driver.findElement(By.xpath(TestBase.or_getproperty("memory")));
            vmMemory.sendKeys(Keys.CONTROL + "a");
            vmMemory.sendKeys(Keys.DELETE);
            driver.findElement(By.xpath(TestBase.or_getproperty("memory"))).sendKeys(Memory);
            Thread.sleep(5000);
            Thread.sleep(3000L);
            driver.findElement(By.xpath(TestBase.or_getproperty("description"))); 
            retryingFindClick(By.xpath(TestBase.or_getproperty("description")));
            retryingFindClick(By.xpath(TestBase.or_getproperty("description")));
            Thread.sleep(5000L);
            driver.findElement(By.xpath(TestBase.or_getproperty("description"))).sendKeys(Description);
            Thread.sleep(5000L);   
            driver.findElement(By.id(TestBase.or_getproperty("location")));
            //((JavascriptExecutor)driver).executeScript("scroll(0,400)");
            Thread.sleep(2000L);
            retryingFindClick(By.id(TestBase.or_getproperty("location")));
            Thread.sleep(2000L);
            retryingFindClick(By.xpath(TestBase.or_getproperty("location_drop")));
            WebElement backupRetention = driver.findElement(By.id(TestBase.or_getproperty("backup_ret")));
            //((JavascriptExecutor)driver).executeScript("scroll(0,150)");
            Thread.sleep(5000L);
            retryingFindClick(By.id(TestBase.or_getproperty("backup_ret")));
            Thread.sleep(5000L);
            backupRetention.findElement(By.xpath((TestBase.or_getproperty("backup_ret_drop1")+ BackupRetention +TestBase.or_getproperty("backup_ret_drop2")))).click();
            Thread.sleep(5000L);
            WebElement backupSchedule = driver.findElement(By.id(TestBase.or_getproperty("backup_schedule")));
            //((JavascriptExecutor)driver).executeScript("scroll(0,400)");
            Thread.sleep(2000L);
            retryingFindClick(By.id(TestBase.or_getproperty("backup_schedule")));
            Thread.sleep(2000L);
            backupSchedule.findElement(By.xpath((TestBase.or_getproperty("backup_schedule_drop1")+ BackupSchedule +TestBase.or_getproperty("backup_schedule_drop2")))).click();
            WebElement firstWindow = driver.findElement(By.id(TestBase.or_getproperty("window1")));
            ((JavascriptExecutor)driver).executeScript("scroll(0,400)");
            Thread.sleep(2000L);
            retryingFindClick(By.id(TestBase.or_getproperty("window1")));
            Thread.sleep(2000L);
            firstWindow.findElement(By.xpath((TestBase.or_getproperty("window1_drop1")+ MaintainanceWindow1 +TestBase.or_getproperty("window1_drop2")))).click();
            WebElement secondWindow = driver.findElement(By.id(TestBase.or_getproperty("window2")));
            ((JavascriptExecutor)driver).executeScript("scroll(0,400)");
            Thread.sleep(2000L);
            retryingFindClick(By.id(TestBase.or_getproperty("window2")));
            Thread.sleep(2000L);
            secondWindow.findElement(By.xpath((TestBase.or_getproperty("window2_drop1")+ MaintainanceWindow2 +TestBase.or_getproperty("window2_drop2")))).click();
           //editing code for new template
            if(SRS.contains("Red Hat Enterprise Linux 7.x 64 Bit"))
            {
            	//driver.switchTo().frame("__gadget_6");
            	//driver.navigate().refresh();
            	Thread.sleep(10000);
            	WebElement vmProfile = driver.findElement(By.xpath(TestBase.or_getproperty("vm_profile_tcb")));
                ((JavascriptExecutor)driver).executeScript("scroll(0,400)");
                WebDriverWait driverWait=new WebDriverWait(driver,30);
                driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(TestBase.or_getproperty("vm_profile_tcb"))));
                vmProfile.click();
                Thread.sleep(2000L);
                //retryingFindClick(By.id(TestBase.or_getproperty("vm_profile_tcb")));
                Thread.sleep(2000L);
                vmProfile.findElement(By.xpath((TestBase.or_getproperty("vm_profile_drop1_tcb")+ VMNetworkprofile +TestBase.or_getproperty("vm_profile_drop2_tcb")))).click();
            }
            else
            {
            WebElement vmProfile = driver.findElement(By.id(TestBase.or_getproperty("vm_profile")));
            ((JavascriptExecutor)driver).executeScript("scroll(0,400)");
            Thread.sleep(2000L);
            retryingFindClick(By.id(TestBase.or_getproperty("vm_profile")));
            Thread.sleep(2000L);
            vmProfile.findElement(By.xpath((TestBase.or_getproperty("vm_profile_drop1")+ VMNetworkprofile +TestBase.or_getproperty("vm_profile_drop2")))).click();
            }
          //end code
            Thread.sleep(3000);
            WebElement reasonText = driver.findElement(By.xpath(TestBase.or_getproperty("reason_text")));
            reasonText.sendKeys(Keys.CONTROL + "a");
            reasonText.sendKeys(Keys.DELETE);
            retryingFindClick(By.xpath(TestBase.or_getproperty("reason_text")));
            retryingFindClick(By.xpath(TestBase.or_getproperty("reason_text")));
            Thread.sleep(3000L);
            driver.findElement(By.xpath(TestBase.or_getproperty("reason_text"))).sendKeys(Reason);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.switchTo().defaultContent();
            driver.switchTo().frame("__gadget_2");
            Thread.sleep(10000);
            WebElement templateSubmitBtn = driver.findElement(By.id(TestBase.or_getproperty("submit_button")));
            //Actions action = new Actions(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(TestBase.or_getproperty("submit_button"))));
            templateSubmitBtn.click();
            Thread.sleep(10000L);
            
            if (driver.getPageSource().contains("Submit"))
            {
				templateSubmitBtn.click();
                Thread.sleep(5000L);
            }
 
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);      
            driver.findElement(By.xpath(TestBase.or_getproperty("confirmation_button"))).click();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);       
            driver.switchTo().defaultContent();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.findElement(By.xpath(TestBase.or_getproperty("request_tab"))).click();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            Thread.sleep(3000);
            driver.switchTo().frame("__gadget_3");
            Thread.sleep(15000L);
            int requestTabRowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();
 
            for(int i=1;i<=requestTabRowCount;i++)
            {
				String requestTabTempDesc = driver.findElement(By.xpath((TestBase.or_getproperty("description_drop1")+ i +TestBase.or_getproperty("description_drop2")))).getText();
               
                if(requestTabTempDesc.equals(Description))
                {
					String templateStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ i +TestBase.or_getproperty("status_drop2")))).getText();
                    TestLog.info("Request Status is:"+templateStatus);
 
                    if(templateStatus.contains("Pending Approval"))
                    {
						TestLog.info("Request status is pending for approval");
                        Assert.assertTrue(true,"Request status is pending for approval");
                        templateRequestId = driver.findElement(By.xpath(TestBase.or_getproperty("requestid_drop1")+ i + TestBase.or_getproperty("requestid_drop2"))).getText();
                        TestLog.info("Request Id is : "+templateRequestId);
                        break;
                    }
                    else
                    {
						TestLog.info("Request not Submitted Successfully****");
						excelScreenshotPath = CaptureScreenshot("Request not Submitted Successfully");
                        Assert.assertTrue(false,"Request not Submitted Succesfully****");
                    }
                
                }
 
            }
            
            driver.switchTo().defaultContent();
            driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();
            driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);  
            driver.get(TestBase.fn_getproperty("TCA"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(15000);
			WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
            WebElement approverUserName= webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("username")));
            approverUserName.sendKeys(TestBase.fn_getproperty("approver_requestor_username"));
            WebElement approverPassword= webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("password")));
            approverPassword.sendKeys(TestBase.fn_getproperty("approver_requestor_passwd"));
            Thread.sleep(15000);
			driver.findElement(By.id("submit")).click();
			Thread.sleep(15000);
			driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebElement approverInbox = driver.findElement(By.xpath(TestBase.or_getproperty("approver_inbox")));
			WebDriverWait driverWait = new WebDriverWait(driver, 30);
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(TestBase.or_getproperty("approver_inbox"))));
			approverInbox.click();
			Thread.sleep(10000); 
			String approInboxTotalPages = approverInbox.findElement(By.xpath(TestBase.or_getproperty("total_pages_inbox"))).getText();
			String[] totalPageCount = approInboxTotalPages.split(" ");
			Thread.sleep(15000L);
			String pageCount_Second = totalPageCount[1];
			Thread.sleep(15000L);
			int inboxTotalPages = Integer.parseInt(pageCount_Second);
			driver.switchTo().defaultContent(); 
			driver.findElement(By.xpath(TestBase.or_getproperty("last_updated_row"))).click();
			Thread.sleep(10000L);

			outer:for(int k=1;k<=inboxTotalPages;k++)
			{
				Thread.sleep(5000L);
				driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(TestBase.or_getproperty("row_count_desc_apprv_login"))));
				
				int apprvDescRowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc_apprv_login"))).size();

				for(int i=1;i<=apprvDescRowCount;i++)
				{
					String apprvRequestId = driver.findElement(By.xpath((TestBase.or_getproperty("reqid_apprv_login_drop1")+ i +TestBase.or_getproperty("reqid_apprv_login_drop2")))).getText();
        
					if(apprvRequestId.equals(templateRequestId))
					{
						TestLog.info("Request ID found on page: "+k);
						
						WebElement approverItemId= driver.findElement(By.xpath(TestBase.or_getproperty("approverid_drop1")+ i + TestBase.or_getproperty("approverid_drop3")));
						approverItemId.click();
						Thread.sleep(10000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("justfication_text"))).sendKeys("Test");
						driver.findElement(By.xpath(TestBase.or_getproperty("approve_button"))).click();
						Thread.sleep(5000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();
						break outer;
					}
					
					else 
					{
						TestLog.info("Request ID not found on page: "+k);
					}

				}
         
				driver.findElement(By.xpath(TestBase.or_getproperty("next_page"))).click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			}

			driver.findElement(By.xpath("//body")).sendKeys(Keys.F5); 
            driver.get(TestBase.fn_getproperty("TCA"));
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            Thread.sleep(15000);
            WebDriverWait requesterWait = new WebDriverWait(driver, 30);
            WebElement requesterUsername= requesterWait.until(ExpectedConditions.elementToBeClickable(By.id("username")));
            requesterUsername.sendKeys(TestBase.fn_getproperty("requestor_username"));
            WebElement requesterPassword= requesterWait.until(ExpectedConditions.elementToBeClickable(By.id("password")));
            requesterPassword.sendKeys(TestBase.fn_getproperty("requestor_passwd"));
            Thread.sleep(15000);
            driver.findElement(By.id("submit")).click();
            Thread.sleep(15000);
            driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
            Thread.sleep(30000L);
            driver.switchTo().defaultContent();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            WebDriverWait reqRequestTabWait = new WebDriverWait(driver, 10);
            reqRequestTabWait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("request_tab"))));
            driver.findElement(By.xpath(TestBase.or_getproperty("request_tab"))).click();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            Thread.sleep(3000);
            driver.switchTo().frame("__gadget_6");
            Thread.sleep(7000L);
            int descriptionRowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();
 
            for(int i=1;i<=descriptionRowCount;i++)
            {
				String machineRequestId = driver.findElement(By.xpath(TestBase.or_getproperty("requestid_drop1")+ i + TestBase.or_getproperty("requestid_drop2"))).getText();
            
                if(machineRequestId.equals(templateRequestId))
                {
					String requestStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ i +TestBase.or_getproperty("status_drop2")))).getText();
					TestLog.info("Request Status is:"+requestStatus);
                    
					if(requestStatus.equals("In Progress"))
                    { 
						result = true;
						TestLog.info("Request Submitted Succesfully");
                        double excelRequestId = Integer.parseInt(templateRequestId);
                        switch (SRS) 
                        {
                        	case "Windows Server 2008 DE R2 64 bit":
                        		writeVmDetails(excelRequestId,"Windows_Server_2008_DE_R2_64");
                        		break;
                        		
                        	case "Windows Server 2008 DE SP2 32 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2008_DE_SP2_32");
                        		break;
                        		
                        	case "Windows Server 2008 EE R2 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2008_EE_R2_64");
                        		break;
                        		
                        	case "Windows Server 2008 EE SP2 32 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2008_EE_SP2_32");
                        		break;
                        		
                        	case "Windows Server 2008 SE R2 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2008_SE_R2_64");
                        		break;
                        		
                        	case "Windows Server 2008 SE SP2 32 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2008_SE_SP2_32");
                        		break;
                        		
                        	case "Windows Server 2012 DE 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2012_DE_64");
                        		break;
                        		
                        	case "Windows Server 2012 SE 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2012_SE_64");
                        		break;
                        		
                        	case "Windows Server 2012 DE R2 U1 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2012_DE_R2_U1_64");
                        		break;
                        		
                        	case "Windows Server 2012 SE R2 U1 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2012_SE_R2_U1_64");
                        		break;
                        		
                        	case "Red Hat Enterprise Linux 6.x 64 bit":
                        		writeVmDetails(excelRequestId, "Red_Hat_Enterprise_Linux");
                        		break;
                        		
                        	case "Red Hat Enterprise Linux 6.x 32 bit":
                        		writeVmDetails(excelRequestId, "Red_Hat_Enterprise_Linux_32");
                        		break;
                        		
                        	case "SuSE Linux Enterprise Server 11.x 64-bit":
                        		writeVmDetails(excelRequestId, "SuSE_Linux_Enterprise_Server");
                        		//writeVmDetails(excelRequestId, "TestCaseResult");
                        		break;
                        		
                        	case "Red Hat Enterprise Linux 7.x 64 Bit":
                        		writeVmDetails(excelRequestId, "Red_Hat_Enterprise_Linux_7x_64");
                        		//writeVmDetails(excelRequestId, "TestCaseResult");
                        		break;
                        		
                        		
                        }
                        
                        	
                        Assert.assertTrue(true,"Request Submitted Succesfully");
                    }
                   
					else
                    {
						result = false;
						TestLog.info("Request not Submitted Succesfully during creation");                    
						double excelRequestId = Integer.parseInt(templateRequestId);
						switch (SRS) 
                        {
                        	case "Windows Server 2008 DE R2 64 bit":
                        		writeVmDetails(excelRequestId,"Windows_Server_2008_DE_R2_64");
                        		break;
                        		
                        	case "Windows Server 2008 DE SP2 32 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2008_DE_SP2_32");
                        		break;
                        		
                        	case "Windows Server 2008 EE R2 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2008_EE_R2_64");
                        		break;
                        		
                        	case "Windows Server 2008 EE SP2 32 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2008_EE_SP2_32");
                        		break;
                        		
                        	case "Windows Server 2008 SE R2 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2008_SE_R2_64");
                        		break;
                        		
                        	case "Windows Server 2008 SE SP2 32 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2008_SE_SP2_32");
                        		break;
                        		
                        	case "Windows Server 2012 DE 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2012_DE_64");
                        		break;
                        		
                        	case "Windows Server 2012 SE 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2012_SE_64");
                        		break;
                        		
                        	case "Windows Server 2012 DE R2 U1 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2012_DE_R2_U1_64");
                        		break;
                        		
                        	case "Windows Server 2012 SE R2 U1 64 bit":
                        		writeVmDetails(excelRequestId, "Windows_Server_2012_SE_R2_U1_64");
                        		break;
                        		
                        	case "Red Hat Enterprise Linux 6.x 64 bit":
                        		writeVmDetails(excelRequestId, "Red_Hat_Enterprise_Linux");
                        		break;
                        		
                        	case "Red Hat Enterprise Linux 6.x 32 bit":
                        		writeVmDetails(excelRequestId, "Red_Hat_Enterprise_Linux_32");
                        		break;
                        		
                        	case "SuSE Linux Enterprise Server 11.x 64-bit":
                        		writeVmDetails(excelRequestId, "SuSE_Linux_Enterprise_Server");
                        		//writeVmDetails(excelRequestId, "TestCaseResult");
                        		break;
                        		
                        	case "Red Hat Enterprise Linux 7.x 64 Bit":
                        		writeVmDetails(excelRequestId, "Red_Hat_Enterprise_Linux_7x_64");
                        }
						excelScreenshotPath = CaptureScreenshot("Request not Submitted Succesfully during creation");
                        Assert.assertTrue(false,"Request not Submitted Succesfully"); 
                    }
                }
                
            }
            
            driver.switchTo().defaultContent();
            driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();
            Thread.sleep(5000L);
            closeBrowser();
            TestLog.endTestCase(SRS); 
            
                
        }
 

       
}
 
