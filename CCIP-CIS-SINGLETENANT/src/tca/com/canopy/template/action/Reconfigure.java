/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : Base Class For Reconfigure Action.
 Date : 26/02/2016
 Test Case Name : NA
 **************************************************************
 */
package tca.com.canopy.template.action;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import tca.com.canopy.base.TestBase;
import tca.com.canopy.util.TestLog;

public class Reconfigure extends CIS_Template_Actions {
	String approverRequestId=null;
	boolean result=false;
	String sytemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;
	boolean vmStatus=false;

	public void reconfigure(
			String SRS,
			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,     
			String MaintainenaceBackupSchedule, 			
			String backupDate,
			String ReconfigureStorage,			
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException{
		result=false;
		if(VMCreated.equalsIgnoreCase("Y"))
		{

			openBrowser();

			driver.get(TestBase.fn_getproperty("TCA"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("username")).sendKeys(TestBase.fn_getproperty("requestor_username"));
			driver.findElement(By.id("password")).sendKeys(TestBase.fn_getproperty("requestor_passwd"));
			driver.findElement(By.id("submit")).click();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			//CLICKING ON ITEMS TAB
			WebDriverWait wait = new WebDriverWait(driver, 60);

			WebElement itemsTab = driver.findElement(By.xpath(TestBase.or_getproperty("items")));
			itemsTab.click();
			Thread.sleep(10000);

			//CLICKING ON MACHINE TAB
			driver.switchTo().frame("__gadget_2");
			Thread.sleep(10000);

			WebElement itemsMachineTab=driver.findElement(By.xpath(TestBase.or_getproperty("items_machine_tab")));
			Thread.sleep(10000);
			itemsMachineTab.click();
			Thread.sleep(10000);

			//CHECKING TOTAL NUMBER OF PAGES USED FOR VERIFYING THE MATCHING MACHINE NAMES
			WebElement totalPageItems = driver.findElement(By.xpath(TestBase.or_getproperty("total_pages_items")));
			WebElement totalPages= wait.until(ExpectedConditions.visibilityOf(totalPageItems));
			String pageCount= totalPages.getText();
			String[] pageCountParts = pageCount.split(" ");

			String pageCountSecond = pageCountParts[1];
			int itemsTabTotalPages = Integer.parseInt(pageCountSecond);
			vmStatus = false;

			Outer:for(int k=1;k<=itemsTabTotalPages;k++)
			{
				int machineColRowCount  = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_itmes"))).size();

				for(int i=1;i<=machineColRowCount ;i++){

					String itemsTabMachineName = driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2"))).getText();

					if(MachineName.contains(itemsTabMachineName)){
						vmStatus = true;
						TestLog.info("*Name is:"+itemsTabMachineName);
						driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2"))).click();
						Thread.sleep(15000L);

						// CLICKING ON RECONFIGURE ACTIONS

						driver.findElement(By.linkText("Reconfigure")).click();

						Thread.sleep(15000);

						// RECONFIGURE STORAGE EDITTED
						driver.switchTo().frame("__gadget_4");
						Thread.sleep(5000);
						List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));

						for(WebElement el : ele1){
							//Returns the Id of a frame.
							TestLog.info("Frame Id :" + el.getAttribute("id"));
							//Returns the Name of a frame.
							TestLog.info("Frame name :" + el.getAttribute("name"));
						}  
						driver.switchTo().frame("innerFrame");
						Thread.sleep(3000L);
						driver.findElement(By.linkText("Storage")).click();;
						Thread.sleep(3000L);
						driver.switchTo().defaultContent();
						driver.switchTo().frame("__gadget_2");
						Thread.sleep(3000L);
						driver.switchTo().frame("__gadget_4");
						Thread.sleep(5000);


						driver.switchTo().frame("innerFrame");
						Thread.sleep(3000L);

						try 
						{
							if ( driver.findElement(By.xpath("//*[@id='ctl00_ctl00_MasterContent_MainContentPlaceHolder_MachineRequestTabs_StorageGrid_pnlContent_storageGrid_DXEditor2_I']")).isDisplayed())
							{

								WebElement toClear4 = driver.findElement(By.xpath(TestBase.or_getproperty("reconfigure_new_storage_edit")));
								toClear4.sendKeys(Keys.CONTROL + "a");
								toClear4.sendKeys(Keys.DELETE);
								toClear4.sendKeys(Storage);

								Thread.sleep(5000L);

							}
						}
						catch (Exception e)
						{

							driver.findElement(By.xpath(TestBase.or_getproperty("reconfigure_storage_edit0"))).click();
							WebElement toClear4 = driver.findElement(By.xpath(TestBase.or_getproperty("reconfigure_new_storage_edit")));
							toClear4.sendKeys(Keys.CONTROL + "a");
							toClear4.sendKeys(Keys.DELETE);
							toClear4.sendKeys(Storage);

							Thread.sleep(5000L);
						}	

						driver.findElement(By.xpath(TestBase.or_getproperty("reconfigure_storage_edit1"))).click();
						Thread.sleep(2000L);
						driver.findElement(By.linkText("Machine Information")).click();

						Thread.sleep(5000L);
						WebElement toClear = driver.findElement(By.xpath(TestBase.or_getproperty("reconfigure_new_cpu")));
						toClear.sendKeys(Keys.CONTROL + "a");
						toClear.sendKeys(Keys.DELETE);
						driver.findElement(By.xpath(TestBase.or_getproperty("reconfigure_new_cpu"))).sendKeys(CPU);


						Thread.sleep(5000L);
						WebElement toClear1 = driver.findElement(By.xpath(TestBase.or_getproperty("reconfigure_new_memory")));
						toClear1.sendKeys(Keys.CONTROL + "a");
						toClear1.sendKeys(Keys.DELETE);
						driver.findElement(By.xpath(TestBase.or_getproperty("reconfigure_new_memory"))).sendKeys(Memory);
						Thread.sleep(5000);

						//-----------------

						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						driver.switchTo().defaultContent();
						Thread.sleep(3000);
						driver.switchTo().frame("__gadget_2");
						Thread.sleep(10000);
						WebElement submit = driver.findElement(By.xpath(TestBase.or_getproperty("submit_for_action_request")));

						WebDriverWait wait1 = new WebDriverWait(driver, 30);
						wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(TestBase.or_getproperty("submit_for_action_request"))));
						submit.click();
						Thread.sleep(10000L);

						if (driver.getPageSource().contains("Submit"))
						{
							submit.click();
							Thread.sleep(5000L);
						}


						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
						Thread.sleep(15000);

						//---------------------
						if (driver.findElements(By.xpath("//*[@id='CONFIRMATION_OK_BUTTON']")).size() > 0)
						{
							driver.findElement(By.xpath("//*[@id='CONFIRMATION_OK_BUTTON']")).click();
						}

						break Outer;

					}
					else
					{
						excelScreenshotPath=CaptureScreenshot("Machine name is not matching");
						TestLog.info("Machine Name is not matching with: "+MachineName);
					}


				}
				driver.findElement(By.xpath(TestBase.or_getproperty("next_page_items_it"))).click();
				Thread.sleep(10000L);
			}
			if(vmStatus==false)
			{
				excelScreenshotPath=CaptureScreenshot("Item not found");
				closeBrowser();
				TestLog.info("Item not found");
				Assert.assertTrue(false,"Item not found");
			}

			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();


			driver.get(TestBase.fn_getproperty("TCA"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("username")).sendKeys(TestBase.fn_getproperty("approver_requestor_username"));
			driver.findElement(By.id("password")).sendKeys(TestBase.fn_getproperty("approver_requestor_passwd"));
			driver.findElement(By.id("submit")).click();
			Thread.sleep(15000);
			driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Thread.sleep(15000L);
			WebElement inbox = driver.findElement(By.xpath(TestBase.or_getproperty("approver_inbox")));
			inbox.click();

			Thread.sleep(5000L);
			String inboxTotalPages = inbox.findElement(By.xpath(TestBase.or_getproperty("total_pages_inbox"))).getText();
			String[] totalPageCount= inboxTotalPages.split(" ");

			String totalCountSecond = totalPageCount[1];
			int finalPageCount = Integer.parseInt(totalCountSecond);
			driver.switchTo().defaultContent(); 
			driver.findElement(By.xpath(TestBase.or_getproperty("last_updated_row"))).click();
			Thread.sleep(20000L);

			outer:for(int k=1;k<=finalPageCount;k++)
			{

				Thread.sleep(20000L); 
				int approverrowCount1 = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc_apprv_login"))).size();		

				Thread.sleep(15000);
				for(int i=1;i<=approverrowCount1;i++){

					String requestedItem = driver.findElement(By.xpath((TestBase.or_getproperty("requested_item_drop3")+ i +TestBase.or_getproperty("requested_item_drop4")))).getText();


					if(requestedItem.contains(MachineName))
					{


						approverRequestId = driver.findElement(By.xpath(TestBase.or_getproperty("req_id_inbox_drop1")+ i + TestBase.or_getproperty("req_id_inbox_drop2"))).getText();

						WebElement itemId= driver.findElement(By.xpath(TestBase.or_getproperty("approverid_drop1")+ i + TestBase.or_getproperty("approverid_drop2")));
						itemId.click();
						Thread.sleep(10000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("justfication_text"))).sendKeys("Test");
						driver.findElement(By.xpath(TestBase.or_getproperty("approve_button"))).click();
						Thread.sleep(5000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();
						break outer;
					}

					else {
						TestLog.info("Request id not found on page:"+k);
					}

				}

				driver.findElement(By.xpath(TestBase.or_getproperty("next_page_appr_tab"))).click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}

			Thread.sleep(5000L);
			driver.get(TestBase.fn_getproperty("TCA"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("username")).sendKeys(TestBase.fn_getproperty("requestor_username"));
			driver.findElement(By.id("password")).sendKeys(TestBase.fn_getproperty("requestor_passwd"));
			driver.findElement(By.id("submit")).click();
			Thread.sleep(30000L);
			driver.switchTo().defaultContent();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);	
			driver.findElement(By.xpath(TestBase.or_getproperty("request_tab"))).click();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			Thread.sleep(5000L);
			List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));


			Thread.sleep(5000L);
			driver.switchTo().frame(ele1.size()-1);
			Thread.sleep(5000L);

			int rowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();

			outer:for(int i=1;i<=rowCount;i++){


				String id = driver.findElement(By.xpath(TestBase.or_getproperty("requestid_drop1")+ i + TestBase.or_getproperty("requestid_drop2"))).getText();


				if(id.equals(approverRequestId))
				{

					for (int m=0;m<30;m++)
					{
						driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
						Thread.sleep(5000L);
						List<WebElement> ele2 = driver.findElements(By.tagName("iframe"));

						Thread.sleep(5000L);
						driver.switchTo().frame(ele2.size()-1);

						Thread.sleep(5000L);
						String requestStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ i +TestBase.or_getproperty("status_drop2")))).getText();
						TestLog.info("Status is:"+requestStatus);

						if(requestStatus.equals("Successful"))
						{
							result=true;
							TestLog.info("Request Submitted Succesfully");
							Assert.assertTrue(true, "Request Submitted Succesfully");
							break outer;

						}else if(requestStatus.equals("In Progress")) 
						{
							TestLog.info("Request is in" +requestStatus+ "status");

						}else
						{
							Assert.assertTrue(false, "Request is in" +requestStatus+ "status");
							TestLog.info("Request is in" +requestStatus+ "status");
						}              
					}

				}

				else
				{
					excelScreenshotPath=CaptureScreenshot("Request ID");
					TestLog.info("Request ID is not matching with: "+approverRequestId);
				}

				driver.findElement(By.xpath(TestBase.or_getproperty("next_page_items_it"))).click();
				Thread.sleep(10000L);


			}

			if(vmStatus==false)
			{
				excelScreenshotPath=CaptureScreenshot("Item not found");

				TestLog.info("Item not found");
				Assert.assertTrue(false,"Item not found");
				closeBrowser();
			}	
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();


			closeBrowser();

		}

		else
		{
			TestLog.info("VM is not created");
			Assert.assertTrue(false,"vm not created");
		}
	}

}

