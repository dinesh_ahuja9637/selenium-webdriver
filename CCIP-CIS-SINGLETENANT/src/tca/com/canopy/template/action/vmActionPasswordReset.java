/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : TestCase For Password Reset Action
 Date : 26/02/2016
 Test Case Name : TC_001-TC_012
 **************************************************************
 */
package tca.com.canopy.template.action;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class vmActionPasswordReset extends PasswordReset{
	String templateName=null;

	@Test(dataProvider="getVmData")
	public void TC_001(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException
			{
		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,
				ReconfigureCPU,
				ReconfigureMemory
				);
			}


	@Test(dataProvider="getVmData1")
	public void TC_002(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException

			{
		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,
				ReconfigureCPU,
				ReconfigureMemory
				);

			}


	@Test(dataProvider="getVmData2")
	public void TC_003(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
			{
		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,		
				ReconfigureCPU,
				ReconfigureMemory
				);
			}


	@Test(dataProvider="getVmData3")
	public void TC_004(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
			{

		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,
				ReconfigureCPU,
				ReconfigureMemory
				);

			}


	@Test(dataProvider="getVmData4")
	public void TC_005(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
			{
		templateName=SRS;
		vm_passwor_actions(
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,	
				ReconfigureCPU,
				ReconfigureMemory
				);

			}


	@Test(dataProvider="getVmData5")
	public void TC_006(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
			{
		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,	
				ReconfigureCPU,
				ReconfigureMemory
				);

			}


	@Test(dataProvider="getVmData6")
	public void TC_007(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
			{
		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,		
				ReconfigureCPU,
				ReconfigureMemory
				);

			}


	@Test(dataProvider="getVmData7")
	public void TC_008(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
			{
		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 	
				backupDate,
				ReconfigureStorage,		
				ReconfigureCPU,
				ReconfigureMemory
				);
			}


	@Test(dataProvider="getVmData8")
	public void TC_009(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
			{
		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,		
				ReconfigureCPU,
				ReconfigureMemory
				);
			}


	@Test(dataProvider="getVmData9")
	public void TC_010(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
			{
		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,		
				ReconfigureCPU,
				ReconfigureMemory
				);
			}


	@Test(dataProvider="getVmData10")
	public void TC_011(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
			{
		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,		
				ReconfigureCPU,
				ReconfigureMemory
				);
			}


	@Test(dataProvider="getVmData11")
	public void TC_012(String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			)throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
			{
		templateName=SRS;
		vm_passwor_actions( 
				SRS,

				Machines,
				LeaseDuration,
				CPU,
				Memory,
				Storage,
				Description,
				Reason,
				BackupRetention,
				BackupSchedule,
				MaintainanceWindow1,
				MaintainanceWindow2,
				VMNetworkprofile,
				StoragePolicy,
				RequestID,
				MachineName,
				VMCreated,
				MaintainenaceBackupSchedule, 
				backupDate,
				ReconfigureStorage,	
				ReconfigureCPU,
				ReconfigureMemory
				);

			}

	@DataProvider(name="getVmData")
	public Object[][]vmData() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Red_Hat_Enterprise_Linux");
		return arrayObject1;                                                                                           
	}

	@DataProvider(name="getVmData1")
	public Object[][]vmData1() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","SuSE_Linux_Enterprise_Server");
		return arrayObject1;                                                                                           
	}


	@DataProvider(name="getVmData2")
	public Object[][]vmData2() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2008_DE_R2_64");
		return arrayObject1;                                                                                           
	}

	@DataProvider(name="getVmData3")
	public Object[][]vmData3() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2008_DE_SP2_32");
		return arrayObject1;                                                                                           
	}

	@DataProvider(name="getVmData4")
	public Object[][]vmData4() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2008_EE_SP2_32");
		return arrayObject1;                                                                                           
	}

	@DataProvider(name="getVmData5")
	public Object[][]vmData5() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2008_SE_R2_64");
		return arrayObject1;                                                                                           
	}

	@DataProvider(name="getVmData6")
	public Object[][]vmData6() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2008_SE_SP2_32");
		return arrayObject1;                                                                                           
	}

	@DataProvider(name="getVmData7")
	public Object[][]vmData7() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2012_DE_64");
		return arrayObject1;                                                                                           
	}

	@DataProvider(name="getVmData8")
	public Object[][]vmData8() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2012_DE_R2_U1_64");
		return arrayObject1;                                                                                           
	}

	@DataProvider(name="getVmData9")
	public Object[][]vmData9() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2012_SE_64");
		return arrayObject1;                                                                                           
	}

	@DataProvider(name="getVmData10")
	public Object[][]vmData10() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2012_SE_R2_U1_64");
		return arrayObject1;                                                                                           
	}

	@DataProvider(name="getVmData11")
	public Object[][]vmData11() {
		Object[][] arrayObject1 = tca.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls","Windows_Server_2008_EE_R2_64");
		return arrayObject1;                                                                                           
	}


	@AfterMethod(alwaysRun=true)
	public void generateReport(){
		if(driver!=null)
		{
			closeBrowser();
		}

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		sytemTime=""+ dateFormat.format(cal.getTime());
		excelRowCount++;

		if(!result)
		{
			switch (templateName) 
			{
			case "Windows Server 2008 DE R2 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",11,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",11,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",11,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  11, "FAIL");
				break;

			case "Windows Server 2008 DE SP2 32 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",12,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",12,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",12,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  12, "FAIL");
				break;

			case "Windows Server 2008 EE R2 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",8,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",8,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",8,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  8, "FAIL");
				break;

			case "Windows Server 2008 EE SP2 32 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",13,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",13,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",13,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  13, "FAIL");
				break;

			case "Windows Server 2008 SE R2 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",9,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",9,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",9,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  9, "FAIL");
				break;

			case "Windows Server 2008 SE SP2 32 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",10,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",10,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",10,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  10, "FAIL");
				break;

			case "Windows Server 2012 DE 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",2,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",2,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",2,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  2, "FAIL");
				break;

			case "Windows Server 2012 SE 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",4,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",4,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",4,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  4, "FAIL");
				break;

			case "Windows Server 2012 DE R2 U1 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",3,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",3,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",3,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  3, "FAIL");
				break;

			case "Windows Server 2012 SE R2 U1 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",5,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",5,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",5,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  5, "FAIL");
				break;

			case "Red Hat Enterprise Linux 6.x 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",6,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",6,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",6,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  6, "FAIL");
				break;

			case "SuSE Linux Enterprise Server 11.x 64-bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",7,"FAIL");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",7,sytemTime);
				test_data_xls.setCellData("TestCaseResult","PasswordReset Screenshot",7,excelScreenshotPath);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  7, "FAIL");
				break;


			}

		}

		else
		{
			switch (templateName) 
			{
			case "Windows Server 2008 DE R2 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",11,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",11,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  11, "PASS");
				break;

			case "Windows Server 2008 DE SP2 32 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",12,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",12,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  12, "PASS");
				break;

			case "Windows Server 2008 EE R2 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",8,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",8,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  8, "PASS");
				break;

			case "Windows Server 2008 EE SP2 32 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",13,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",13,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  13, "PASS");
				break;

			case "Windows Server 2008 SE R2 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",9,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",9,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  9, "PASS");
				break;

			case "Windows Server 2008 SE SP2 32 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",10,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",10,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  10, "PASS");
				break;

			case "Windows Server 2012 DE 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",2,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",2,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  2, "PASS");
				break;

			case "Windows Server 2012 SE 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",4,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",4,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  4, "PASS");
				break;

			case "Windows Server 2012 DE R2 U1 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",3,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",3,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  3, "PASS");
				break;

			case "Windows Server 2012 SE R2 U1 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",5,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",5,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  5, "PASS");
				break;

			case "Red Hat Enterprise Linux 6.x 64 bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",6,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",6,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  6, "PASS");
				break;

			case "SuSE Linux Enterprise Server 11.x 64-bit":
				test_data_xls.setCellData("TestCaseResult","PasswordReset Result",7,"PASS");
				test_data_xls.setCellData("TestCaseResult","PasswordReset Time",7,sytemTime);
				test_data_xls.setCellData("Suite", "PasswordReset Result",  7, "PASS");
				break;

			}
		}
	}


}
