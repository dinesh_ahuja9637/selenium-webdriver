/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose :Base Class for Action Suite
 Date : 26/02/2016
 Test Case Name : NA
 **************************************************************
 */
package tca.com.canopy.template.action;

import java.io.IOException;

import org.apache.log4j.xml.DOMConfigurator;
import org.testng.SkipException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import tca.com.canopy.base.TestBase;
import tca.com.canopy.util.ExcelReadAndWrite;
import tca.com.canopy.util.TestLog;
import tca.com.canopy.util.TestUtil;

public class CIS_Template_Actions extends TestBase {
	
	@BeforeSuite
	public void checkSuiteSkip() throws Exception
	{
		DOMConfigurator.configure("log4j.xml");
		if(!TestUtil.isSuiteExecutable("CIS_Template_Actions"))
		{
			TestLog.info("Runmode of CIS_Template_Actions suite set to no. So Skipping all tests in CIS_Template_Actions suite");
			throw new SkipException("Runmode of CIS_Template_Actions suite set to no. So Skipping all tests in CIS_Template_Actions suite");
		}
	}
	
	 @AfterSuite
     public void generateBackupReport() throws IOException
     {
     	ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\tca\\com\\canopy\\xls\\TestData.xls");

     }

}
