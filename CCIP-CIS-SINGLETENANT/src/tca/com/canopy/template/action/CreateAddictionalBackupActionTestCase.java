/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : Base Class For Create Additional Backup Action
 Date : 26/02/2016
 Test Case Name : NA
 **************************************************************
 */
package tca.com.canopy.template.action;

import java.io.IOException;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import tca.com.canopy.base.TestBase;
import tca.com.canopy.util.TestLog;

public class CreateAddictionalBackupActionTestCase extends CIS_Template_Actions 
{
	boolean vmStatus=false;
	boolean result = false;
	String sytemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;

	public void Create_Additional_Backup_Action(

			String SRS,
			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException{

		result=false;

		if(RequestID.trim().isEmpty())
		{
			Assert.assertTrue(false,"Request ID is not Present in Excel for the current template");

		}
		if((VMCreated!=null)&& VMCreated.equalsIgnoreCase("Y"))
		{
			openBrowser();
			driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
			Thread.sleep(10000L);
			driver.get(TestBase.fn_getproperty("TCA"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(driver, 30);
			WebElement requestorUsername= wait.until(ExpectedConditions.elementToBeClickable(By.id("username")));
			requestorUsername.sendKeys(TestBase.fn_getproperty("requestor_username"));
			WebElement requestorPassword= wait.until(ExpectedConditions.elementToBeClickable(By.id("password")));
			requestorPassword.sendKeys(TestBase.fn_getproperty("requestor_passwd"));
			Thread.sleep(15000);
			driver.findElement(By.id("submit")).click();

			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			WebElement itemsTab = driver.findElement(By.xpath(TestBase.or_getproperty("items")));
			itemsTab.click();
			Thread.sleep(10000);

			List<WebElement> ele3 = driver.findElements(By.tagName("iframe"));


			Thread.sleep(5000L);
			driver.switchTo().frame(ele3.size()-1);
			Thread.sleep(15000L);
			WebElement machineTab = driver.findElement(By.xpath(TestBase.or_getproperty("machine_tab")));
			machineTab.click();//--or property 
			Thread.sleep(15000L);

			WebElement totalPageItems = driver.findElement(By.xpath(TestBase.or_getproperty("total_pages_items")));
			WebElement totalPages= wait.until(ExpectedConditions.visibilityOf(totalPageItems));
			String pageCount= totalPages.getText();
			String[] pageCountParts = pageCount.split(" ");

			String pageCountSecond = pageCountParts[1];
			int itemsTabTotalPages = Integer.parseInt(pageCountSecond);
			vmStatus = false;

			Outer:for(int k=1;k<=itemsTabTotalPages;k++)
			{
				int machineColRowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_itmes"))).size();

				for(int i=1;i<=machineColRowCount;i++)		
				{

					String itemsTabMachineName = driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2"))).getText();
					if(MachineName.contains(itemsTabMachineName))
					{

						// code for verification with testdata

						vmStatus = true;
						Thread.sleep(5000L);

						WebElement requestedItem=driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2")));
						requestedItem.click();
						Thread.sleep(10000L);

						Thread.sleep(10000L);

						driver.findElement(By.linkText("Backup - Create Additional Backup")).click();

						Thread.sleep(5000L);

						driver.findElement(By.xpath(TestBase.or_getproperty("submit_for_action_request"))).click();

						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);       
						driver.findElement(By.xpath(TestBase.or_getproperty("confirmation_button"))).click();

						Thread.sleep(15000L);

						driver.switchTo().defaultContent();
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
						Thread.sleep(20000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("request_tab"))).click();
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
						Thread.sleep(5000L);

						List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));

						Thread.sleep(5000L);
						driver.switchTo().frame(ele1.size()-1);

						Thread.sleep(15000L);
						int rowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();

						Thread.sleep(20000L);

						for(int j=1;j<=rowCount;j++)
						{
							String requestDesc = driver.findElement(By.xpath((TestBase.or_getproperty("item_drop1")+ j +TestBase.or_getproperty("item_drop2")))).getText();


							String[] totalPartsDesc = requestDesc.split(" ");

							String descPart1 = totalPartsDesc[0]; 

							Thread.sleep(10000);


							String descPart7="" ;

							if( descPart1.equals("Backup"))
							{
								descPart7 = totalPartsDesc[6]; 
							}

							if(descPart1.equals("Backup")& descPart7.equals(MachineName))
							{
								for( int m=0;m<20;m++ )
								{
									driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
									Thread.sleep(5000L);
									List<WebElement> ele2 = driver.findElements(By.tagName("iframe"));


									Thread.sleep(5000L);
									driver.switchTo().frame(ele2.size()-1);

									Thread.sleep(5000L);

									String requestStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ j +TestBase.or_getproperty("status_drop2")))).getText();
									TestLog.info("Status is:"+requestStatus);
									if(requestStatus.equals("Successful")|requestStatus.equals("Approved"))
									{
										result=true;
										TestLog.info("Request Submitted Succesfully");

										Assert.assertTrue(true, "Request Submitted Succesfully");
										break Outer;

									}
									else if(requestStatus.equals("In Progress")) 
									{
										TestLog.info("Request is in" +requestStatus+ "status");

									}
									else
									{
										Assert.assertTrue(false, "Request is in" +requestStatus+ "status");
										TestLog.info("Request is in" +requestStatus+ "status");

									}              

								}

							}

						}
						break Outer;

					}
					else
					{
						excelScreenshotPath=CaptureScreenshot("Machine name is not matching");

						TestLog.info("Machine name is not matching");

					}
					driver.findElement(By.xpath(TestBase.or_getproperty("next_page_items_it"))).click();
					Thread.sleep(10000L);
				}

			}	
			if(vmStatus==false)
			{
				excelScreenshotPath=CaptureScreenshot("Item not found");
				TestLog.info("Item not found");
				closeBrowser();

				Assert.assertTrue(false,"Item not found");
			}
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();
			closeBrowser();	

		}

		else
		{
			TestLog.info("VM is not created");

			Assert.assertTrue(false,"vm not created");
			closeBrowser();
		}
	}
}
