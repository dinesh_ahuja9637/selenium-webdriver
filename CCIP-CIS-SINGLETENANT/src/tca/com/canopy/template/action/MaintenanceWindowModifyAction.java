/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : Base Class For Maintenance Window Modify Action.
 Date : 26/02/2016
 Test Case Name : NA
 **************************************************************
 */
package tca.com.canopy.template.action;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import tca.com.canopy.base.TestBase;
import tca.com.canopy.util.TestLog;

public class MaintenanceWindowModifyAction extends CIS_Template_Actions {

	boolean vmStatus=false;
	boolean result = false;
	String sytemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;

	public void Maintenance_Window_Modify_Action (
			String SRS,
			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,           
			String MaintainenaceBackupSchedule, 			
			String backupDate,
			String ReconfigureStorage,		
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, RowsExceededException, WriteException, BiffException, IOException{
		result=false;

		if(VMCreated.equalsIgnoreCase("Y"))
		{

			openBrowser();
			driver.get(TestBase.fn_getproperty("TCA"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("username")).sendKeys(TestBase.fn_getproperty("requestor_username"));
			driver.findElement(By.id("password")).sendKeys(TestBase.fn_getproperty("requestor_passwd"));
			driver.findElement(By.id("submit")).click();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(driver, 60);

			WebElement requestorItemsTab = driver.findElement(By.xpath(TestBase.or_getproperty("items")));
			requestorItemsTab.click();
			Thread.sleep(10000);
			driver.switchTo().frame("__gadget_2");
			Thread.sleep(15000L);
			WebElement machineTab = driver.findElement(By.xpath(TestBase.or_getproperty("machine_tab")));
			machineTab.click();//--or property 
			Thread.sleep(15000L);
			WebElement totalPageItems = driver.findElement(By.xpath(TestBase.or_getproperty("total_pages_items")));
			WebElement totalPages= wait.until(ExpectedConditions.visibilityOf(totalPageItems));
			String pageCount= totalPages.getText();
			String[] pageCountParts = pageCount.split(" ");

			String pageCountSecond = pageCountParts[1];
			int itemsTabTotalPages = Integer.parseInt(pageCountSecond);
			vmStatus = false;

			Outer:for(int k=1;k<=itemsTabTotalPages;k++)
			{
				int machineColRowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_itmes"))).size();

				for(int i=1;i<=machineColRowCount;i++)
				{

					String itemsTabMachineName = driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2"))).getText();

					if(MachineName.contains(itemsTabMachineName))
					{
						vmStatus = true;
						TestLog.info("*Name is:"+itemsTabMachineName);
						driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2"))).click();
						// code for verification with testdata

						Thread.sleep(5000L);


						Thread.sleep(10000L);

						driver.findElement(By.linkText("Maintenance Window - Modify")).click();

						Thread.sleep(10000L);

						Thread.sleep(10000L);

						//List<WebElement> dropdown = driver.findElements(By.tagName("select"));  

						Thread.sleep(3000L);
						WebElement mainBackupBschedulePrimary = driver.findElement(By.xpath(TestBase.or_getproperty("maintenance_backup_schedule_primary")));
						((JavascriptExecutor)driver).executeScript("scroll(0,400)");
						Thread.sleep(2000L);
						mainBackupBschedulePrimary.click();
						Thread.sleep(15000L);

						TestLog.info(TestBase.or_getproperty("maintenance_backup_schedule_primary_drop1")+ MaintainenaceBackupSchedule+TestBase.or_getproperty("maintenance_backup_schedule_primary_drop2"));
						mainBackupBschedulePrimary.findElement(By.xpath((TestBase.or_getproperty("maintenance_backup_schedule_primary_drop1")+ MaintainenaceBackupSchedule +TestBase.or_getproperty("maintenance_backup_schedule_primary_drop2")))).click();


						Thread.sleep(5000L);
						WebElement mainBackupScheduleSec = driver.findElement(By.xpath(TestBase.or_getproperty("maintenance_backup_schedule_sec")));
						((JavascriptExecutor)driver).executeScript("scroll(0,400)");
						Thread.sleep(2000L);
						mainBackupScheduleSec.click();
						Thread.sleep(2000L);
						TestLog.info(TestBase.or_getproperty("maintenance_backup_schedule_sec_drop1")+ BackupSchedule+TestBase.or_getproperty("maintenance_backup_schedule_sec_drop2"));
						mainBackupScheduleSec.findElement(By.xpath((TestBase.or_getproperty("maintenance_backup_schedule_sec_drop1")+ MaintainenaceBackupSchedule +TestBase.or_getproperty("maintenance_backup_schedule_sec_drop2")))).click();

						Thread.sleep(2000L);

						driver.findElement(By.xpath(TestBase.or_getproperty("maintenancesubmit"))).click();
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);       
						driver.findElement(By.xpath(TestBase.or_getproperty("confirmation_button"))).click();


						Thread.sleep(15000L);

						driver.switchTo().defaultContent();
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
						Thread.sleep(20000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("request_tab"))).click();
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
						Thread.sleep(5000L);

						List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));


						Thread.sleep(5000L);
						driver.switchTo().frame(ele1.size()-1);

						Thread.sleep(15000L);
						int rowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();

						Thread.sleep(20000L);

						for(int j=1;j<=rowCount;j++)
						{
							String requestDesc = driver.findElement(By.xpath((TestBase.or_getproperty("item_drop1")+ j +TestBase.or_getproperty("item_drop2")))).getText();


							String[] totalPartsDesc = requestDesc.split(" ");

							for(int l=0;l<totalPartsDesc.length;l++){
								String descPart1 =totalPartsDesc[l]; 

							}

							String descPart1 = totalPartsDesc[0];
							String descPart5 ="";
							if(descPart1.equals("Maintenance"))
							{
								descPart5 = totalPartsDesc[5]; 
							}

							Thread.sleep(20000L);

							if(descPart1.equals("Maintenance")& descPart5.equals(MachineName))
							{
								for( int m=0;m<30;m++ )
								{
									driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
									Thread.sleep(5000L);
									List<WebElement> ele2 = driver.findElements(By.tagName("iframe"));

									Thread.sleep(5000L);
									driver.switchTo().frame(ele2.size()-1);

									Thread.sleep(5000L);

									String requestStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ j +TestBase.or_getproperty("status_drop2")))).getText();
									TestLog.info("Status is:"+requestStatus);

									if(requestStatus.equals("Completed")|requestStatus.equals("Successful"))
									{
										result=true;
										TestLog.info("Request Submitted Succesfully");
										Assert.assertTrue(true, "Request Submitted Succesfully");
										break Outer;

									}else if(requestStatus.equals("In Progress")) 
									{
										TestLog.info("Request is in" +requestStatus+ "status");

									}else
									{
										Assert.assertTrue(false, "Request is in" +requestStatus+ "status");
										TestLog.info("Request is in" +requestStatus+ "status");

									}              

								}
							}

						}

						break Outer;
					}
					else
					{
						excelScreenshotPath=CaptureScreenshot("Machine name is not matching");
						TestLog.info("Machine Name is not matching with: "+MachineName);
						TestLog.info("Machine name is not matching");

					}

					driver.findElement(By.xpath(TestBase.or_getproperty("next_page_items_it"))).click();

				}	
			}
			if(vmStatus==false)
			{
				excelScreenshotPath=CaptureScreenshot("Item not found");


				TestLog.info("Item not found");
				closeBrowser();
				Assert.assertTrue(false,"Item not found");
			}


			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();
			closeBrowser();
		}

		else
		{
			TestLog.info("VM is not created");
			Assert.assertTrue(false,"vm not created");
			closeBrowser();
		}
	}

}
