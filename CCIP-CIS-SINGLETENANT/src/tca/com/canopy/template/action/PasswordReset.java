/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : Base Class For Password Reset.
 Date : 26/02/2016
 Test Case Name : NA
 **************************************************************
 */
package tca.com.canopy.template.action;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import tca.com.canopy.base.TestBase;
import tca.com.canopy.util.TestLog;

public class PasswordReset extends CIS_Template_Actions {

	public String approverRequestId=null;
	boolean result=false;
	String sytemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;
	boolean vmStatus=false;

	public void vm_passwor_actions
	(	
			String SRS,

			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String BackupRetention,
			String BackupSchedule,
			String MaintainanceWindow1,
			String MaintainanceWindow2,
			String VMNetworkprofile,
			String StoragePolicy,
			String RequestID,
			String MachineName,
			String VMCreated,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory
			) throws InterruptedException, IOException{
		if(RequestID.trim().isEmpty())
		{
			Assert.assertTrue(false,"Request ID is not Present in Excel for the current template");

		}
		if((VMCreated!=null)&& VMCreated.equalsIgnoreCase("Y"))
		{

			openBrowser();
			driver.get(TestBase.fn_getproperty("TCA"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("username")).sendKeys(TestBase.fn_getproperty("requestor_username"));
			driver.findElement(By.id("password")).sendKeys(TestBase.fn_getproperty("requestor_passwd"));
			driver.findElement(By.id("submit")).click();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			WebDriverWait wait = new WebDriverWait(driver, 60);

			WebElement itemsTab = driver.findElement(By.xpath(TestBase.or_getproperty("items")));
			itemsTab.click();
			Thread.sleep(10000);


			driver.switchTo().frame("__gadget_2");

			Thread.sleep(15000L);
			WebElement machineTab = driver.findElement(By.xpath(TestBase.or_getproperty("machine_tab")));
			machineTab.click();//--or property 
			Thread.sleep(15000L);
			WebElement totalPageItems = driver.findElement(By.xpath(TestBase.or_getproperty("total_pages_items")));
			WebElement totalPages= wait.until(ExpectedConditions.visibilityOf(totalPageItems));
			String pageCount= totalPages.getText();
			String[] pageCountParts = pageCount.split(" ");

			String pageCountSecond = pageCountParts[1];
			int itemsTabTotalPages = Integer.parseInt(pageCountSecond);
			vmStatus = false;

			Outer:for(int k=1;k<=itemsTabTotalPages;k++)
			{
				int machineColRowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_itmes"))).size();

				for(int i=1;i<=machineColRowCount;i++){

					String itemsTabMachineName = driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2"))).getText();
					TestLog.info("*Name is:"+itemsTabMachineName);

					if(MachineName.contains(itemsTabMachineName)){
						vmStatus = true;

						driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2"))).click();
						// code for verification with testdata

						Thread.sleep(5000L);

						driver.findElement(By.linkText("Password Reset")).click();

						Thread.sleep(15000);

						driver.findElement(By.xpath(TestBase.or_getproperty("admin_account"))).sendKeys("cust-admin");

						driver.findElement(By.xpath(TestBase.or_getproperty("submit_for_action_request"))).click();
						driver.findElement(By.xpath("//*[@id='CONFIRMATION_OK_BUTTON']")).click();

						Thread.sleep(15000L);

						driver.switchTo().defaultContent();
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
						Thread.sleep(20000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("request_tab"))).click();
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
						Thread.sleep(5000L);

						List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));

						Thread.sleep(5000L);
						driver.switchTo().frame(ele1.size()-1);

						Thread.sleep(5000L);
						int rowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();

						Thread.sleep(20000L);

						for(int j=1;j<=rowCount;j++){
							String requestDesc = driver.findElement(By.xpath((TestBase.or_getproperty("item_drop1")+ j +TestBase.or_getproperty("item_drop2")))).getText();

							String[] totalPartsDesc  = requestDesc.split(" ");

							String descPart1 = totalPartsDesc[0]; 

							Thread.sleep(10000);



							if(descPart1.equals("Password"))
							{
								for( int m=0;m<30;m++ )
								{
									driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
									Thread.sleep(5000L);
									List<WebElement> ele2 = driver.findElements(By.tagName("iframe"));


									Thread.sleep(5000L);
									driver.switchTo().frame(ele2.size()-1);

									Thread.sleep(5000L);

									String requestStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ j +TestBase.or_getproperty("status_drop2")))).getText();
									TestLog.info("Status is:"+requestStatus);
									if(requestStatus.equals("Successful"))
									{                result=true;
									TestLog.info("Request Submitted Succesfully");
									Assert.assertTrue(true, "Request Submitted Succesfully");
									break Outer;

									}else if(requestStatus.equals("In Progress")) 
									{
										TestLog.info("Request is in" +requestStatus+ "status");

									}else
									{
										Assert.assertTrue(false, "Request is in" +requestStatus+ "status");
										TestLog.info(requestStatus);
									}              

								}

							}

						}

						break Outer;

					}
					else
					{
						excelScreenshotPath=CaptureScreenshot("Machine name is not matching");
						TestLog.info("Machine Name is not matching with: "+MachineName);
					}

					driver.findElement(By.xpath(TestBase.or_getproperty("next_page_items_it"))).click();
					Thread.sleep(10000L);

				}

			}
			if(vmStatus==false)
			{
				excelScreenshotPath=CaptureScreenshot("Item not found");

				TestLog.info("Item not found");
				Assert.assertTrue(false,"Item not found");
				closeBrowser();
			}	
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();
			closeBrowser();	
		}

		else
		{
			TestLog.info("VM is not created");
			Assert.assertTrue(false,"vm not created");
		}

	}

}




