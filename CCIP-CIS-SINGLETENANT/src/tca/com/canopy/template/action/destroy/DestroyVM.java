package tca.com.canopy.template.action.destroy;

/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : Base Class For Destroy VM Action.
 Date : 26/02/2016
 Test Case Name : NA
 **************************************************************
 */


import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;

import tca.com.canopy.base.TestBase;

import tca.com.canopy.util.TestLog;


public class DestroyVM extends CIS_Template_Actions {

	String approverRequestId=null;
	boolean result=false;
	
	String sytemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;
	boolean vmStatus=false;

	public void vmDestroyAction	(
			String SRS,
			String Machines,
			String LeaseDuration,
			String CPU,
			String Memory,
			String Storage,
			String Description,
			String Reason,
			String Backup_Retention,
			String Backup_Schedule,
			String Maintainance_win1,
			String Maintainance_win2,
			String VM_profile,
			String Storage_Policy,
			String Request_ID,
			String Machine_Name,
			String VM_Created,
			String MaintainenaceBackupSchedule, 
			String backupDate,
			String ReconfigureStorage,
			String ReconfigureCPU,
			String ReconfigureMemory

			) throws InterruptedException, IOException{

		result=false;
		excelScreenshotPath= null;
		vmStatus=false;

		if(VM_Created.equalsIgnoreCase("Y"))
		{
			openBrowser();
			driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
			Thread.sleep(10000L);
			driver.get(TestBase.fn_getproperty("TCA"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(driver, 30);
			WebElement requestorUsername= wait.until(ExpectedConditions.elementToBeClickable(By.id("username")));
			requestorUsername.sendKeys(TestBase.fn_getproperty("requestor_username"));
			WebElement requestorPassword= wait.until(ExpectedConditions.elementToBeClickable(By.id("password")));
			requestorPassword.sendKeys(TestBase.fn_getproperty("requestor_passwd"));
			Thread.sleep(15000);
			driver.findElement(By.id("submit")).click();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebElement requestorItemsTab = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("items"))));
			requestorItemsTab.click();
			Thread.sleep(10000);


			List<WebElement> lstItemsTabFrames = driver.findElements(By.tagName("iframe"));

			driver.switchTo().frame(lstItemsTabFrames.size()-1);
			Thread.sleep(5000L);
			WebElement machine_tab = driver.findElement(By.xpath(TestBase.or_getproperty("machine_tab")));
			machine_tab.click();//--or property
			Thread.sleep(15000L);
			WebElement totalPageItems = driver.findElement(By.xpath(TestBase.or_getproperty("total_pages_items")));
			WebElement totalPages= wait.until(ExpectedConditions.visibilityOf(totalPageItems));
			String pageCount= totalPages.getText();
			String[] pageCountParts = pageCount.split(" ");

			String pageCountSecond = pageCountParts[1];
			int itemsTabTotalPages = Integer.parseInt(pageCountSecond);
			vmStatus = false;

			Outer:for(int k=1;k<=itemsTabTotalPages;k++)
			{
				int machineColRowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_itmes"))).size();

				for(int i=1;i<=machineColRowCount;i++)
				{
					String itemsTabMachineName = driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2"))).getText();
					TestLog.info("*Machine name from items tab is:"+itemsTabMachineName);
					TestLog.info("**Desired Machine name is:"+Machine_Name);
					TestLog.info("**VMCreated**"+VM_Created);

					if(Machine_Name.contains(itemsTabMachineName))
					{
						vmStatus=true;
						WebElement machineDescription = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("descr_drop1")+ i + TestBase.or_getproperty("descr_drop2"))));
						machineDescription.click();
						Thread.sleep(3000L);
						WebElement requestedItem=driver.findElement(By.xpath(TestBase.or_getproperty("machine_name_drop1")+ i + TestBase.or_getproperty("machine_name_drop2")));
						requestedItem.click();
						Thread.sleep(10000L);
						//WebElement destroyActionBtn=driver.findElement(By.xpath(TestBase.or_getproperty("destroy_action")));
						//destroyActionBtn.click();
						driver.findElement(By.linkText("Destroy")).click();
						Thread.sleep(15000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("submit_for_action_request"))).click();
						Thread.sleep(2000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("confirmation_button"))).click();	
						break Outer;
					}

					else
					{
						excelScreenshotPath=CaptureScreenshot("Machine name is not matching_"+Machine_Name);
						TestLog.info("Machine Name is not matching with: "+Machine_Name);
					}
				}

				driver.findElement(By.xpath(TestBase.or_getproperty("next_page_items_it"))).click();
				Thread.sleep(10000L);
			}

			if(!vmStatus)
			{
				TestLog.info("Machine Name:"+Machine_Name);
				excelScreenshotPath=CaptureScreenshot("Item not found_"+Machine_Name);
				closeBrowser();
				result=false;
				TestLog.info("Item not found");
				Assert.assertTrue(false,"Item not found");
			}

			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();
			driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
			Thread.sleep(10000L);
			driver.get(TestBase.fn_getproperty("TCA"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("username")).sendKeys(TestBase.fn_getproperty("approver_requestor_username"));
			driver.findElement(By.id("password")).sendKeys(TestBase.fn_getproperty("approver_requestor_passwd"));
			driver.findElement(By.id("submit")).click();

			Thread.sleep(15000);
			driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);

			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Thread.sleep(15000L);
			WebElement approverInbox = driver.findElement(By.xpath(TestBase.or_getproperty("approver_inbox")));
			approverInbox.click();
			Thread.sleep(5000L);
			String inboxTotalPages = approverInbox.findElement(By.xpath(TestBase.or_getproperty("total_pages_inbox"))).getText();
			String[] totalPageCount= inboxTotalPages.split(" ");
			String totalCountSecond = totalPageCount[1];
			int finalPageCount = Integer.parseInt(totalCountSecond);
			driver.switchTo().defaultContent(); 
			driver.findElement(By.xpath(TestBase.or_getproperty("last_updated_row"))).click();
			Thread.sleep(20000L);

			outer:for(int k=1;k<=finalPageCount;k++)
			{
				TestLog.info("We are on page:"+k);
				Thread.sleep(5000L);
				int approverDescCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc_apprv_login"))).size();
				TestLog.info ("Rowcount: "+approverDescCount);
				Thread.sleep(10000L);

				for(int i=1;i<=approverDescCount;i++)
				{
					String approverRequestedItem = driver.findElement(By.xpath((TestBase.or_getproperty("requested_item_drop1")+ i +TestBase.or_getproperty("requested_item_drop2")))).getText();
					TestLog.info(approverRequestedItem);

					if(approverRequestedItem.contains(Machine_Name))
					{
						TestLog.info("Found id on page: "+k);
						approverRequestId = driver.findElement(By.xpath(TestBase.or_getproperty("req_id_inbox_drop1")+ i + TestBase.or_getproperty("req_id_inbox_drop2"))).getText();
						TestLog.info("Request id is:" +approverRequestId );
						WebElement approverItemId= driver.findElement(By.xpath(TestBase.or_getproperty("approverid_drop1")+ i + TestBase.or_getproperty("approverid_drop3")));
						approverItemId.click();
						Thread.sleep(10000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("justfication_text"))).sendKeys("Test");
						driver.findElement(By.xpath(TestBase.or_getproperty("approve_button"))).click();
						Thread.sleep(5000L);
						driver.findElement(By.xpath(TestBase.or_getproperty("logout_button"))).click();
						break outer;
					}

					else 
					{
						TestLog.info("Request id not found on page:"+k);
					}

				}

				driver.findElement(By.xpath(TestBase.or_getproperty("next_page"))).click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}

			Thread.sleep(5000L);
			driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
			Thread.sleep(10000L);
			driver.get(TestBase.fn_getproperty("TCA"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("username")).sendKeys(TestBase.fn_getproperty("requestor_username"));
			driver.findElement(By.id("password")).sendKeys(TestBase.fn_getproperty("requestor_passwd"));
			driver.findElement(By.id("submit")).click();
			Thread.sleep(30000L);
			driver.switchTo().defaultContent();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.findElement(By.xpath(TestBase.or_getproperty("request_tab"))).click();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			Thread.sleep(5000L);
			driver.switchTo().frame("__gadget_6");
			Thread.sleep(20000L);
			int apprvDescRowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();

			for(int i=1;i<=apprvDescRowCount;i++)
			{
				String reqId = driver.findElement(By.xpath(TestBase.or_getproperty("requestid_drop1")+ i + TestBase.or_getproperty("requestid_drop2"))).getText();

				if(reqId.equals(approverRequestId))
				{
					String machineStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ i +TestBase.or_getproperty("status_drop2")))).getText();
					TestLog.info("Status is:"+machineStatus);
					if(machineStatus.equalsIgnoreCase("Successful"))
					{
						result=true;
						TestLog.info("Request Submitted Succesfully");
						Assert.assertTrue(true, "Request Submitted Succesfully");
					}

					else 
					{
						result=false;
						TestLog.info("Request not Submitted Succesfully");
						excelScreenshotPath = CaptureScreenshot("Request not Submitted Succesfully_"+approverRequestId);
						Assert.assertTrue(false, "Request not Submitted Succesfully****");
					}
				}
			}

			closeBrowser();
		}

		
		else
		{   
			result=false;
			TestLog.info("vm not created");
			
			Assert.assertTrue(false, "VM not created");
		}
	}

}



