#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:   Shraddha Bhongle & Dinesh Ahuja
 Purpose : Script will automatically renew the license for different vSphere components.
 Date :    29/04/2016


#ce ----------------------------------------------------------------------------

#include <Excel.au3>
#include <File.au3>
#include <TestBase.au3>
#include <StringConstants.au3>



Func vChargeBackLicenseRenew()

   ;Checking Run-Mode
   if $vChargeBackRunMode=="Y" Then
   vSphereLogin()
   ;clicking vchargeback
   MouseClick("right",124,233,1,0)
   _FileWriteLog(@ScriptDir & "\vChargeBack.log", "Clicked on vchargeback option",1)
   vSphereLicenseRenew($vChargeBackKey)
   vChargeBackAddition()
   vSphereClose()
   writeExcelData("PASS","D2")
   Else
   ConsoleWrite("Run Mode is set to NO")
   writeExcelData("SKIP","D2")
   EndIf

EndFunc


Func vCenterEnterpriseLicenseRenew()

   ;Checking Run-Mode
   if $vCenterEnterpriseRunMode=="Y" Then
   vSphereLogin()
   ;clicking on component
   MouseClick("right",187,251,1,0)
   _FileWriteLog(@ScriptDir & "\vChargeBack.log", "Clicked on vchargeback option",1)
   vSphereLicenseRenew($vCenterEnterpriseKey)
   vCenterEnterpriseSolutions()
   vSphereClose()
   writeExcelData("PASS","D3")
   Else
   ConsoleWrite("Run Mode is set to NO")
   writeExcelData("SKIP","D3")
   EndIf
EndFunc

Func vCentStdLicenseRenew()

   ;Checking Run-Mode
   if $vCenterStdRunMode=="Y" Then
   vSphereLogin()
   ;clicking on component
   MouseClick("right",208,269,1,0)
   _FileWriteLog(@ScriptDir & "\vChargeBack.log", "Clicked on vchargeback option",1)
   vSphereLicenseRenew($vCenterStdKey)
   vCenterStdSolutions()
   vSphereClose()
   writeExcelData("PASS","D4")
   Else
   ConsoleWrite("Run Mode is set to NO")
   writeExcelData("SKIP","D4")
   EndIf
EndFunc

Func vCenterServerLicenseRenew()

   ;Checking Run-Mode
   if $vCenterServer=="Y" Then
   vSphereLogin()
   ;clicking on component
   MouseClick("right",160,287,1,0)
   _FileWriteLog(@ScriptDir & "\vChargeBack.log", "Clicked on vchargeback option",1)
   vSphereLicenseRenew($vCenterServerKey)
   vCenterStdHostAddition()
   vSphereClose()
   writeExcelData("PASS","D5")
   Else
   ConsoleWrite("Run Mode is set to NO")
   writeExcelData("SKIP","D5")
   EndIf
EndFunc

Func vCloudLicenseRenew()

   ;Checking Run-Mode
   if $vCloudRunMode=="Y" Then
   vSphereLogin()
   ;clicking on component
   MouseClick("right",146,302,1,0)
   _FileWriteLog(@ScriptDir & "\vChargeBack.log", "Clicked on vchargeback option",1)
   vSphereLicenseRenew($vCloudKey)
   vCloudNetworkSolutions()
   vSphereClose()
   writeExcelData("PASS","D6")
   Else
   ConsoleWrite("Run Mode is set to NO")
   writeExcelData("SKIP","D6")
   EndIf
EndFunc

Func vSphereRenew()

   ;Checking Run-Mode
   if $vSphereRunMode=="Y" Then
   vSphereLogin()
   ;clicking on component
   MouseClick("right",188,324,1,0)
   _FileWriteLog(@ScriptDir & "\vChargeBack.log", "Clicked on vchargeback option",1)
   vSphereLicenseRenew($vSphereKey)
   vSphereLicenseHostAddition()
   vSphereClose()
   writeExcelData("PASS","D7")
   Else
   ConsoleWrite("Run Mode is set to NO")
   writeExcelData("SKIP","D7")
   EndIf
EndFunc

Func vcloudDirector()

   if $vCloudDirector=="Y" Then
   Run($chromeExePath)
   WinActivate("New Tab - Google Chrome")
   Sleep(5000)
   AutoItSetOption('MouseCoordMode',0)
   MouseClick("left",500,44,1,0)
   Send("{DELETE}")
   Send($vCloudPath)
   WinActivate("New Tab - Google Chrome")
   Send("{Enter}")
   Sleep(50000)
   Send($vCloudUserName)
   Sleep(5000)
   Send("{TAB}")
   ;Password for vcloud director cannot be kept dynamic due to special characters in the password
   Send("Start123")
   Send("{!}")
   ;Sleep(5000)
   ;Send("{TAB}")
   Sleep(5000)
   ;Send("{Enter}")
    MouseClick("left",273,846,1,0)
   WinActivate("VMware vCloud Director - Google Chrome")
   $hWnd1 = WinGetHandle("VMware vCloud Director - Google Chrome")
   WinSetState ( $hWnd1, "", @SW_MAXIMIZE )
   $hWnd2 = WinGetHandle("VMware vCloud Director - Google Chrome")

   Sleep(5000)
   WinWait("VMware vCloud Director - Google Chrome")
   WinActive("VMware vCloud Director - Google Chrome")
   Sleep(5000)
   MouseClick("left",300,160,1,0)
   Sleep(5000)
   MouseClick("left",88,459,1,0)
   Sleep(5000)
   Send("{TAB}")
   Send("{DELETE}")
   Send($vCloudDirectorKey)
   Sleep(5000)
   MouseClick("left",1771,981,1,0)
   ;Send("{Enter}")
   Sleep(8000)
   MouseClick("left",1334,89,1,0)
   Sleep(500)
   WinClose ("VMware vCloud Director - Google Chrome")
   writeExcelData("PASS","D8")
   Else
   ConsoleWrite("Run Mode is set to NO")
   writeExcelData("SKIP","D8")
   EndIf
EndFunc

Func vCACApplicance()
   if $vCacApplication=="Y" Then
   Run($chromeExePath)
   $hWnd1 = WinGetHandle("New Tab - Google Chrome")
   WinSetState ( $hWnd1, "", @SW_MAXIMIZE )
   Sleep(5000)
   AutoItSetOption('MouseCoordMode',0)
   MouseClick("left",500,44,1,0)
   Send("{DELETE}")
   Send($vcacPath)
   Send("{Enter}")
   Sleep(5000)
   MouseClick("left",714,515,1,0)
   Sleep(5000)
   MouseClick("left",748,681,1,0)
   Sleep(6000)
   WinActivate("VMware vRealize Appliance - Google Chrome")
   $hWnd2 = WinGetHandle("VMware vRealize Appliance - Google Chrome")
   MouseClick("left",945,202,1,0)
   Sleep(6000)
   send($vcacUserName)
   Send("{TAB}")
   send($vcacPassword)
   Send("{ENTER}")
   Sleep(5000)
   WinSetState ( $hWnd2, "", @SW_MAXIMIZE )
   MouseClick("left",788,118,1,0)
   Sleep(5000)
   MouseClick("left",792,156,1,0)
    Sleep(9000)
	MouseClick("left",726,229,1,0)
	Sleep(5000)
   send($vCacApplicationKey)
   Sleep(5000)
   MouseClick("left",1297,245,1,0)
   sleep(15000)
   MouseClick("left",1327,120,1,0)
   WinClose ("VMware vRealize Appliance - Google Chrome")
   writeExcelData("PASS","D9")
   Else
   ConsoleWrite("Run Mode is set to NO")
   writeExcelData("SKIP","D9")
   EndIf
EndFunc

#cs
Func hyperic()
ConsoleWrite($hypericPath)
ConsoleWrite($hypericHost)
   if $hyperic =="Y" Then
 Run($hypericPath)
WinActive("PuTTY Configuration")
$hWnd = WinGetHandle("PuTTY Configuration")
WinSetState ($hWnd , "", @SW_ENABLE )
WinWaitActive("PuTTY Configuration")
WinSetState ($hWnd , "", @SW_ENABLE )
WinWaitActive("PuTTY Configuration")
sleep(5000)
   Send($hypericHost)
   AutoItSetOption('MouseCoordMode',0)
   MouseClick("left",319,428,1,0)
   sleep(5000)
   WinActivate("BEDTAMS064.dta.inf - PuTTY")
   Send($hypericUserName)
   Send("{ENTER}")
   Send("VMware11")
   Send("{@}")
    Send("{!}")
   Send("{ENTER}")
   sleep(5000)
   Send($hypericDir)
   sleep(5000)
   Send("{ENTER}")
   sleep(5000)
   Send($ls)
   Send("{ENTER}")
   sleep(5000)
   Send($hypericFile)
   sleep(5000)
   Send("{ENTER}")
   sleep(5000)
   send ($E)
   sleep(5000)
   Send("{ENTER}")
   sleep(5000)
   send ("E")
   sleep(5000)
   ;send ($I)
   sleep(5000)
   send("/vcops.license.key=")
   Send("{ENTER}")
   send("dd")
;Send("{UP}")

   send ($I)
   Send ($hyperickey)

   Send("{ESCAPE}")

   Send("{:}")
   Send("wq")
   Send("{!}")

   Send("{ENTER}")
   Send("su hyperic")
    Send("{ENTER}")
   Send("cd ..")
    Send("{ENTER}")
   Send("cd bin/")
    Send("{ENTER}")
   Send($ls)
   Send("{ENTER}")
   Send("sh hq-server.sh stop")
   Send("{ENTER}")

Send("sh hq-server.sh start")

   Send("{ENTER}")

   Send("sh hq-server.sh status")
    Send("{ENTER}")
  ;WinClose ("BEDTAMS064.dta.inf - PuTTY")

  ;Send("{ENTER}")
  ;WinClose ("PuTTY Configuration")
   writeExcelData("PASS","D2")
   Else
   ConsoleWrite("Run Mode is set to NO")
   writeExcelData("SKIP","D2")

   EndIf

EndFunc
#ce

;End of the Script


