
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:   Shraddha Bhongle & Dinesh Ahuja
 Purpose : Script will automatically call all the function for renew license key
 Date :    29/04/2016


#ce ----------------------------------------------------------------------------

#include <vChargeBack.au3>

;Calling common fucntions
checkExcelSheet()
readExcelData()





;calling functions for vChargeBack Component
writeExcelData("FAIL","D2")
vChargeBackLicenseRenew()

;calling functions for vCenterEnterprise Component
writeExcelData("FAIL","D3")
vCenterEnterpriseLicenseRenew()

;calling functions for vCentStd Component
writeExcelData("FAIL","D4")
vCentStdLicenseRenew()

;calling functions for vCenterLicense Component
writeExcelData("FAIL","D5")
vCenterServerLicenseRenew()

;calling functions for vCloud Component
writeExcelData("FAIL","D6")
vCloudLicenseRenew()

;calling functions for Vsphere Component
writeExcelData("FAIL","D7")
vSphereRenew()

;calling functions for vCACApplicance Component
writeExcelData("FAIL","D8")
vcloudDirector()

;calling functions for vCACApplicance Component
writeExcelData("FAIL","D9")
vCACApplicance()
#cs
writeExcelData("FAIL","D2");
hyperic()
#ce
;End of Script
