#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:   Shraddha Bhongle & Dinesh Ahuja
 Purpose : Script will automatically renew the license for different vSphere components.
 Date :    29/04/2016


#ce ----------------------------------------------------------------------------

#include <Excel.au3>
#include <File.au3>

;Declaring global variables for script
Global $sFilePath,$oExcel,$oWorkbook,$Key,$UserName,$Password,$vSpherePath,$vChargeBackRunMode,$vCenterEnterpriseRunMode,$vCenterStdRunMode,$vCenterServer,$vCloudRunMode,$vSphereRunMode,$vChargeBackKey,$vCenterEnterpriseKey,$vCacApplicationKey,$hyperickey,$hyperic,$vCacApplication,$vCenterStdKey,$vCenterServerKey,$vCloudKey,$vSphereKey,$vCloudDirector,$vCloudDirectorKey,$IP
Global $chromeExePath,$vCloudPath,$vCloudUserName,$vCloudPassword,$vcacPath,$vcacUserName,$vcacPassword,$hypericPath,$hypericUserName,$hypericPassword
Global $hypericHost,$hypericDir,$ls,$hypericFile,$E,$I,$hypericServiceStart,$hypericServiceStatus

;Function for checking availability of excel sheet
Func checkExcelSheet()
   ; This part will check for the existing excel sheet ;This file should already exist
   $sFilePath = @ScriptDir & "\TestData.xls"
   ConsoleWrite($sFilePath)
   _FileWriteLog(@ScriptDir & "\vChargeBack.log", $sFilePath,1)
   If Not FileExists($sFilePath) Then
	  MsgBox(16, '', 'Does NOT exists')
	  _FileWriteLog(@ScriptDir & "\vChargeBack.log", 'File Does NOT exists',1)
	  Exit
   EndIf
EndFunc

;Function for reading all the input data from excel sheet
Func readExcelData()
   ;Reading  data from excel (Key, UserName & Password)
   $oExcel = _Excel_Open()
   $oWorkbook = _Excel_BookOpen($oExcel, @ScriptDir & "\TestData.xls")

   $chromeExePath = _Excel_RangeRead($oWorkbook, "Creds", "C3",3)
   ConsoleWrite($chromeExePath)

   $vCloudPath = _Excel_RangeRead($oWorkbook, "Creds", "C4",3)
   ConsoleWrite($vCloudPath)

   $vCloudUserName = _Excel_RangeRead($oWorkbook, "Creds", "A4",3)
   ConsoleWrite($vCloudUserName)

   $vCloudPassword = _Excel_RangeRead($oWorkbook, "Creds", "B4",3)
   ConsoleWrite($vCloudPassword)

   $vcacPath = _Excel_RangeRead($oWorkbook, "Creds", "C5",3)
   ConsoleWrite($vcacPath)

   $vcacUserName = _Excel_RangeRead($oWorkbook, "Creds", "A5",3)
   ConsoleWrite($vcacUserName)

   $vcacPassword = _Excel_RangeRead($oWorkbook, "Creds", "B5",3)
   ConsoleWrite($vcacPassword)

   $hypericPath = _Excel_RangeRead($oWorkbook, "Creds", "C6",3)
   ConsoleWrite($hypericPath)

   $hypericUserName = _Excel_RangeRead($oWorkbook, "Creds", "A6",3)
   ConsoleWrite($hypericUserName)

   $hypericPassword = _Excel_RangeRead($oWorkbook, "Creds", "B6",3)
   ConsoleWrite($hypericPassword)

   $hypericHost = _Excel_RangeRead($oWorkbook, "Creds", "C7",3)
   ConsoleWrite($hypericHost)

   $hypericDir = _Excel_RangeRead($oWorkbook, "Creds", "C8",3)
   ConsoleWrite($hypericDir)

   $ls = _Excel_RangeRead($oWorkbook, "Creds", "C9",3)
   ConsoleWrite($ls)

   $hypericFile = _Excel_RangeRead($oWorkbook, "Creds", "C10",3)
   ConsoleWrite($hypericFile)

   $E = _Excel_RangeRead($oWorkbook, "Creds", "C11",3)
   ConsoleWrite($E)

   $I = _Excel_RangeRead($oWorkbook, "Creds", "C12",3)
   ConsoleWrite($I)



   $vChargeBackKey = _Excel_RangeRead($oWorkbook, "Test", "B2",3)
   ConsoleWrite($vChargeBackKey)
   $vCenterEnterpriseKey = _Excel_RangeRead($oWorkbook, "Test", "B3",3)
   ConsoleWrite($vCenterEnterpriseKey)
   $vCenterStdKey = _Excel_RangeRead($oWorkbook, "Test", "B4",3)
   ConsoleWrite($vCenterStdKey)
   $vCenterServerKey = _Excel_RangeRead($oWorkbook, "Test", "B5",3)
   ConsoleWrite($vCenterServerKey)
   $vCloudKey = _Excel_RangeRead($oWorkbook, "Test", "B6",3)
   ConsoleWrite($vCloudKey)
   $vSphereKey = _Excel_RangeRead($oWorkbook, "Test", "B7",3)
   ConsoleWrite($vSphereKey)
   $vCloudDirectorKey = _Excel_RangeRead($oWorkbook, "Test", "B8",3)
   ConsoleWrite($vCloudDirectorKey)
   $vCacApplicationKey= _Excel_RangeRead($oWorkbook, "Test", "B9",3)
   ConsoleWrite($vCacApplicationKey)
   $hyperickey=_Excel_RangeRead($oWorkbook, "Hyperic", "B2",3)
   ConsoleWrite($hyperickey)
   $UserName = _Excel_RangeRead($oWorkbook, "Creds", "A2",3)
   ConsoleWrite($UserName)
   $Password = _Excel_RangeRead($oWorkbook, "Creds", "B2",3)
   ConsoleWrite($Password)
   $vSpherePath = _Excel_RangeRead($oWorkbook, "Creds", "C2",3)
   ConsoleWrite($vSpherePath)
   $IP = _Excel_RangeRead($oWorkbook, "Creds", "D2",3)
   ConsoleWrite($IP)

   ;Reading Run-Mode for all components
   $vChargeBackRunMode = _Excel_RangeRead($oWorkbook, "Test", "C2",3)
   ConsoleWrite($vChargeBackRunMode)
   $vCenterEnterpriseRunMode = _Excel_RangeRead($oWorkbook, "Test", "C3",3)
   ConsoleWrite($vCenterEnterpriseRunMode)
   $vCenterStdRunMode = _Excel_RangeRead($oWorkbook, "Test", "C4",3)
   ConsoleWrite($vCenterStdRunMode)
   $vCenterServer = _Excel_RangeRead($oWorkbook, "Test", "C5",3)
   ConsoleWrite($vCenterServer)
   $vCloudRunMode = _Excel_RangeRead($oWorkbook, "Test", "C6",3)
   ConsoleWrite($vCloudRunMode)
   $vSphereRunMode = _Excel_RangeRead($oWorkbook, "Test", "C7",3)
   ConsoleWrite($vSphereRunMode)
   $vCloudDirector= _Excel_RangeRead($oWorkbook, "Test", "C8",3)
   ConsoleWrite($vCloudDirector)
   $vCacApplication= _Excel_RangeRead($oWorkbook, "Test", "C9",3)
   ConsoleWrite($vCacApplication)
   $hyperic = _Excel_RangeRead($oWorkbook, "Hyperic", "C2",3)
   ConsoleWrite($hyperic)
   WinWait("Microsoft Excel - TestData  [Compatibility Mode]")
   WinActive("Microsoft Excel - TestData  [Compatibility Mode]")
   ConsoleWrite("foudn sheet")
   _FileWriteLog(@ScriptDir & "\vChargeBack.log", "Sheet Found",1)
   $hWnd = WinGetHandle("Microsoft Excel - TestData  [Compatibility Mode]")
   WinSetState ($hWnd , "", @SW_ENABLE )
   WinSetState ( $hWnd, "", @SW_MAXIMIZE )
   MouseClick("left",34,42,1,0)
   MouseClick("left",65,74,1,0)
   sleep(5000)
   MouseClick("left",34,42,1,0)
   MouseClick("left",53,447,1,0)
EndFunc

Func writeExcelData($Result,$cellNumber)
   $oExcel = _Excel_Open()
   $oWorkbook = _Excel_BookOpen($oExcel, @ScriptDir & "\TestData.xls")
   WinWait("Microsoft Excel - TestData  [Compatibility Mode]")
   WinActive("Microsoft Excel - TestData  [Compatibility Mode]")
   ConsoleWrite("foudn sheet")
   _FileWriteLog(@ScriptDir & "\vChargeBack.log", "Sheet Found",1)
	$hWnd = WinGetHandle("Microsoft Excel - TestData  [Compatibility Mode]")
   WinSetState ($hWnd , "", @SW_ENABLE )
   _Excel_RangeWrite($oWorkbook, "Test", $Result,$cellNumber)
    _Excel_RangeWrite($oWorkbook, "Hyperic", $Result,"D2")
   MouseClick("left",34,42,1,0)
   MouseClick("left",65,74,1,0)
   sleep(5000)
   MouseClick("left",34,42,1,0)
   MouseClick("left",53,447,1,0)
EndFunc


Func vSphereLogin()
   ;Execution of vSphere Component
   Run($vSpherePath)
   ; WinWaitActive("VMware vSphere Client")
   Sleep(5000)
   WinWait("VMware vSphere Client")
   WinActive("VMware vSphere Client")
   $hWnd = WinGetHandle("VMware vSphere Client")
   WinSetState ($hWnd , "", @SW_ENABLE )
   AutoItSetOption('MouseCoordMode',0)
   MouseClick("left",289,380,1,0)
   ;Send("{LCTRL}" + "{A}")
   Send("{DELETE}")
   Send($IP)
   Send("{TAB}")
   Send($UserName)
   Send("{TAB}")
   Send($Password)
  ; send("{TAB}")
  ; send("{TAB}")
   ;send("{ENTER}")
   ;233501
   AutoItSetOption('MouseCoordMode',0)
   MouseClick("left",233,501,1,0)
   Sleep(5000)
   WinWait("Security Warning")
   WinActive("Security Warning")
   $hWnd = WinGetHandle("Security Warning")
   MouseClick("left",332,151,1,0)
   ; ControlClick($hWnd,"Ignore")
   Sleep(1000)
   ;WinWaitActive("Security Warning")
   Sleep(5000)
   WinWait("Security Warning")
   WinActive("Security Warning")
   $hWnd = WinGetHandle("Security Warning")
   MouseClick("left",353,170,1,0)
   Sleep(1000)
   ;WinWaitActive("Security Warning")
   Sleep(5000)
   WinWait("Security Warning")
   WinActive("Security Warning")
   $hWnd = WinGetHandle("Security Warning")
   MouseClick("left",327,216,1,0)
   Sleep(5000)
   WinWaitActive("bedtams001.dta.inf - vSphere Client")
   $hWnd1 = WinGetHandle("bedtams001.dta.inf - vSphere Client")
   WinSetState ( $hWnd1, "", @SW_MAXIMIZE )
   $hWnd2 = WinGetHandle("bedtams001.dta.inf - vSphere Client")
   AutoItSetOption('MouseCoordMode',0)
   Sleep(5000)
   WinWait("bedtams001.dta.inf - vSphere Client")
   WinActive("bedtams001.dta.inf - vSphere Client")
   Sleep(5000)

   ;clicking on home option
   MouseClick("left",134,70,1,0)

   _FileWriteLog(@ScriptDir & "\vChargeBack.log", "Clicked on home button",1)
   Sleep(5000)
   WinWait("bedtams001.dta.inf - vSphere Client")
   WinActive("bedtams001.dta.inf - vSphere Client")
   Sleep(5000)

   ;clicking on license option on home page
   MouseClick("left",264,273,1,0)
   _FileWriteLog(@ScriptDir & "\vChargeBack.log", "Clicked on license button on homepage",1)
   Sleep(5000)
   WinWait("bedtams001.dta.inf - vSphere Client")
   WinActive("bedtams001.dta.inf - vSphere Client")
   Sleep(5000)
EndFunc

Func vSphereLicenseRenew($Key)

   send("{TAB}")
   send("{ENTER}")
  Sleep(5000)
   WinWait("Manage vSphere Licenses")
   WinActive("Manage vSphere Licenses")
   send("{TAB}")
   send("{TAB}")
   send("{TAB}")
   Sleep(5000)
   WinWait("Manage vSphere Licenses")
   WinActive("Manage vSphere Licenses")
   send($Key)
   send("{TAB}")
   send("Evaluation_Licenses")
   ;clicking on add key button
   MouseClick("left",450,280,1,0)
   Sleep(5000)
   WinWait("Manage vSphere Licenses")
   WinActive("Manage vSphere Licenses")
   Sleep(5000)
   ;clicking on next buttton
   MouseClick("left",657,564,1,0)
   ;send("{TAB}")
   ;send("{ENTER}")
   ;Selecting showlicense option
   WinWait("Manage vSphere Licenses")
   WinActive("Manage vSphere Licenses")
   Sleep(5000)
   MouseClick("left",314,116,1,0)
   Sleep(5000)
EndFunc

;The functions below select different option specfic to applications
 Func vChargeBackAddition()
   ;Selecting solution option
   MouseClick("left",358,144,1,0)
   sleep(5000)
   ;Selecting the chargeback option
   MouseClick("left",261,263,1,0)
   Sleep(5000)
   ;Selecting the new key
   MouseClick("left",479,261,1,0)
   Sleep(5000)
   ;next
   MouseClick("left",643,563,1,0)
   sleep(5000)
 EndFunc

 Func vCloudNetworkSolutions()
   ;selecting solutions option
   MouseClick("left",358,144,1,0)
   sleep(5000)
   ;Selecting the vcloudnetwork option
   MouseClick("left",279,226,1,0)
   Sleep(5000)
   ;Selecting the new key
   MouseClick("left",481,280,1,0)
   Sleep(5000)
   MouseClick("left",643,563,1,0)
   sleep(5000)
EndFunc

Func vCenterStdHostAddition()
   ;selecting vCenter
   MouseClick("left",265,143,1,0)
   sleep(5000)
   ;Selecting the 1st server
   MouseClick("left",291,210,1,0)
   sleep(5000)
   MouseClick("left",479,281,1,0)
   sleep(5000)
   ;Selecting the 2nd server
   MouseClick("left",264,228,1,0)
   sleep(5000)
   MouseClick("left",479,281,1,0)
   sleep(5000)
   ;next
   MouseClick("left",643,563,1,0)
   sleep(5000)
EndFunc

Func vCenterEnterpriseSolutions()
   ;selecting solutions option
   MouseClick("left",358,144,1,0)
   sleep(5000)
   ;Selecting the vChargeOperationEnt
   MouseClick("left",268,260,1,0)
   sleep(5000)
   ;Selecting the new key for Enrerprise edition
   MouseClick("left",480,316,1,0)
   Sleep(5000)
   ;next
   MouseClick("left",643,563,1,0)
   sleep(5000)
EndFunc

Func vCenterStdSolutions()
   ;selecting solutions option
   MouseClick("left",358,144,1,0)
   sleep(5000)
   ;Selecting the vChargeOperationEnt
   MouseClick("left",268,260,1,0)
   sleep(5000)
   ;Selecting the new key for Standard edition
   MouseClick("left",480,279,1,0)
   Sleep(5000)
   ;next
   MouseClick("left",643,563,1,0)
   sleep(5000)
EndFunc

Func vSphereLicenseHostAddition()
   ;Selecting EXI host
   MouseClick("left",195,143,1,0)
   sleep(5000)
   ;Than selecting the new license key for each host seperately
   ;1 host
   MouseClick("left",245,215,1,0)
   sleep(5000)
   MouseClick("left",481,287,1,0)
   sleep(5000)
   ;2 host
   MouseClick("left",248,233,1,0)
   sleep(5000)
   MouseClick("left",481,287,1,0)
   sleep(5000)
   ;3 host
   MouseClick("left",268,252,1,0)
   sleep(500)
    MouseClick("left",481,287,1,0)
   sleep(5000)
   ;4 host
   MouseClick("left",249,268,1,0)
   sleep(5000)
    MouseClick("left",481,287,1,0)
   sleep(5000)
   ;5 host
   MouseClick("left",246,287,1,0)
   sleep(5000)
   MouseClick("left",481,287,1,0)
   sleep(5000)
   ;6 host
   MouseClick("left",234,305,1,0)
   sleep(5000)
  MouseClick("left",481,287,1,0)
   sleep(5000)
   ;7 host
   MouseClick("left",232,323,1,0)
   sleep(5000)
   MouseClick("left",481,287,1,0)
   sleep(5000)
   ;8 host
   MouseClick("left",230,342,1,0)
   sleep(5000)
   MouseClick("left",481,287,1,0)
   sleep(5000)
   ;9 host
   MouseClick("left",228,357,1,0)
   sleep(5000)
  MouseClick("left",481,287,1,0)
   sleep(5000)
   ;10 host
   MouseClick("left",228,378,1,0)
   sleep(5000)
   MouseClick("left",481,287,1,0)
   sleep(5000)
   ;11 host
   MouseClick("left",233,394,1,0)
   sleep(5000)
    MouseClick("left",481,287,1,0)
   sleep(5000)
   ;12 host
   MouseClick("left",238,394,1,0)
   sleep(5000)
  MouseClick("left",481,287,1,0)
   sleep(5000)
   ;13 host
   MouseClick("left",238,394,1,0)
   sleep(5000)
   MouseClick("left",481,287,1,0)
   sleep(5000)
   ;next
   MouseClick("left",643,563,1,0)
   sleep(5000)
EndFunc


Func vSphereClose()
   MouseClick("left",662,565,1,0)
   sleep(5000)
    MouseClick("left",662,565,1,0)
   sleep(8000)
   WinWait("Manage vSphere Licenses")
   WinActive("Manage vSphere Licenses")
   ;send("{ENTER}")
   Sleep(6000)
   WinWait("Manage vSphere Licenses")
   WinActive("Manage vSphere Licenses")
   WinWait("Manage vSphere Licenses")
   WinActive("Manage vSphere Licenses")
   Sleep(6000)
   send('!+{F4}')
   Sleep(6000)
   WinWaitActive("bedtams001.dta.inf - vSphere Client")
   send('!+{F4}')
EndFunc