#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
Run("C:\Program Files (x86)\Google\Chrome\Application\chrome.exe")
   WinActivate("New Tab - Google Chrome")
   Sleep(5000)
   AutoItSetOption('MouseCoordMode',0)
   MouseClick("left",500,44,1,0)
   Send("{DELETE}")
   Send("https://emea01.dta.canopy-cloud.com/cloud")
   WinActivate("New Tab - Google Chrome")
   Send("{Enter}")
   Sleep(50000)
   Send("vcd-emea01-sys-admin")
   Sleep(5000)
   Send("{TAB}")
   ;Password for vcloud director cannot be kept dynamic due to special characters in the password
   Send("Start123")
   Send("{!}")
   ;Sleep(5000)
   ;Send("{TAB}")
   Sleep(5000)
   ;Send("{Enter}")
    MouseClick("left",273,846,1,0)
   WinActivate("VMware vCloud Director - Google Chrome")
   $hWnd1 = WinGetHandle("VMware vCloud Director - Google Chrome")
   WinSetState ( $hWnd1, "", @SW_MAXIMIZE )
   $hWnd2 = WinGetHandle("VMware vCloud Director - Google Chrome")

   Sleep(5000)
   WinWait("VMware vCloud Director - Google Chrome")
   WinActive("VMware vCloud Director - Google Chrome")
   AutoItSetOption('MouseCoordMode',0)
    MouseClick("left",1888,89,1,0)
   Sleep(500)
   WinClose ("VMware vCloud Director - Google Chrome")