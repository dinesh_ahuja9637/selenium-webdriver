package vCloudDirector;

import org.openqa.selenium.WebElement;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import base.TestBase;
import util.TestLog;
import util.ExcelReadAndWrite; 
import util.TestUtil;
 



public class EMEAAvailability  extends TestBase {
	boolean VDCResult=false;
	
	String systemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;
	
	boolean testskip=false;
	

	
	boolean skip =false;
	
	@AfterSuite
	public void generateBackupReport() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls");
	}

	@Test
	public void testCISTenant()throws InterruptedException, BiffException, IOException ,RowsExceededException, WriteException{
		
		
		
		if(!TestUtil.isSuiteExecutable("VDCAvailability"))
		{
			testskip=true;
			TestLog.info("Skipping the execution of VDCAvailability Suite as RunMode is Set to No");
			throw new SkipException("Runmode of VDCAvailability suite set to no. So Skipping all tests in CIS_Template_Creation suite");
		}
			openBrowser();
    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
    driver.get(TestBase.fn_getproperty("EMEA"));
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
    Thread.sleep(15000);
    List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));
    Thread.sleep(5000L);
	driver.switchTo().frame(ele1.size()-1);
    WebDriverWait wait = new WebDriverWait(driver, 60);
    WebElement requestorUsername= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("emea_req_username"))));
    requestorUsername.sendKeys(TestBase.fn_getproperty("EMEA_username"));
    WebElement requestorPassword= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("emea_req_password"))));
    requestorPassword.sendKeys(TestBase.fn_getproperty("EMEA_password"));
   
    Thread.sleep(15000);
    driver.findElement(By.xpath(TestBase.or_getproperty("login_button"))).click();
    Thread.sleep(3000L); 
    /*
    Runtime.getRuntime().exec("C:\\Users\\A611037\\Desktop\\Automation-monitoring\\monitoring\\Automation_Monitoring\\src\\vCloudDirector\\New AutoIt v3 Script.exe");
    Runtime.getRuntime().exec("C:\\Users\\A611037\\Desktop\\Automation-monitoring\\monitoring\\Automation_Monitoring\\src\\vCloudDirector\\verify.exe");
   WebElement vcdapplication= wait.until((ExpectedConditions.visibilityOfElementLocated(By.xpath(TestBase.or_getproperty("vcdxpath")))));
    
    if(vcdapplication.isDisplayed()){
   	 System.out.println("Login sucessful"); 
   	
   	Assert.assertTrue(true,"login sucessful");
   	 
    }
    else{
    	VDCResult=true;
    	Assert.assertFalse(true,"Error in login");
    }
    */
    
	}

	
	@AfterMethod(alwaysRun=true)
	public void generateReport()
	{
		if(driver!=null){
			closeBrowser();
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		systemTime=""+ dateFormat.format(cal.getTime());
		excelRowCount++;
		//System.out.println(skip +"123ssssssssssss");
		
		if(!testskip){
	
			if(!skip)
			{
				if(VDCResult)
				{
				
				test_data_xls.setCellData("VDCAvailability", "Result", 2, "FAIL");
				test_data_xls.setCellData("VDCAvailability", "Time", 2, systemTime);
				test_data_xls.setCellData("VDCAvailability", "Screenshot", 2, excelScreenshotPath);
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				}
				 else
			    {
				test_data_xls.setCellData("VDCAvailability", "Result", 2, "PASS");
				test_data_xls.setCellData("VDCAvailability", "Time", 2, systemTime);
			
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			    }
		}
		else
		{
			test_data_xls.setCellData("VDCAvailability", "Result", 2, "SKIP");
			test_data_xls.setCellData("VDCAvailability", "Time", 2, systemTime);
			//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
		}
			
		
}
	}
}
