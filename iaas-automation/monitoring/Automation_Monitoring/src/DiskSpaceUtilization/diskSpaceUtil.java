package DiskSpaceUtilization;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.profesorfalken.jpowershell.PowerShell;
import com.profesorfalken.jpowershell.PowerShellNotAvailableException;
import com.profesorfalken.jpowershell.PowerShellResponse;

import base.TestBase;
import util.ExcelReadAndWrite;
import util.TestLog;
import util.TestUtil;

public class diskSpaceUtil extends TestBase {
	
	String systemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;
	String action=null;
	boolean skip;
	boolean demStatus=false;
	double cpuusage;
	double memoryusage;
	double cdiskspace;
	double ddiskspace;
	double ediskspace;
	boolean cpuusageresult=false;
	boolean memoryusageresult=false;
	boolean cdiskspaceresult =false;
	boolean ddiskspaceresult=false;
	boolean ediskspaceresult=false;
	boolean testskip=false;
	public static SoftAssert softassert = new SoftAssert();
	
	@AfterSuite
	public void generateBackupReport() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls");
	}

	@Test
	public void diskspace() throws PowerShellNotAvailableException{
		
		
		if(!TestUtil.isSuiteExecutable("DiskSpaceUtilization"))
		{
			testskip=true;
			TestLog.info("Skipping the execution of Disk Space Utilization Suite as RunMode is Set to No");
			throw new SkipException("Runmode of Disk Space Utilization suite set to no. So Skipping all tests in CIS_Template_Creation suite");
		}
	PowerShell powerShell = null;
	powerShell = PowerShell.openSession();

	//Print results    
	//System.out.println(powerShell.executeCommand("C:\\Users\\A611037\\Desktop\\Automation-monitoring\\CPUUtilization.ps1").getCommandOutput());
	PowerShellResponse response = powerShell.executeCommand("C:\\Users\\A611037\\Desktop\\Automation-monitoring\\cpu.ps1");
	String cpu=response.getCommandOutput();
	
  
   cpuusage =Double.parseDouble(cpu.replaceAll("[^\\d.]", ""));
   System.out.println("CPU: "+cpuusage);
   
   
    response = powerShell.executeCommand("C:\\Users\\A611037\\Desktop\\Automation-monitoring\\memory.ps1" );
    String memory=response.getCommandOutput();
  memoryusage=Double.parseDouble(memory.replaceAll("[^\\d.]", ""));
    System.out.println("Memory: "+memoryusage);
	
  
    
    response = powerShell.executeCommand("C:\\Users\\A611037\\Desktop\\Automation-monitoring\\CDriveDisk.ps1" );
    String cdisk=response.getCommandOutput();
    cdiskspace=Double.parseDouble(cdisk.replaceAll("[^\\d.]", ""));
    System.out.println("C: "+cdiskspace);
   
    response = powerShell.executeCommand("C:\\Users\\A611037\\Desktop\\Automation-monitoring\\DDriveDisk.ps1" );
    String ddisk=response.getCommandOutput();
     ddiskspace=Double.parseDouble(ddisk.replaceAll("[^\\d.]", ""));
    System.out.println("D: "+ddiskspace);
    
    response = powerShell.executeCommand("C:\\Users\\A611037\\Desktop\\Automation-monitoring\\EdriveDisk.ps1" );
    String edisk=response.getCommandOutput();
    ediskspace=Double.parseDouble(edisk.replaceAll("[^\\d.]", ""));
    System.out.println("E: "+ediskspace);
    
    if (cpuusage<80.0) 
	{
    	
		TestLog.info("CPU Usage"+cpuusage);
		softassert.assertTrue(true, "CPU Usage"+cpuusage);
	} 
    else
    {
    			cpuusageresult=true;
    			TestLog.info("CPU Usage more than 80%");
    			softassert.assertFalse(true, "CPU Usage more than 80%");
    	
    }
    
    
    if (memoryusage<98.0) 
	{
    	
		TestLog.info("Memory Usage"+memoryusage);
		softassert.assertTrue(true, "Memory Usage"+memoryusage);
	} 
    else
    {
    			memoryusageresult=true;
    			TestLog.info("Memory Usage more than 98%");
    			softassert.assertFalse(true, "Memory Usage more than 98%");
    	
    }
    
    if (cdiskspace>5.0) 
	{
    	
		TestLog.info("C Drive free space:"+cdiskspace);
		softassert.assertTrue(true, "C Drive free space:"+cdiskspace);
	} 
    else
    {
    			cdiskspaceresult=true;
    			TestLog.info("C Drive: Less than 5 GB free space available");
    			softassert.assertFalse(true, "C Drive: Less than 5 GB free space available");
    	
    }
    
    if (ddiskspace>5.0) 
	{
    	
		TestLog.info("D Drive free space:"+ddiskspace);
		softassert.assertTrue(true, "D Drive free space:"+ddiskspace);
	} 
    else
    {
    			ddiskspaceresult=true;
    			TestLog.info("D Drive: Less than 5 GB free space available");
    			softassert.assertFalse(true, "D Drive: Less than 5 GB free space available");
    	
    }
    if (ediskspace>5.0) 
	{
    	
		TestLog.info("E Drive free space:"+ediskspace);
		softassert.assertTrue(true, "E Drive free space:"+ediskspace);
	} 
    else
    {
    			ediskspaceresult=true;
    			TestLog.info("E Drive: Less than 5 GB free space available");
    			softassert.assertFalse(true, "E Drive: Less than 5 GB free space available");
    	
    }
  
    
    
    powerShell.close();
	}
	
	@AfterMethod(alwaysRun=true)
	public void generateReport()
	{
		if(driver!=null){
			closeBrowser();
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		systemTime=""+ dateFormat.format(cal.getTime());
		excelRowCount++;
		if(!testskip){
		//System.out.println(skip +"123ssssssssssss");
		
			
				if(cpuusageresult)
				{
				
				test_data_xls.setCellData("DiskSpaceUtilization", "Result", 2, "FAIL");
				test_data_xls.setCellData("DiskSpaceUtilization", "Time", 2, systemTime);
				test_data_xls.setCellData("DiskSpaceUtilization", "Value", 2, String.valueOf(cpuusage));
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				}
				 else
			    {
				test_data_xls.setCellData("DiskSpaceUtilization", "Result", 2, "PASS");
				test_data_xls.setCellData("DiskSpaceUtilization", "Time", 2, systemTime);
				test_data_xls.setCellData("DiskSpaceUtilization", "Value", 2, String.valueOf(cpuusage));
			
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			    }
				
				
				
				if(memoryusageresult)
				{
				
				test_data_xls.setCellData("DiskSpaceUtilization", "Result", 3, "FAIL");
				test_data_xls.setCellData("DiskSpaceUtilization", "Time", 3, systemTime);
				test_data_xls.setCellData("DiskSpaceUtilization", "Value", 3, String.valueOf(memoryusage));
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				}
				 else
			    {
				test_data_xls.setCellData("DiskSpaceUtilization", "Result", 3, "PASS");
				test_data_xls.setCellData("DiskSpaceUtilization", "Time", 3, systemTime);
				test_data_xls.setCellData("DiskSpaceUtilization", "Value", 3, String.valueOf(memoryusage));
			
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			    }
				if(cdiskspaceresult)
				{
				
				test_data_xls.setCellData("DiskSpaceUtilization", "Result", 4, "FAIL");
				test_data_xls.setCellData("DiskSpaceUtilization", "Time", 4, systemTime);
				test_data_xls.setCellData("DiskSpaceUtilization", "Value", 4, String.valueOf(cdiskspace));
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				}
				 else
			    {
				test_data_xls.setCellData("DiskSpaceUtilization", "Result", 4, "PASS");
				test_data_xls.setCellData("DiskSpaceUtilization", "Time", 4, systemTime);
				test_data_xls.setCellData("DiskSpaceUtilization", "Value", 4, String.valueOf(cdiskspace));
			
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			    }
				
				if(ddiskspaceresult)
				{
				
				test_data_xls.setCellData("DiskSpaceUtilization", "Result", 5, "FAIL");
				test_data_xls.setCellData("DiskSpaceUtilization", "Time", 5, systemTime);
				test_data_xls.setCellData("DiskSpaceUtilization", "Value", 5, String.valueOf(ddiskspace));
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				}
				 else
			    {
				test_data_xls.setCellData("DiskSpaceUtilization", "Result", 5, "PASS");
				test_data_xls.setCellData("DiskSpaceUtilization", "Time", 5, systemTime);
				test_data_xls.setCellData("DiskSpaceUtilization", "Value", 5, String.valueOf(ddiskspace));
			
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			    }
				if(ediskspaceresult)
				{
				
				test_data_xls.setCellData("DiskSpaceUtilization", "Result", 6, "FAIL");
				test_data_xls.setCellData("DiskSpaceUtilization", "Time", 6, systemTime);
				test_data_xls.setCellData("DiskSpaceUtilization", "Value", 6, String.valueOf(ediskspace));
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				}
				 else
			    {
				test_data_xls.setCellData("DiskSpaceUtilization", "Result", 6, "PASS");
				test_data_xls.setCellData("DiskSpaceUtilization", "Time", 6, systemTime);
				test_data_xls.setCellData("DiskSpaceUtilization", "Value", 6, String.valueOf(ediskspace));
			
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			    }
				
				
				
				
		}
		
	}
		
		
	

}

