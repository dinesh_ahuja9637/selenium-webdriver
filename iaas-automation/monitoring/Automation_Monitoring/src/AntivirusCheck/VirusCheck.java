package AntivirusCheck;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import util.ExcelReadAndWrite;
import util.TestLog;
import util.TestUtil;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import base.TestBase;


public class VirusCheck  extends TestBase{
	String systemTime=null;
	boolean skip=false;
	boolean testskip=false;
	String excelScreenshotPath= null;
	
	@AfterSuite
	public void generateBackupReport() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls");
	}
	
	
	@Test
	public void antivirusCheck()  throws InterruptedException, BiffException, IOException ,RowsExceededException, WriteException{
		if(!TestUtil.isSuiteExecutable("Antivirus"))
		{
			testskip=true;
			TestLog.info("Skipping the execution of DEM Service Suite as RunMode is Set to No");
			throw new SkipException("Runmode of DEM Service suite set to no. So Skipping all tests in CIS_Template_Creation suite");
		}
		openBrowser();
		 driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
		    driver.get(TestBase.fn_getproperty("Antivirus"));
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
		    Thread.sleep(15000);
		    WebDriverWait wait = new WebDriverWait(driver, 60);
		    WebElement requestorUsername= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("username_antivirus"))));
		    requestorUsername.sendKeys(TestBase.fn_getproperty("requestor_username"));
		    WebElement requestorPassword= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("password_antivirus"))));
		    requestorPassword.sendKeys(TestBase.fn_getproperty("requestor_passwd"));
		    Thread.sleep(15000);
		   WebElement login = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("login_antivirus"))));
		   login.click();
		   Thread.sleep(3000L); 
		   
		   String compliance =wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("Msv_compliance")))).getText();
		   System.out.println(compliance);
		   WebElement status = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("Msv_compliance_status"))));
		   String msv_status= status.getText();
		   System.out.println(msv_status);
		  
		   String compliance1 =wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("Av_complianace")))).getText();
		   System.out.println(compliance1);
		  // Thread.sleep(15000);
		   String legend;
		  // String asv_status = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(Av_complianace_status,'"+id+"')]"))).getText();
		   String asv_status = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[id^='legend_com.mcafee.orion.core.query.QueryChartDataSourc']"))).getText();
		  // legend_com.mcafee.orion.core.query.QueryChartDataSource
		   System.out.println(asv_status);
		String ld = asv_status.split("\n")[1];
		String asvthreat= ld.split(" ")[1];
		System.out.println(asvthreat);
		  //int sde=Integer.parseInt();
		  
	
		   String asv_threat1 = "Threat= ";
		   String threat =  asv_threat1+asvthreat ; 
		   DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		   Calendar cal = Calendar.getInstance();
		   systemTime=""+ dateFormat.format(cal.getTime());
		   
		   if(msv_status.contains("Compliant")){
			   System.out.println("Pass");
			   System.out.println(compliance + "Pass");
			   test_data_xls.setCellData("viruscheck", "Compliancename", 2, compliance);
			   test_data_xls.setCellData("viruscheck", "Result", 2, "Pass");
			   test_data_xls.setCellData("viruscheck", "ExecutionTime", 2, systemTime);
			   test_data_xls.setCellData("viruscheck", "Status", 2, msv_status);
			   
		   }
		   else{
			   excelScreenshotPath=CaptureScreenshot("DEM is Offline"+msv_status);
			   test_data_xls.setCellData("viruscheck", "Compliancename", 2, compliance);
			   test_data_xls.setCellData("viruscheck", "Result", 2, "Fail");
			   test_data_xls.setCellData("viruscheck", "ExecutionTime", 2, systemTime);
			   test_data_xls.setCellData("viruscheck", "Status", 2, msv_status);
			   test_data_xls.setCellData("viruscheck", "Screenshot", 2, excelScreenshotPath);
		   }
	if(asvthreat.contains("0")){
			 test_data_xls.setCellData("viruscheck", "Compliancename", 3, compliance1);
			   test_data_xls.setCellData("viruscheck", "Result", 3, "Pass");
			   test_data_xls.setCellData("viruscheck", "ExecutionTime", 3, systemTime);
			   test_data_xls.setCellData("viruscheck", "Status", 3, threat);
			 
		}
		else{
			excelScreenshotPath=CaptureScreenshot("DEM is Offline"+threat);
			 test_data_xls.setCellData("viruscheck", "Compliancename", 3, compliance1);
			   test_data_xls.setCellData("viruscheck", "Result", 3, "Fail");
			   test_data_xls.setCellData("viruscheck", "ExecutionTime", 3, systemTime);
			   test_data_xls.setCellData("viruscheck", "Status", 3, threat);
			   test_data_xls.setCellData("viruscheck", "Screenshot", 3, excelScreenshotPath);
		
		}
		Thread.sleep(5000);
		driver.findElement(By.xpath(TestBase.or_getproperty("logout"))).click();
		closeBrowser();
	}
	
	
	
}
