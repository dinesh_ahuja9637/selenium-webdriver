package vCacHealthCheck;
import org.openqa.selenium.WebElement;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import base.TestBase;
import util.TestLog;
import util.ExcelReadAndWrite; 
import util.TestUtil;
public class VcacHealth extends TestBase{
	
boolean result=false;
	String CR = null;
	String systemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;
	boolean AgentStatus=false;
	boolean skip=false;
	boolean testskip=false;
	boolean ComplianceStatus=false;
	String agentstatus;
	String  st;
	
	@AfterSuite
	public void generateBackupReport() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls");
	}
	
	@Test(dataProvider= "getTestData")
	
	public void vcacchekup(String ComputeResource , String Status, String Result , String ExecutionTime, String Screenshoot) throws InterruptedException, BiffException, IOException ,RowsExceededException, WriteException{
		
		if(!TestUtil.isSuiteExecutable("vCACHealthCheck"))
		{
			testskip=true;
			TestLog.info("Skipping the execution of DEM Service Suite as RunMode is Set to No");
			throw new SkipException("Runmode of DEM Service suite set to no. So Skipping all tests in CIS_Template_Creation suite");
		}
		
		openBrowser();
		CR=ComputeResource;
		driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
	    driver.get(TestBase.fn_getproperty("vCAC"));
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
	    Thread.sleep(15000);
	    WebDriverWait wait = new WebDriverWait(driver, 60);
	    WebElement requestorUsername= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("req_username"))));
	    requestorUsername.sendKeys(TestBase.fn_getproperty("requestor_username"));
	    WebElement requestorPassword= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("req_password"))));
	    requestorPassword.sendKeys(TestBase.fn_getproperty("requestor_passwd"));
	    Thread.sleep(15000);
	    driver.findElement(By.id("submit")).click();
	    
	    
	    Thread.sleep(15000);
	    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    Thread.sleep(15000);
	    driver.findElement(By.xpath(TestBase.or_getproperty("infrastructure"))).click();
	    Thread.sleep(15000);
	    driver.switchTo().frame("__gadget_6");
		Thread.sleep(5000L);
		driver.findElement(By.xpath(TestBase.or_getproperty("Computeresource"))).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(TestBase.or_getproperty("Computeresource1"))).click();
		System.out.println("inside");
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		List<WebElement> ele12 = driver.findElements(By.tagName("iframe"));

		System.out.println("Number of frames in a page :" + ele12.size());
		for(WebElement el : ele12)
		{
			//Returns the Id of a frame.
			System.out.println("Frame Id :" + el.getAttribute("id"));
			
			//Returns the Name of a frame.
			System.out.println("Frame name :" + el.getAttribute("name"));
		}  
		System.out.println(ele12.size() );
		System.out.println(ele12.size()-2);

		driver.switchTo().frame(ele12.size()-2);
		

		for(int i=0;i<=1;i++){
		agentstatus=TestBase.or_getproperty("agentstatus1")+i+TestBase.or_getproperty("agentstatus2");
		System.out.println(agentstatus);
		driver.switchTo().defaultContent();
		Thread.sleep(5000);
		driver.switchTo().frame("__gadget_6");
		Thread.sleep(5000);
		driver.switchTo().frame("csp.places.iaas.iframe");
	      st = driver.findElement(By.xpath(agentstatus)).getText();
		System.out.println("AgentStatus"+st);
		
		
		String resourcename;
		resourcename=TestBase.or_getproperty("computename1")+i+TestBase.or_getproperty("computename2");
		System.out.println(resourcename);
		driver.switchTo().defaultContent();
		Thread.sleep(5000);
		driver.switchTo().frame("__gadget_6");
		Thread.sleep(5000);
		driver.switchTo().frame("csp.places.iaas.iframe");
		String  name = driver.findElement(By.xpath(resourcename)).getText();
		//agentstatus=driver.findElement(By.xpath(agentstatus)).getText();
		System.out.println("AgentStatus"+name);
		
		if(name.equalsIgnoreCase(CR)){
			
			if(st.contains("OK")){
				System.out.println(ComputeResource);
				System.out.println("pAss");
			}else{
				System.out.println("Fail");
				excelScreenshotPath=CaptureScreenshot("Status off");
				AgentStatus=true;
			}
			
		}
		else{
			System.out.println("fail");
		}
}
		
		
		 }
@DataProvider(name="getTestData")
public Object[][] testData() 
{
	Object[][] arrayObject = util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls","vCACHealthCheck");
	return arrayObject;                                                                                         
}
	
@AfterMethod(alwaysRun=true)
public void generateReport()
{
	if(driver!=null){
		closeBrowser();
	}
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
	Calendar cal = Calendar.getInstance();
	systemTime=""+ dateFormat.format(cal.getTime());
	excelRowCount++;
	
	if(!testskip){
	switch(CR){
	case "SHR-MEC-01-ESX01":
	if(!skip)
	{
		if(AgentStatus)
		{
			
			test_data_xls.setCellData("vCACHealthCheck", "Result", 2, "FAIL");
			test_data_xls.setCellData("vCACHealthCheck", "ExecutionTime", 2, systemTime);
			test_data_xls.setCellData("vCACHealthCheck", "Status", 2,st );
			test_data_xls.setCellData("vCACHealthCheck", "Screenshot", 2, excelScreenshotPath);
			//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
			
			
		}
		else
		{
			test_data_xls.setCellData("vCACHealthCheck", "Result", 2, "PASS");
			test_data_xls.setCellData("vCACHealthCheck", "ExecutionTime", 2, systemTime);
			test_data_xls.setCellData("vCACHealthCheck", "Status", 2,st);
			//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
			
		}
	}
	else
	{
		test_data_xls.setCellData("vCACHealthCheck", "Result", 2, "SKIP");
		test_data_xls.setCellData("vCACHealthCheck", "ExecutionTime", 2, systemTime);
		//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
	}
	
	case "TCA-MEC-01-ESX01":
		if(!skip)
		{
			if(AgentStatus)
			{
				
				test_data_xls.setCellData("vCACHealthCheck", "Result", 3, "FAIL");
				test_data_xls.setCellData("vCACHealthCheck", "ExecutionTime", 3, systemTime);
				test_data_xls.setCellData("vCACHealthCheck", "Status", 3,st );
				test_data_xls.setCellData("vCACHealthCheck", "Screenshot", 3, excelScreenshotPath);
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				
			}
			else
			{
				test_data_xls.setCellData("vCACHealthCheck", "Result", 3, "PASS");
				test_data_xls.setCellData("vCACHealthCheck", "ExecutionTime", 3, systemTime);
				test_data_xls.setCellData("vCACHealthCheck", "Status", 3,st);
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			}
		}
		else
		{
			test_data_xls.setCellData("vCACHealthCheck", "Result", 3, "SKIP");
			test_data_xls.setCellData("vCACHealthCheck", "ExecutionTime", 3, systemTime);
			//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
		}
		
	
		
		
		
		
	}
}
	/*else{
		test_data_xls.setCellData("Suite", "Result", 2, "SKIP");
		test_data_xls.setCellData("Suite", "Time", 2, systemTime);
	}*/
}


}