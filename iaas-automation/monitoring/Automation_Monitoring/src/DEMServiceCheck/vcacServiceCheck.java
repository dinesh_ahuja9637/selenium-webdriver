package DEMServiceCheck;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import util.ExcelReadAndWrite;
import util.TestLog;
import util.TestUtil;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import base.TestBase;

public class vcacServiceCheck extends TestBase{
	
	boolean result=false;
	
	String systemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;
	String DEM=null;
	boolean skip=false;
	boolean testskip=false;
	boolean demStatus=false;
	String serviceStatus;

	

	@AfterSuite
	public void generateBackupReport() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls");
	}
	
	
	@Test(dataProvider= "getTestData")
	public void vCacService(String Distributed_Execution_Manager,String Status,String DEMResult,String DEMTime,String Screenshot)throws InterruptedException, BiffException, IOException ,RowsExceededException, WriteException{
	
		if(!TestUtil.isSuiteExecutable("DEMService"))
		{
			testskip=true;
			TestLog.info("Skipping the execution of DEM Service Suite as RunMode is Set to No");
			throw new SkipException("Runmode of DEM Service suite set to no. So Skipping all tests in CIS_Template_Creation suite");
		}
	openBrowser();		
	DEM=Distributed_Execution_Manager;
    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
    driver.get(TestBase.fn_getproperty("vCAC"));
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
    Thread.sleep(15000);
    WebDriverWait wait = new WebDriverWait(driver, 60);
    WebElement requestorUsername= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("req_username"))));
    requestorUsername.sendKeys(TestBase.fn_getproperty("requestor_username"));
    WebElement requestorPassword= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("req_password"))));
    requestorPassword.sendKeys(TestBase.fn_getproperty("requestor_passwd"));
    Thread.sleep(15000);
    driver.findElement(By.id("submit")).click();
    Thread.sleep(3000L); 
    
    Thread.sleep(15000);
    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    Thread.sleep(15000);
    driver.findElement(By.xpath(TestBase.or_getproperty("infrastructure"))).click();
    Thread.sleep(15000);
    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
    Thread.sleep(15000);
    driver.switchTo().defaultContent();
    Thread.sleep(5000L);

	List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));


	Thread.sleep(5000L);
	driver.switchTo().frame(ele1.size()-1);
	Thread.sleep(5000L);
	 driver.findElement(By.xpath(TestBase.or_getproperty("monitoring"))).click();
	 
	


		Thread.sleep(5000L);
	
		
		driver.findElement(By.xpath(TestBase.or_getproperty("Distributed_Execution_Status"))).click();
		Thread.sleep(5000L);
		List<WebElement> ele12 = driver.findElements(By.tagName("iframe"));

		System.out.println("Number of frames in a page :" + ele12.size());
		for(WebElement el : ele12)
		{
			//Returns the Id of a frame.
			System.out.println("Frame Id :" + el.getAttribute("id"));
			
			//Returns the Name of a frame.
			System.out.println("Frame name :" + el.getAttribute("name"));
		}  

		Thread.sleep(5000L);
		//driver.switchTo().defaultContent();
		driver.switchTo().frame(ele12.size()-1);
		Thread.sleep(5000L);
		
		for(int i=0;i<=3;i++)
		{
			
			
			serviceStatus=TestBase.or_getproperty("DES_table_status1")+i+TestBase.or_getproperty("DES_table_status12");
			System.out.println("serviceStatus"+serviceStatus);
			Thread.sleep(5000L);
			serviceStatus=driver.findElement(By.xpath(serviceStatus)).getText();
			System.out.println(serviceStatus);
			
			String serviceName;
			serviceName=TestBase.or_getproperty("DES_table_machine1")+i+TestBase.or_getproperty("DES_table_machine12");
			System.out.println("serviceName"+serviceName);
			serviceName=driver.findElement(By.xpath(serviceName)).getText();
			if(serviceName.equalsIgnoreCase(DEM))
			{
				if (serviceStatus.contains("Online"))
				{
				System.out.println("Service Name"+serviceName);
				System.out.println("Pass");
				}
				else
				{
				System.out.println("Fail");
				excelScreenshotPath=CaptureScreenshot("DEM is Offline"+DEM);
				demStatus=true;
				}
				break;
		    }
			else{
				System.out.println("DEM not found");
				
			}
			
		}
	}
	@DataProvider(name="getTestData")
	public Object[][] testData() 
	{
		Object[][] arrayObject = util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls","DEMServiceResult");
		return arrayObject;                                                                                         
	}
	
	@AfterMethod(alwaysRun=true)
	public void generateReport()
	{
		if(driver!=null){
			closeBrowser();
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		systemTime=""+ dateFormat.format(cal.getTime());
		excelRowCount++;
		
		if(!testskip){
		switch(DEM){
		case "BE-MEC-DEM-ORC-01":
		if(!skip)
		{
			if(demStatus)
			{
				
				test_data_xls.setCellData("DEMServiceResult", "DEMResult", 2, "FAIL");
				test_data_xls.setCellData("DEMServiceResult", "DEMTime", 2, systemTime);
				test_data_xls.setCellData("DEMServiceResult", "Status", 2,serviceStatus );
				test_data_xls.setCellData("DEMServiceResult", "Screenshot", 2, excelScreenshotPath);
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				
			}
			else
			{
				test_data_xls.setCellData("DEMServiceResult", "DEMResult", 2, "PASS");
				test_data_xls.setCellData("DEMServiceResult", "DEMTime", 2, systemTime);
				test_data_xls.setCellData("DEMServiceResult", "Status", 2,serviceStatus);
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			}
		}
		else
		{
			test_data_xls.setCellData("DEMServiceResult", "DEMReport", 2, "SKIP");
			test_data_xls.setCellData("DEMServiceResult", "DEMTime", 2, systemTime);
			//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
		}
		
		case "BE-MEC-DEM-WRK-01":
			if(!skip)
			{
				if(demStatus)
				{
					
					test_data_xls.setCellData("DEMServiceResult", "DEMResult", 4, "FAIL");
					test_data_xls.setCellData("DEMServiceResult", "DEMTime", 4, systemTime);
					test_data_xls.setCellData("DEMServiceResult", "Status", 4,serviceStatus );
					test_data_xls.setCellData("DEMServiceResult", "Screenshot", 4, excelScreenshotPath);
					//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
					
					
				}
				else
				{
					test_data_xls.setCellData("DEMServiceResult", "DEMResult", 4, "PASS");
					test_data_xls.setCellData("DEMServiceResult", "DEMTime", 4, systemTime);
					test_data_xls.setCellData("DEMServiceResult", "Status", 4,serviceStatus);
					//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
					
				}
			}
			else
			{
				test_data_xls.setCellData("DEMServiceResult", "DEMReport", 4, "SKIP");
				test_data_xls.setCellData("DEMServiceResult", "DEMTime", 4, systemTime);
				//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
			}
			
		case "BE-MEC-DEM-WRK-02":
			if(!skip)
			{
				if(demStatus)
				{
					
					test_data_xls.setCellData("DEMServiceResult", "DEMResult", 5, "FAIL");
					test_data_xls.setCellData("DEMServiceResult", "DEMTime", 5, systemTime);
					test_data_xls.setCellData("DEMServiceResult", "Status", 5,serviceStatus );
					test_data_xls.setCellData("DEMServiceResult", "Screenshot", 5, excelScreenshotPath);
					//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
					
					
				}
				else
				{
					test_data_xls.setCellData("DEMServiceResult", "DEMResult", 5, "PASS");
					test_data_xls.setCellData("DEMServiceResult", "DEMTime", 5, systemTime);
					test_data_xls.setCellData("DEMServiceResult", "Status", 5,serviceStatus);
					//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
					
				}
			}
			else
			{
				test_data_xls.setCellData("DEMServiceResult", "DEMReport", 5, "SKIP");
				test_data_xls.setCellData("DEMServiceResult", "DEMTime", 5, systemTime);
				//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
			}
		case "BE-MEC-DEM-ORC-02":
			if(!skip)
			{
				if(demStatus)
				{
					
					test_data_xls.setCellData("DEMServiceResult", "DEMResult", 3, "FAIL");
					test_data_xls.setCellData("DEMServiceResult", "DEMTime", 3, systemTime);
					test_data_xls.setCellData("DEMServiceResult", "Status", 3,serviceStatus );
					test_data_xls.setCellData("DEMServiceResult", "Screenshot", 3, excelScreenshotPath);
					//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
					
					
				}
				else
				{
					test_data_xls.setCellData("DEMServiceResult", "DEMResult", 3, "PASS");
					test_data_xls.setCellData("DEMServiceResult", "DEMTime", 3, systemTime);
					test_data_xls.setCellData("DEMServiceResult", "Status", 3,serviceStatus);
					//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
					
				}
			}
			else
			{
				test_data_xls.setCellData("DEMServiceResult", "DEMReport", 3, "SKIP");
				test_data_xls.setCellData("DEMServiceResult", "DEMTime", 3, systemTime);
				//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
			}
			
			
			
			
		}
	}
		/*else{
			test_data_xls.setCellData("Suite", "Result", 2, "SKIP");
			test_data_xls.setCellData("Suite", "Time", 2, systemTime);
		}*/
}
}