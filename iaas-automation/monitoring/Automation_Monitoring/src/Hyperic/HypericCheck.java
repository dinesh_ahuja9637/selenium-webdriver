package Hyperic;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import base.TestBase;
import util.TestLog;
import util.ExcelReadAndWrite; 
import util.TestUtil;
public class HypericCheck extends TestBase{
	String alertname;
	String alertreason;
	String systemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;
	String VRA=null;
	boolean testskip=false;
	boolean hypericResult=false;
	String hypericserver;
	boolean skip=false;
	boolean	result=false;
	
	@AfterSuite
	public void generateBackupReport() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls");
	}
	
	@Test(dataProvider="getTestData")
	public void hyperichealth( String ServerNames ,String Link, String Username , String Password,String Result,String ExecutionTime,String Screenshoot ) throws InterruptedException, RowsExceededException, WriteException, BiffException{
		if(!TestUtil.isSuiteExecutable("Hypericcheckup"))
		{
			testskip=true;
			TestLog.info("Skipping the execution of DEM Service Suite as RunMode is Set to No");
			throw new SkipException("Runmode of DEM Service suite set to no. So Skipping all tests in CIS_Template_Creation suite");
		}
        hypericserver= Link;
		openBrowser();
		driver.navigate().to(ServerNames);
		WebDriverWait wait = new WebDriverWait(driver, 60);
		 WebElement requestorUsername= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("hyperic_username"))));
		    requestorUsername.sendKeys(Username);
		    Thread.sleep(5000);
		    WebElement requestorpassword= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("hyperic_password"))));
		   
		    requestorpassword.sendKeys(Password);
		 
		    Thread.sleep(5000);
		
			if(!hypericserver.equals("BEDTAMS025")){
			    driver.findElement(By.xpath(TestBase.or_getproperty("checkbox"))).click();
			    }
			
			 WebElement login = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("hyperic_submit"))));
			   login.click();
			   Actions action = new Actions(driver);
			   WebElement analzeclick = driver.findElement(By.xpath(or_getproperty("Analyze")));
		    	action.contextClick(analzeclick).perform();
		    	Thread.sleep(5000);
		    	
		    	if(hypericserver.equalsIgnoreCase("BEDTAMS025")){
		    		
		    	driver.findElement(By.xpath(or_getproperty("Alert"))).click();
		    	
		    	}
	        if(!hypericserver.equals("BEDTAMS025")){
		   	    	driver.findElement(By.xpath(or_getproperty("Alert1"))).click();
	                 }
		   	    	Thread.sleep(10000);
		   	    	
			    	int requestTabRowCount = driver.findElements(By.xpath(TestBase.or_getproperty("alert_count"))).size();
			   
			    	TestLog.info("count" + requestTabRowCount);
			    	if(requestTabRowCount>0){
			    	for(int i= 1 ; i<=requestTabRowCount; i++){
			    		Thread.sleep(5000);
			    		WebElement alertclick= driver.findElement(By.xpath(TestBase.or_getproperty("alertcount1")+ i + TestBase.or_getproperty("alertcount2")));
						alertclick.click();
						Thread.sleep(10000L);
						
						 alertname= driver.findElement(By.xpath(TestBase.or_getproperty("AlertName"))).getText();
						Thread.sleep(5000);
						 alertreason= driver.findElement(By.xpath(TestBase.or_getproperty("alertdescription"))).getText();
						 TestLog.info(alertname + ":"  +   alertreason);
			    	
			    	 
			    	    Thread.sleep(5000);
			    	    try 
						{  
			    	    	
							Workbook workbook1 = Workbook.getWorkbook(new File("src\\xls\\RDPTestData.xls"));
							WritableWorkbook copy = Workbook.createWorkbook(new File("src\\xls\\RDPTestData.xls"), workbook1);
							WritableSheet sheet2 = copy.getSheet(hypericserver); 
							int r = 0 , c = 0;						//System.out.println("r VALUE = "+r+ "");
							//System.out.println("c VALUE = "+c);
							//System.out.println(sheet2.findCell(RequestID).getRow());
							
							//System.out.println("r="+ r);
							WritableCell cell = sheet2.getWritableCell(r,c); 
							Label mn1 = new Label(c, r+i,alertname); 
						//System.out.println(r);
							Label mn = new Label(c+1, r+i, alertreason);
							//System.out.println(c);
				
							sheet2.addCell(mn1); 
							sheet2.addCell(mn);
						
							copy.write();
							copy.close();
							
						}
						catch(IOException e)
						{
							//excelScreenshotPath = CaptureScreenshot("worksheet not created");
						TestLog.info("worksheet not created");
						  //Assert.assertTrue(false, "worksheet not created");
						 hypericResult=true;
						  
							
						}
			    	    
			    	    driver.navigate().back();
			    	  
			    	}
			    	
			    	}
			    	else {
			    		System.out.println("No alerts");
			    		test_data_xls.setCellData(hypericserver, "AlertName", 2, "No Alert");
			    		test_data_xls.setCellData(hypericserver, "Reason", 2, "No Reason");
			    	
			    		
			    		
			    	}
			    	driver.findElement(By.xpath(or_getproperty("singout_hyperic"))).click();	
			    	 
	}
	@DataProvider(name="getTestData")
	public Object[][] testData() 
	{
		Object[][] arrayObject = util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls","HypericCheck");
		return arrayObject;                                                                                         
	}
	@AfterMethod(alwaysRun=true)
	public void generateReport()
	{
		if(driver!=null){
			closeBrowser();
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		systemTime=""+ dateFormat.format(cal.getTime());
		excelRowCount++;
		//System.out.println(skip +"123ssssssssssss");
		
		if(!testskip){
		switch(hypericserver)
		{
		case"BEDTAMS025":
			if(!skip)
			{
				if(hypericResult)
				{
				
				test_data_xls.setCellData("HypericCheck", "Result", 2, "FAIL");
				test_data_xls.setCellData("HypericCheck", "ExecutionTime", 2, systemTime);
				test_data_xls.setCellData("HypericCheck", "Screenshot", 2, excelScreenshotPath);
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				}
				 else
			    {
				test_data_xls.setCellData("HypericCheck", "Result", 2, "PASS");
				test_data_xls.setCellData("HypericCheck", "ExecutionTime", 2, systemTime);
			
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			    }
		}
		else
		{
			test_data_xls.setCellData("HypericCheck", "Result", 2, "SKIP");
			test_data_xls.setCellData("HypericCheck", "ExecutionTime", 2, systemTime);
			//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
		}
			break;
		
		case "BEDTAMS070":
			if(!skip)
			{
				if(hypericResult)
				{
					
					test_data_xls.setCellData("HypericCheck", "Result", 3, "FAIL");
					test_data_xls.setCellData("HypericCheck", "ExecutionTime", 3, systemTime);
					test_data_xls.setCellData("HypericCheck", "Screenshot", 3, excelScreenshotPath);
					//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
					
					
				}
				else
				{
					test_data_xls.setCellData("HypericCheck", "Result", 3, "PASS");
					test_data_xls.setCellData("HypericCheck", "ExecutionTime", 3, systemTime);
				
					//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
					
				}
			}
			else
			{
				test_data_xls.setCellData("HypericCheck", "Result", 3, "SKIP");
				test_data_xls.setCellData("HypericCheck", "ExecutionTime", 3, systemTime);
				//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
			}
			break;
		case "BEDTAMS068":
			if(!skip)
			{
				if(hypericResult)
				{
					
					test_data_xls.setCellData("HypericCheck", "Result", 4, "FAIL");
					test_data_xls.setCellData("HypericCheck", "ExecutionTime", 4, systemTime);
					test_data_xls.setCellData("HypericCheck", "Screenshot", 4, excelScreenshotPath);
					//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
					
					
				}
				else
				{
					test_data_xls.setCellData("HypericCheck", "Result", 4, "PASS");
					test_data_xls.setCellData("HypericCheck", "ExecutionTime", 4, systemTime);
				
					//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
					
				}
			}
			else
			{
				test_data_xls.setCellData("HypericCheck", "Result", 4, "SKIP");
				test_data_xls.setCellData("HypericCheck", "ExecutionTime", 4, systemTime);
				//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
			}
			break;
		case "BEDTAMS064":
			if(!skip)
			{
				if(hypericResult)
				{
					
					test_data_xls.setCellData("HypericCheck", "Result", 5, "FAIL");
					test_data_xls.setCellData("HypericCheck", "ExecutionTime", 5, systemTime);
					test_data_xls.setCellData("HypericCheck", "Screenshot", 5, excelScreenshotPath);
					//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
					
					
				}
				else
				{
					test_data_xls.setCellData("HypericCheck", "Result", 5, "PASS");
					test_data_xls.setCellData("HypericCheck", "ExecutionTime", 5, systemTime);
				
					//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
					
				}
			}
			else
			{
				test_data_xls.setCellData("HypericCheck", "Result", 5, "SKIP");
				test_data_xls.setCellData("HypericCheck", "ExecutionTime", 5, systemTime);
				//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
			}
			break;
		case "BEDTAMS066":
			if(!skip)
			{
				if(hypericResult)
				{
					
					test_data_xls.setCellData("HypericCheck", "Result", 6, "FAIL");
					test_data_xls.setCellData("HypericCheck", "ExecutionTime", 6, systemTime);
					test_data_xls.setCellData("HypericCheck", "Screenshot", 6, excelScreenshotPath);
					//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
					
					
				}
				else
				{
					test_data_xls.setCellData("HypericCheck", "Result", 6, "PASS");
					test_data_xls.setCellData("HypericCheck", "ExecutionTime", 6, systemTime);
				
					//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
					
				}
			}
			else
			{
				test_data_xls.setCellData("HypericCheck", "Result", 6, "SKIP");
				test_data_xls.setCellData("HypericCheck", "ExecutionTime", 6, systemTime);
				//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
			}
		}
		}
	}
	
}
