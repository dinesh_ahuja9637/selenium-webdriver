package DatabaseCheck;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.awt.AWTException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import base.TestBase;
import util.ExcelReadAndWrite;
import util.TestLog;
import util.TestUtil;
public class SuseSSHSDE extends VerifySSH{

	/**
	 * @param args
	 * @throws IOException 
	 * @throws AWTException 
	 * @throws InterruptedException 
	 * @throws WriteException 
	 * @throws BiffException 
	 * @throws JSchException 
	 */
	public String systemTime;
	boolean skip=false;
	public Session session;
	boolean sdeResult=false;
	boolean sdbResult=false;
	String HostName="";
	String SDEUsage="";
	boolean testskip=false;
	@AfterSuite
	public void generateBackupReport() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls");
	}

	@Test(dataProvider= "getTestData")
	public void ssh(String TemplateName,
			String MachineName,
			String UserName,
			String Password,
			String IP,
			String Port) throws  Exception{
		
		if(!TestUtil.isSuiteExecutable("SDB-SDE"))
		{
			testskip=true;
			TestLog.info("Skipping the execution of SDB-SDE Suite as RunMode is Set to No");
			throw new SkipException("Runmode of SDB-SDE suite set to no. So Skipping all tests in CIS_Template_Creation suite");
		}
		HostName=MachineName;
		getSession(TemplateName,MachineName,UserName,Password,IP,Port);
		session=getSession(TemplateName,MachineName,UserName,Password,IP,Port);
		System.out.println(session.getTimeout());
		session.setTimeout(50000);
		System.out.println("Command execution start");
		
		session.connect();
			System.out.println("session connected");
			
		
		String SuseCPUUsagesde=executeCommand(session,TestBase.or_getproperty("susecpuusagesde"));
		System.out.println("SUSE usage sde "+SuseCPUUsagesde);
		SDEUsage=SuseCPUUsagesde;
		int sde=Integer.parseInt(SuseCPUUsagesde.substring(0,SuseCPUUsagesde.length()-2 ));
		System.out.println(sde +"sde");
		
		if(sde < 80){
			
			System.out.println("less than 80% usage");
			
		}
		
		else{
		
			System.out.println("sde more than 80");
			sdeResult=true;
			Assert.assertFalse(true," sde more than 80");
		}
		
		
	}

	

@DataProvider(name="getTestData")
public Object[][] testData() 
{
	Object[][] arrayObject = util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls","SUSE");
	return arrayObject;                                                                                         
}

@AfterMethod(alwaysRun=true)
public void generateReport()
{
	if(driver!=null){
		closeBrowser();
	}
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
	Calendar cal = Calendar.getInstance();
	systemTime=""+ dateFormat.format(cal.getTime());
	excelRowCount++;
	if(!testskip){
	switch(HostName){
	case "BEDTAMS519":
	if(!skip)
	{
		if(sdeResult)
		{
			
			test_data_xls.setCellData("DatabaseResult", "SDEResult", 2, "FAIL");
			test_data_xls.setCellData("DatabaseResult", "SDETime", 2, systemTime);
			test_data_xls.setCellData("DatabaseResult", "SDEUsage", 2, SDEUsage);
			//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
			
			
		}
		else
		{
			test_data_xls.setCellData("DatabaseResult", "SDEResult", 2, "PASS");
			test_data_xls.setCellData("DatabaseResult", "SDETime", 2, systemTime);
			test_data_xls.setCellData("DatabaseResult", "SDEUsage", 2, SDEUsage);
			//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
			
		}
	}
	else
	{
		test_data_xls.setCellData("DatabaseResult", "SDEReport", 2, "SKIP");
		test_data_xls.setCellData("DatabaseResult", "SDETime", 2, systemTime);
		//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
	}
	
	case "BEDTAMS520":
		
		if(!skip)
		{
			if(sdeResult)
			{
				
				test_data_xls.setCellData("DatabaseResult", "SDEResult", 3, "FAIL");
				test_data_xls.setCellData("DatabaseResult", "SDETime", 3, systemTime);
				test_data_xls.setCellData("DatabaseResult", "SDEUsage", 3,SDEUsage );
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				
			}
			else
			{
				test_data_xls.setCellData("DatabaseResult", "SDEResult", 3, "PASS");
				test_data_xls.setCellData("DatabaseResult", "SDETime", 3, systemTime);
				test_data_xls.setCellData("DatabaseResult", "SDEUsage", 3,SDEUsage );
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			}
		}
		else
		{
			test_data_xls.setCellData("DatabaseResult", "SDEReport", 3, "SKIP");
			test_data_xls.setCellData("DatabaseResult", "SDETime", 3, systemTime);
			//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
		}
		
		
		
		
	}
	}
}

}
