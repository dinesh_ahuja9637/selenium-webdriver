package DatabaseCheck;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.awt.AWTException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import base.TestBase;
import util.ExcelReadAndWrite;
import util.TestLog;
import util.TestUtil;
public class Hyperic extends VerifySSH{

	/**
	 * @param args
	 * @throws IOException 
	 * @throws AWTException 
	 * @throws InterruptedException 
	 * @throws WriteException 
	 * @throws BiffException 
	 * @throws JSchException 
	 */
	public String systemTime;
	boolean skip=false;
	public Session session;
	boolean sda1Result=false;
	boolean sda3Result=false;
	String HostName="";
	String SDA1Usage="";
	String SDA3Usage="";
	boolean testskip=false;
	@AfterSuite
	public void generateBackupReport() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls");
	}

	@Test(dataProvider= "getTestData")
	public void ssh(String TemplateName,
			String MachineName,
			String UserName,
			String Password,
			String IP,
			String Port, String SDA1,String SDA1Result,String SDA1Time,String SDA3,String SDA3Result,String SDA3Time) throws  Exception{
		
		if(!TestUtil.isSuiteExecutable("Hyperic"))
		{
			testskip=true;
			TestLog.info("Skipping the execution of Hyperic Suite as RunMode is Set to No");
			throw new SkipException("Runmode of Hyperic suite set to no. So Skipping all tests in suite");
		}
		HostName=MachineName;
		getSession(TemplateName,MachineName,UserName,Password,IP,Port);
		session=getSession(TemplateName,MachineName,UserName,Password,IP,Port);
		System.out.println(session.getTimeout());
		session.setTimeout(50000);
		System.out.println("Command execution start");
		
		session.connect();
			System.out.println("session connected");
			
		
		String SuseCPUUsagesda1=executeCommand(session,TestBase.or_getproperty("susecpuusagesda1"));
		System.out.println("SUSE usage sda1 "+SuseCPUUsagesda1);
		SDA1Usage=SuseCPUUsagesda1;
		int sda1=Integer.parseInt(SuseCPUUsagesda1.substring(0,SuseCPUUsagesda1.length()-2 ));
		System.out.println(sda1 +"sda1");
		String SuseCPUUsagesda3=executeCommand(session,TestBase.or_getproperty("susecpuusagesda3"));
		System.out.println("SUSE usage sda3 "+SuseCPUUsagesda3);
		SDA3Usage=SuseCPUUsagesda3;
		int sda3=Integer.parseInt(SuseCPUUsagesda3.substring(0,SuseCPUUsagesda3.length()-2 ));
		System.out.println(sda3 +"sda3");
		
		if(sda1 < 80){
			
			System.out.println("less than 80% usage");
			
		}
		
		else{
		
			System.out.println("SDA1 more than 80");
			sda1Result=true;
			Assert.assertFalse(true," sda1 more than 80");
		}
		
		
		
		if(sda3 < 80){
			
			System.out.println("less than 80% usage");
			
		}
		
		else{
		
			System.out.println("SDA3 more than 80");
			sda3Result=true;
			Assert.assertFalse(true," sda3 more than 80");
		}
		
		
	}

	

@DataProvider(name="getTestData")
public Object[][] testData() 
{
	Object[][] arrayObject = util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls","Hyperic");
	return arrayObject;                                                                                         
}

@AfterMethod(alwaysRun=true)
public void generateReport()
{
	if(driver!=null){
		closeBrowser();
	}
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
	Calendar cal = Calendar.getInstance();
	systemTime=""+ dateFormat.format(cal.getTime());
	excelRowCount++;
	if(!testskip){
	switch(HostName){
	case "BEDTAMS025":
	if(!skip)
	{
		if(sda1Result)
		{
			
			test_data_xls.setCellData("Hyperic", "SDA1Result", 2, "FAIL");
			test_data_xls.setCellData("Hyperic", "SDA1Time", 2, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA1", 2, SDA1Usage);
			//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
			
			
		}
		else
		{
			test_data_xls.setCellData("Hyperic", "SDA1Result", 2, "PASS");
			test_data_xls.setCellData("Hyperic", "SDA1Time", 2, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA1", 2, SDA1Usage);
			//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
			
		}
		
		
		if(sda3Result)
		{
			
			test_data_xls.setCellData("Hyperic", "SDA3Result", 2, "FAIL");
			test_data_xls.setCellData("Hyperic", "SDA3Time", 2, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA3", 2, SDA3Usage);
			//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
			
			
		}
		else
		{
			test_data_xls.setCellData("Hyperic", "SDA3Result", 2, "PASS");
			test_data_xls.setCellData("Hyperic", "SDA3Time", 2, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA3", 2, SDA3Usage);
			//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
			
		}
		
	}
	else
	{
		test_data_xls.setCellData("Hyperic", "SDA1Report", 2, "SKIP");
		test_data_xls.setCellData("Hyperic", "SDA1Time", 2, systemTime);
		test_data_xls.setCellData("Hyperic", "SDA3Report", 2, "SKIP");
		test_data_xls.setCellData("Hyperic", "SDA3Time", 2, systemTime);
		//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
	}
	
	case "BEDTAMS068":
		
		if(!skip)
		{
			if(sda1Result)
			{
				
				test_data_xls.setCellData("Hyperic", "SDA1Result", 3, "FAIL");
				test_data_xls.setCellData("Hyperic", "SDA1Time", 3, systemTime);
				test_data_xls.setCellData("Hyperic", "SDA1", 3,SDA1Usage );
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				
			}
			else
			{
				test_data_xls.setCellData("Hyperic", "SDA1Result", 3, "PASS");
				test_data_xls.setCellData("Hyperic", "SDA1Time", 3, systemTime);
				test_data_xls.setCellData("Hyperic", "SDA1", 3,SDA1Usage );
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			}
			
			if(sda3Result)
			{
				
				test_data_xls.setCellData("Hyperic", "SDA3Result", 3, "FAIL");
				test_data_xls.setCellData("Hyperic", "SDA3Time", 3, systemTime);
				test_data_xls.setCellData("Hyperic", "SDA3", 3,SDA3Usage );
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				
			}
			else
			{
				test_data_xls.setCellData("Hyperic", "SDA3Result", 3, "PASS");
				test_data_xls.setCellData("Hyperic", "SDA3Time", 3, systemTime);
				test_data_xls.setCellData("Hyperic", "SDA3", 3,SDA3Usage );
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			}
		}
		else
		{
			test_data_xls.setCellData("Hyperic", "SDA1Report", 3, "SKIP");
			test_data_xls.setCellData("Hyperic", "SDA1Time", 3, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA3Report", 3, "SKIP");
			test_data_xls.setCellData("Hyperic", "SDA3Time", 3, systemTime);
			//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
		}
		
case "BEDTAMS070":
		
		if(!skip)
		{
			if(sda1Result)
			{
				
				test_data_xls.setCellData("Hyperic", "SDA1Result", 4, "FAIL");
				test_data_xls.setCellData("Hyperic", "SDA1Time", 4, systemTime);
				test_data_xls.setCellData("Hyperic", "SDA1", 4,SDA1Usage );
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				
			}
			else
			{
				test_data_xls.setCellData("Hyperic", "SDA1Result", 4, "PASS");
				test_data_xls.setCellData("Hyperic", "SDA1Time", 4, systemTime);
				test_data_xls.setCellData("Hyperic", "SDA1", 4,SDA1Usage );
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			}
			
			if(sda3Result)
			{
				
				test_data_xls.setCellData("Hyperic", "SDA3Result", 4, "FAIL");
				test_data_xls.setCellData("Hyperic", "SDA3Time", 4, systemTime);
				test_data_xls.setCellData("Hyperic", "SDA3", 4,SDA3Usage );
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				
			}
			else
			{
				test_data_xls.setCellData("Hyperic", "SDA3Result", 4, "PASS");
				test_data_xls.setCellData("Hyperic", "SDA3Time", 4, systemTime);
				test_data_xls.setCellData("Hyperic", "SDA3", 4,SDA3Usage );
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			}
		}
		else
		{
			test_data_xls.setCellData("Hyperic", "SDA1Report", 4, "SKIP");
			test_data_xls.setCellData("Hyperic", "SDA1Time", 4, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA3Report", 4, "SKIP");
			test_data_xls.setCellData("Hyperic", "SDA3Time", 4, systemTime);
			//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
		}
		
case "BEDTAMS064":
	
	if(!skip)
	{
		if(sda1Result)
		{
			
			test_data_xls.setCellData("Hyperic", "SDA1Result", 5, "FAIL");
			test_data_xls.setCellData("Hyperic", "SDA1Time", 5, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA1", 5,SDA1Usage );
			//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
			
			
		}
		else
		{
			test_data_xls.setCellData("Hyperic", "SDA1Result", 5, "PASS");
			test_data_xls.setCellData("Hyperic", "SDA1Time", 5, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA1", 5,SDA1Usage );
			//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
			
		}
		
		if(sda3Result)
		{
			
			test_data_xls.setCellData("Hyperic", "SDA3Result", 5, "FAIL");
			test_data_xls.setCellData("Hyperic", "SDA3Time", 5, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA3", 5,SDA3Usage );
			//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
			
			
		}
		else
		{
			test_data_xls.setCellData("Hyperic", "SDA3Result", 5, "PASS");
			test_data_xls.setCellData("Hyperic", "SDA3Time", 5, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA3", 5,SDA3Usage );
			//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
			
		}
	}
	else
	{
		test_data_xls.setCellData("Hyperic", "SDA1Report", 5, "SKIP");
		test_data_xls.setCellData("Hyperic", "SDA1Time", 5, systemTime);
		test_data_xls.setCellData("Hyperic", "SDA3Report", 5, "SKIP");
		test_data_xls.setCellData("Hyperic", "SDA3Time", 5, systemTime);
		//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
	}
case "BEDTAMS066":
	
	if(!skip)
	{
		if(sda1Result)
		{
			
			test_data_xls.setCellData("Hyperic", "SDA1Result", 6, "FAIL");
			test_data_xls.setCellData("Hyperic", "SDA1Time", 6, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA1", 6,SDA1Usage );
			//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
			
			
		}
		else
		{
			test_data_xls.setCellData("Hyperic", "SDA1Result", 6, "PASS");
			test_data_xls.setCellData("Hyperic", "SDA1Time", 6, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA1", 6,SDA1Usage );
			//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
			
		}
		
		if(sda3Result)
		{
			
			test_data_xls.setCellData("Hyperic", "SDA3Result", 6, "FAIL");
			test_data_xls.setCellData("Hyperic", "SDA3Time", 6, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA3", 6,SDA3Usage );
			//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
			
			
		}
		else
		{
			test_data_xls.setCellData("Hyperic", "SDA3Result", 6, "PASS");
			test_data_xls.setCellData("Hyperic", "SDA3Time", 6, systemTime);
			test_data_xls.setCellData("Hyperic", "SDA3", 6,SDA3Usage );
			//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
			
		}
	}
	else
	{
		test_data_xls.setCellData("Hyperic", "SDA1Report", 6, "SKIP");
		test_data_xls.setCellData("Hyperic", "SDA1Time", 6, systemTime);
		test_data_xls.setCellData("Hyperic", "SDA3Report", 6, "SKIP");
		test_data_xls.setCellData("Hyperic", "SDA3Time", 6, systemTime);
		//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
	}
	}
	}
}

}
