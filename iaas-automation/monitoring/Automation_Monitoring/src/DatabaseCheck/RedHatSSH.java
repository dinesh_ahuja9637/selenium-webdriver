package DatabaseCheck;

import org.testng.annotations.Test;

import java.awt.AWTException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import base.TestBase;
import util.TestLog;
import util.TestUtil;

public class RedHatSSH extends VerifySSH{

	/**
	 * @param args
	 * @throws Exception 
	 */
	public Session session;
	
	@Test(dataProvider= "getTestData")
	public void ssh(String TemplateName,
			String MachineName,
			String UserName,
			String Password,
			String IP,
			String Port) throws Exception{
		
		
		//getPassword(TemplateName,MachineName,UserName,Password,IP,Port);
		//getIPAddress(TemplateName,MachineName,UserName,Password,IP,Port);
		session=getSession(TemplateName,MachineName,UserName,Password,IP,Port);
		System.out.println(session.getTimeout());
		session.setTimeout(50000);
		System.out.println("Command execution start");
		
		session.connect();
			System.out.println("session connected");
			
		
		String redhatCPU=executeCommand(session,TestBase.or_getproperty("redhatCPU"));
		System.out.println("Redhat CPU "+redhatCPU);
		
		String  redhatMemory=executeCommand(session,TestBase.or_getproperty("redhatMemory"));
		System.out.println("Red hat Memory " +redhatMemory);
		
		String  DiskDetails=executeCommand(session,TestBase.or_getproperty("redhatDisk"));
		System.out.println("SUSE " +DiskDetails);
		
		String RedhatVersion =executeCommand(session,TestBase.or_getproperty("redhatOS"));
		System.out.println("Suse OS" + RedhatVersion);
	}

	

@DataProvider(name="getTestData")
public Object[][] testData() 
{
	Object[][] arrayObject = util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls","RHEL");
	return arrayObject;                                                                                         
}
}
