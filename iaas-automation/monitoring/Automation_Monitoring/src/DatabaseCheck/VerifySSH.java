package DatabaseCheck;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import base.TestBase;
import util.TestLog;

public class VerifySSH extends TestBase{

	String approverRequestId=null;
	boolean result=false;
	static String result1;
	String sytemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;
	boolean vmStatus=false;
	
	
	
	
	
	public static Session getSession(String TemplateName,
			String MachineName,
			String UserName,
			String Password,
			String IP,
			String Port) throws JSchException {
		
		System.out.println("Details from Excel:"+TemplateName +MachineName +UserName +Password +IP +Port);
		Session session = null;
		int port = Integer.parseInt(Port);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch jsch = new JSch();
		session = jsch.getSession(UserName, IP, port);
		session.setPassword(Password);
		session.setConfig(config);
		System.out.println("Session is created...");
		return session;

	}
		
	public String executeCommand(Session session, String command)
			throws Exception {
		try {
			System.out.println("Inside command Executer..");
			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();

			byte[] tmp = new byte[1024];

			while (in.available() >= 0) {
				int j = in.read(tmp, 0, 1024);
				if (j < 0)
					break;
				System.out.println("$$$$$$$$$" + (j));
				//  System.out.print(new String(tmp, 0, j));
				Thread.sleep(5000);
				result1 = new String(tmp, 0, j);
				//System.out.println("Output:"+Result1);
			}

		} catch (Error e) {
			//softassert.assertTrue(false,
				//	"Successfully not executed command :" + command
						//	+ ":  Test Fail ");
		}

		return result1;
	}


}
