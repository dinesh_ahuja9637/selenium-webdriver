package VRAAvailability;

import org.openqa.selenium.WebElement;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import base.TestBase;
import util.TestLog;
import util.ExcelReadAndWrite; 
import util.TestUtil;
 
public class vRAAvailability  extends TestBase {
	
	String systemTime=null;
	int excelRowCount=1;
	String excelScreenshotPath= null;
	String VRA=null;
	boolean testskip=false;
	boolean VRAResult=false;

	
	boolean skip =false;

	@AfterSuite
	public void generateBackupReport() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls");
	}

	@Test(dataProvider= "getTestData")
	public void testVRAAvailability(String tenantURL,String RunMode,String Result,String Time)throws InterruptedException, BiffException, IOException ,RowsExceededException, WriteException{
		
		if(!TestUtil.isSuiteExecutable("VRAAvailability"))
		{
			testskip=true;
			TestLog.info("Skipping the execution of VRAAvailability Suite as RunMode is Set to No");
			throw new SkipException("Runmode of VRAAvailability suite set to no. So Skipping all tests in CIS_Template_Creation suite");
		}
		
		VRA=tenantURL;
		//System.out.println(skip);
		if(!RunMode.equalsIgnoreCase("Y")){
			TestLog.info("Skipping execution of test case as Run mode is set to No");
			skip=true;
			throw new SkipException("Skipping execution of test case as Run mode is set to No");
		}
		else{
			skip =false;
		
		openBrowser();
			//VRA=tenantURL;
    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
   
    driver.get(TestBase.fn_getproperty(tenantURL));

    Thread.sleep(15000);
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
    Thread.sleep(15000);
    if(driver.findElements(By.id("submit")).size()==0)
    {
    	VRAResult=true;
    	Assert.assertFalse(true,"URL not loaded");
  	
    }
    WebDriverWait wait = new WebDriverWait(driver, 60);
    WebElement requestorUsername= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("req_username"))));
    requestorUsername.sendKeys(TestBase.fn_getproperty("requestor_username"));
    WebElement requestorPassword= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TestBase.or_getproperty("req_password"))));
    requestorPassword.sendKeys(TestBase.fn_getproperty("requestor_passwd"));
    Thread.sleep(15000);
    driver.findElement(By.id("submit")).click();
    Thread.sleep(3000L); 
    
 
    if(driver.findElements(By.xpath(TestBase.or_getproperty("LoginError"))).size()==1)
    {
    	 String currentURL = driver.getCurrentUrl();
         System.out.println(currentURL);
         /*if(currentURL.contains("saml/websso/sso")){
        	 System.out.println("Error"); 
         }
        	*/
    	System.out.println(driver.findElement(By.xpath(TestBase.or_getproperty("LoginError"))).getText());
    	
    	System.out.println("Login error");
    	VRAResult=true;
    	Assert.assertFalse(true,"Error in login");
    }
    else
    {
    	System.out.println("Pass");
    }
		}
    }
    
	
	@DataProvider(name="getTestData")
	public Object[][] testData() 
	{
		Object[][] arrayObject = util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls","VRAAvailability");
		return arrayObject;                                                                                         
	}
	
	@AfterMethod(alwaysRun=true)
	public void generateReport()
	{
		if(driver!=null){
			closeBrowser();
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		systemTime=""+ dateFormat.format(cal.getTime());
		excelRowCount++;
		//System.out.println(skip +"123ssssssssssss");
		
		if(!testskip){
		switch(VRA)
		{
		case "TCA":
			if(!skip)
			{
				if(VRAResult)
				{
				
				test_data_xls.setCellData("VRAAvailability", "Result", 2, "FAIL");
				test_data_xls.setCellData("VRAAvailability", "Time", 2, systemTime);
				test_data_xls.setCellData("VRAAvailability", "Screenshot", 2, excelScreenshotPath);
				//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
				
				}
				 else
			    {
				test_data_xls.setCellData("VRAAvailability", "Result", 2, "PASS");
				test_data_xls.setCellData("VRAAvailability", "Time", 2, systemTime);
			
				//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
				
			    }
		}
		else
		{
			test_data_xls.setCellData("VRAAvailability", "Result", 2, "SKIP");
			test_data_xls.setCellData("VRAAvailability", "Time", 2, systemTime);
			//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
		}
			break;
		
		case "TCB":
			if(!skip)
			{
				if(VRAResult)
				{
					
					test_data_xls.setCellData("VRAAvailability", "Result", 3, "FAIL");
					test_data_xls.setCellData("VRAAvailability", "Time", 3, systemTime);
					test_data_xls.setCellData("VRAAvailability", "Screenshot", 3, excelScreenshotPath);
					//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
					
					
				}
				else
				{
					test_data_xls.setCellData("VRAAvailability", "Result", 3, "PASS");
					test_data_xls.setCellData("VRAAvailability", "Time", 3, systemTime);
				
					//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
					
				}
			}
			else
			{
				test_data_xls.setCellData("VRAAvailability", "Result", 3, "SKIP");
				test_data_xls.setCellData("VRAAvailability", "Time", 3, systemTime);
				//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
			}
			break;
		case "TCC":
			if(!skip)
			{
				if(VRAResult)
				{
					
					test_data_xls.setCellData("VRAAvailability", "Result", 4, "FAIL");
					test_data_xls.setCellData("VRAAvailability", "Time", 4, systemTime);
					test_data_xls.setCellData("VRAAvailability", "Screenshot", 4, excelScreenshotPath);
					//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
					
					
				}
				else
				{
					test_data_xls.setCellData("VRAAvailability", "Result", 4, "PASS");
					test_data_xls.setCellData("VRAAvailability", "Time", 4, systemTime);
				
					//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
					
				}
			}
			else
			{
				test_data_xls.setCellData("VRAAvailability", "Result", 4, "SKIP");
				test_data_xls.setCellData("VRAAvailability", "Time", 4, systemTime);
				//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
			}
			break;
		case "TCD":
			if(!skip)
			{
				if(VRAResult)
				{
					
					test_data_xls.setCellData("VRAAvailability", "Result", 5, "FAIL");
					test_data_xls.setCellData("VRAAvailability", "Time", 5, systemTime);
					test_data_xls.setCellData("VRAAvailability", "Screenshot", 5, excelScreenshotPath);
					//test_data_xls.setCellData("Suite", "Result",  7, "FAIL");
					
					
				}
				else
				{
					test_data_xls.setCellData("VRAAvailability", "Result", 5, "PASS");
					test_data_xls.setCellData("VRAAvailability", "Time", 5, systemTime);
				
					//test_data_xls.setCellData("Suite", "Result", 7, "PASS");
					
				}
			}
			else
			{
				test_data_xls.setCellData("VRAAvailability", "Result", 5, "SKIP");
				test_data_xls.setCellData("VRAAvailability", "Time", 5, systemTime);
				//test_data_xls.setCellData("Suite", "Result", 1, "SKIP");
			}
	
		}
		}
	}
}