/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : Common utility for checking run mode at suite level and test case level.
 Date : 26/02/2016
 **************************************************************
 */

package util;


import util.Constants;
import base.*;


public class TestUtil extends Constants {

	
	public static boolean isExecutable(String tcid)
	{
		for(int rowNum=2; rowNum<=vmDeatils.getRowCount("TestCaseResult"); rowNum++)
		{
			if(vmDeatils.getCellData("TestCaseResult", "MachineName", rowNum).equals(tcid))
			{
				if(vmDeatils.getCellData("TestCaseResult", "RunMode", rowNum).equalsIgnoreCase("Y"))
				{
					return true;
					
				}
				else
				{	
					return false;
				}
			
			}
			
		}
		
		return false;
	}



	public static boolean isSuiteExecutable(String suiteName)
	{
		boolean suiteResult=false;
	
		for(int rowNum=2; rowNum<=excel.getRowCount("Suite"); rowNum++)
		{
			if(excel.getCellData("Suite", "Task", rowNum).equals(suiteName))
			{
				if(excel.getCellData("Suite", "RunMode", rowNum).equalsIgnoreCase("Y"))
				{
					suiteResult =  true;
				}
				
				else
				{
					suiteResult =  false;
				}
		
			}
		}
	
		return suiteResult;

	}
	
	public static void setPassResult(String sheetName,String colName, int rowNum, String screenshot)
	{
		vmDeatils.setCellData(sheetName, colName, rowNum, screenshot);
		//vmDeatils.setCellData("Red_Hat_Enterprise_Linux", "Destroy_Screenshot", 2, excelScreenshotPath);
	}
	
	public static void setFailureResult()
	{
		vmDeatils.setCellData("Red_Hat_Enterprise_Linux", "Destroy_Result", 2, "PASS");
		//vmDeatils.setCellData("Red_Hat_Enterprise_Linux", "Destroy_Screenshot", 2, excelScreenshotPath);
	}


}
