/*
 **************************************************************
 Author : IaaS Test Automation Team
 Purpose : Declaration of constants/reusable variables.
 Date : 26/02/2016
 **************************************************************
 */

package util;

public class Constants {
	
	public static xls_reader excel = new xls_reader(System.getProperty("user.dir")+"\\src\\xls\\Suite.xls");
	public static xls_reader vmDeatils = new xls_reader(System.getProperty("user.dir")+"\\src\\xls\\RDPTestData.xls");

	
}
