﻿###########################################################################
################ DTA PRODUCT VERSIONING STARTED(Windows) ##########################


Write-Host "DTA PRODUCT VERSIONING STARTED (Windows)"
$infoProperties=@()
$infoProperties=Import-Csv -Path '.\ProductVersioningWindows.csv'


$Return=@()
for($i=0;$i -lt @( $infoProperties).Length;$i++)
{
try{
$i
$flag=$false
$UserName=$infoProperties[0].DTAUserName

$Password=$infoProperties[0].DTAPassword
Write-host $UserName
$creds=New-Object System.Management.Automation.PSCredential ($UserName,$(ConvertTo-SecureString $Password -AsPlainText -Force))
$Parameters=$infoProperties[$i].AppName
Write-Host "DTA Application Name:"$Parameters

$Return=Invoke-Command -ComputerName $infoProperties[$i].DTAHost -Credential $creds -ArgumentList $Parameters -ScriptBlock{
Param ($Parameters)
$Parameters

$command1=Get-WmiObject -Class Win32_Product | where {$_.Name.ToLower() -eq $Parameters.ToLower().trim()} | Select-Object Version
$command2=Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\* | where {$_.DisplayName -Like "*$Parameters*" } | Select-Object DisplayVersion
$command3=Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | where {$_.DisplayName -Like "*$Parameters*" } | Select-Object DisplayVersion

#$command5= plink -ssh -l root -P 22 -pw VMware11@! BEDTAMS026.dta.inf command cat /opt/vmware/vpostgres/current/data/PG_VERSION
if($command1 -ne $null)
{
    Write-Host "Inside Command1:Paramater:$Parameters :Output: $command1"
    return $command1
}
elseif($command2 -ne $null)
{
    Write-Host "Inside Command2:Paramater:$Parameters :Output: $command2"
    return $command2
}
elseif($command3 -ne $null)   
{
   $command3=Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | where {$_.DisplayName -Like "*$Parameters*" } | Select-Object DisplayVersion
   Write-Host "Inside Command3:Paramater:$Parameters :Output: $command3"
   return $command3
}elseif(($command4=get-itemproperty HKLM:\SOFTWARE\Microsoft\InetStp\  | select versionstring) -ne $null)
{
$command4=get-itemproperty HKLM:\SOFTWARE\Microsoft\InetStp\  | select versionstring 
Write-Host "Inside Command4:Paramater:$Parameters :Output: $command4"
   return $command4
}else{
$command5=Get-ItemProperty 'HKLM:\Software\VMware, Inc.\VMware Infrastructure\Orchestrator'| Select-Object InstalledVersion
Write-Host "Inside Command5:Paramater:$Parameters :Output: $command5"
   return $command5

}

}
Write-Host $Return
if($Return.DisplayVersion.Length -gt 1){
Write-Host "DTA Application version" $Return.DisplayVersion
$splitted = $Return.DisplayVersion
}
elseif($Return.Version.Length -gt 1){
Write-Host "DTA Application version" $Return.Version
$splitted = $Return.Version
}
else{
Write-Host "DTA Application version" $Return.VersionString
$splitted = $Return.VersionString
}

$infoProperties | %{ $infoProperties[$i].DTAAppVersion = $splitted }
$infoProperties | Export-Csv -Path '.\ProductVersioningWindows.csv' -NoTypeInformation
}Catch{
write-host “Caught an exception:” 
    write-host “Exception Type: $($_.Exception.GetType().FullName)”
    write-host "Linux Machine"
}
}

Write-Host "DTA PRODUCT VERSIONING COMPLETED(Windows)"

##################################################################################
################# PRODUCTION PRODUCT VERSIONING STARTING #########################

Write-Host "PRODUCTION PRODUCT VERSIONING STARTING(Windows)"
$infoProperties=@()
$infoProperties=Import-Csv -Path '.\ProductVersioningWindows.csv'

$Return=@()
for($i=0;$i -lt@( $infoProperties).Length;$i++)
{
$i

$flag=$false
$UserName=$infoProperties[0].ProductionUserName

$Password=$infoProperties[0].ProductionPassword
Write-host $UserName

$creds=New-Object System.Management.Automation.PSCredential ($UserName,$(ConvertTo-SecureString $Password -AsPlainText -Force))

$Parameters=$infoProperties[$i].AppName
Write-Host "DTA Application Name:"$Parameters

$Return=Invoke-Command -ComputerName $infoProperties[$i].ProductionHost -Credential $creds -ArgumentList $Parameters -ScriptBlock{
Param ($Parameters)
$Parameters

$command1=Get-WmiObject -Class Win32_Product | where {$_.Name.ToLower() -eq $Parameters.ToLower().trim()} | Select-Object Version
$command2=Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\* | where {$_.DisplayName -Like "*$Parameters*" } | Select-Object DisplayVersion
$command3=Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | where {$_.DisplayName -Like "*$Parameters*" } | Select-Object DisplayVersion

if($command1 -ne $null)
{
    Write-Host "Inside Command1:Paramater:$Parameters :Output: $command1"
    return $command1
}
elseif($command2 -ne $null)
{
    Write-Host "Inside Command2:Paramater:$Parameters :Output: $command2"
    return $command2
}
elseif($command3 -ne $null)
{
   $command3=Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | where {$_.DisplayName -Like "*$Parameters*" } | Select-Object DisplayVersion
   Write-Host "Inside Command3:Paramater:$Parameters :Output: $command3"
   return $command3
}
elseif(($command4=get-itemproperty HKLM:\SOFTWARE\Microsoft\InetStp\  | select versionstring) -ne $null )
{
$command4=get-itemproperty HKLM:\SOFTWARE\Microsoft\InetStp\  | select versionstring 
 Write-Host "Inside Command4:Paramater:$Parameters :Output: $command4"
 return $command4
}else{
$command5=Get-ItemProperty 'HKLM:\Software\VMware, Inc.\VMware Infrastructure\Orchestrator'| Select-Object InstalledVersion
Write-Host "Inside Command5:Paramater:$Parameters :Output: $command5"
 return $command5
}
}
if($Return.DisplayVersion.Length -gt 1){
Write-Host "Production version" $Return.DisplayVersion
$splitted = $Return.DisplayVersion
}
elseif($Return.Version.Length -gt 1){
Write-Host "Prouction Application version" $Return.Version
$splitted = $Return.Version
}
else{
Write-Host "Production version" $Return.VersionString
$splitted = $Return.VersionString

}

$infoProperties | %{ $infoProperties[$i].ProdVersion = $splitted }
$infoProperties | Export-Csv -Path '.\ProductVersioningWindows.csv' -NoTypeInformation
}

Write-Host "PRODUCTION PRODUCT VERSIONING COMPLETED(Windows)"
################# PRODUCTION PRODUCT VERSIONING COMPLETED ####################################

########################### LINUX DTA VERSION STARTING ####################################

Write-Host "DTA PRODUCT VERSIONING STARTED (Linux)"
$infoProperties=@()
$infoProperties=Import-Csv -Path '.\ProductVersioningLinux.csv'
$LinuxAppVersion = "y"|& ".\linuxDTA.bat"
$separator = " "
$option = [System.StringSplitOptions]::RemoveEmptyEntries
$LinuxAppVersion=$LinuxAppVersion.Split($separator,$option)
$n=($LinuxAppVersion.Length)
Write-Host $n
for($j=0;$j -lt $n; $j=$j+2)
{
	for($i=0;$i -le@( $infoProperties).Length; $i++)
	{

	if($infoProperties[$i].DTAHost -eq $LinuxAppVersion[$j])
	{
	$linuxVer=$LinuxAppVersion[$j+1].ToString().split("-");
	Write-Host $linuxVer
	$version=$linuxVer[$linuxVer.Length - 2]+"-"+$linuxVer[$linuxVer.Length - 1]
	Write-Host  "version" $version
	$infoProperties | %{ $infoProperties[$i].DTAAppVersion = $version}
    $infoProperties | Export-Csv -Path '.\ProductVersioningLinux.csv' -NoTypeInformation

	}

}
}
Write-Host "DTA PRODUCT VERSIONING COMPLETED (Linux)"

########################### LINUX DTA VERSION END  ####################################
########################### LINUX PRODUCTION VERSION STARTING  ####################################

Write-Host "PRODUCTION PRODUCT VERSIONING STARTED (Linux)"
$infoProperties=@()
$infoProperties=Import-Csv -Path '.\ProductVersioningLinux.csv'
$LinuxAppVersion ="y"| & ".\linuxProd.bat"
$separator = " "
$option = [System.StringSplitOptions]::RemoveEmptyEntries
$LinuxAppVersion=$LinuxAppVersion.Split($separator,$option)
$n=($LinuxAppVersion.Length)
Write-Host $n
for($j=0;$j -lt $n; $j=$j+2)
{
	for($i=0;$i -le@( $infoProperties).Length; $i++)
	{

	if($infoProperties[$i].ProductionHost -eq $LinuxAppVersion[$j])
	{
	$linuxVer=$LinuxAppVersion[$j+1].ToString().split("-");
	Write-Host $linuxVer
	$version=$linuxVer[$linuxVer.Length - 2]+"-"+$linuxVer[$linuxVer.Length - 1]
	Write-Host  "version" $version
	$infoProperties | %{ $infoProperties[$i].ProdVersion = $version}
    $infoProperties | Export-Csv -Path '.\ProductVersioningLinux.csv' -NoTypeInformation

	}

}
}

Write-Host "PRODUCTION PRODUCT VERSIONING COMPLETED (Linux)"
########################### LINUX PRODUCTION VERSION END  ####################################

##############################Linux Appliances DTA Versioning###################################

Write-Host "Linux Appliances DTA Versioning Started"
add-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
Connect-VIServer -Server bedtams001

$infoProperties=Import-Csv -Path '.\ProductVersioningAppliances.csv'

foreach ($infoPropertie in $infoProperties)
{
$vmname=$infoPropertie.DTAHost
Write-Host $vmname

$VSEs = Get-View -ViewType virtualmachine -Filter @{"name"="$vmname"}

$version=$VSEs.config.vappconfig.product[0].version
$infoProperties | %{ $infoPropertie.DTAAppVersion = $version}
$infoProperties | Export-Csv -Path '.\ProductVersioningAppliances.csv' -NoTypeInformatio

}
Write-Host "Linux Appliances DTA Versioning complete"
##############################Linux Appliances DTA Versioning Complete ###############################

##############################Linux Appliances Production Versioning###################################

Write-Host "Linux Appliances Production Versioning Started"
add-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
Connect-VIServer -Server bedtams001

$infoProperties=Import-Csv -Path '.\ProductVersioningAppliances.csv'

foreach ($infoPropertie in $infoProperties)
{
$vmname=$infoPropertie.DTAHost
Write-Host $vmname

$VSEs = Get-View -ViewType virtualmachine -Filter @{"name"="$vmname"}

$version=$VSEs.config.vappconfig.product[0].version
$infoProperties | %{ $infoPropertie.ProdVersion = $version}
$infoProperties | Export-Csv -Path '.\ProductVersioningAppliances.csv' -NoTypeInformatio

}
Write-Host "Linux Appliances Production Versioning complete"
##############################Linux Appliances production Versioning Complete ###############################

########################### VERSION VERIFICATION STARTING ####################################

Write-Host "VERSION VERIFICATION STARTING(windows)"
$infoProperties=@()
$infoProperties=Import-Csv -Path '.\ProductVersioningWindows.csv'
for($i=0;$i -lt@( $infoProperties).Length;$i++)
{
$DTAAppVersion=$infoProperties[$i].DTAAppVersion

$prodVersion=$infoProperties[$i].ProdVersion
#Write-Host $DTAAppVersion.Length
#Write-Host $prodVersion.Length

if($DTAAppVersion.Length -eq 0 -or $prodVersion.Length -eq 0 )
{
$resultStatus="Application version not found"
}
else{
if( $DTAAppVersion -eq $prodVersion )
{
    $resultStatus="Match"
}
else
{
    $resultStatus="NO Match"
}
}
    $infoProperties | %{ $infoProperties[$i].Status = $resultStatus
    $infoProperties | Export-Csv -Path './ProductVersioningWindows.csv' -NoTypeInformation
}
}

Write-Host "VERSION VERIFICATION COMPLETED(windows)"
############################################################## Version Verification Start Linux ###############
Write-Host "VERSION VERIFICATION STARTING(Linux)"
$infoProperties=@()
$infoProperties=Import-Csv -Path '.\ProductVersioningLinux.csv'
for($i=0;$i -lt@( $infoProperties).Length;$i++)
{
$DTAAppVersion=$infoProperties[$i].DTAAppVersion

$prodVersion=$infoProperties[$i].ProdVersion
#Write-Host $DTAAppVersion.Length
#Write-Host $prodVersion.Length

if($DTAAppVersion.Length -eq 0 -or $prodVersion.Length -eq 0 )
{
$resultStatus="Application version not found"
}
else{
if( $DTAAppVersion -eq $prodVersion )
{
    $resultStatus="Match"
}
else
{
    $resultStatus="NO Match"
}
}
    $infoProperties | %{ $infoProperties[$i].Status = $resultStatus
    $infoProperties | Export-Csv -Path './ProductVersioningLinux.csv' -NoTypeInformation
}
}

Write-Host "VERSION VERIFICATION COMPLETED(linux)"

########################### VERSION VERIFICATION COMPLETED ####################################

############################################################## Version Verification Start Appliance ###############
Write-Host "VERSION VERIFICATION STARTING(Appliance)"
$infoProperties=@()
$infoProperties=Import-Csv -Path '.\ProductVersioningAppliances.csv'
for($i=0;$i -lt@( $infoProperties).Length;$i++)
{
$DTAAppVersion=$infoProperties[$i].DTAAppVersion

$prodVersion=$infoProperties[$i].ProdVersion
#Write-Host $DTAAppVersion.Length
#Write-Host $prodVersion.Length

if($DTAAppVersion.Length -eq 0 -or $prodVersion.Length -eq 0 )
{
$resultStatus="Application version not found"
}
else{
if( $DTAAppVersion -eq $prodVersion )
{
    $resultStatus="Match"
}
else
{
    $resultStatus="NO Match"
}
}
    $infoProperties | %{ $infoProperties[$i].Status = $resultStatus
    $infoProperties | Export-Csv -Path './ProductVersioningAppliances.csv' -NoTypeInformation
}
}

Write-Host "VERSION VERIFICATION COMPLETED(Appliance)"

########################### VERSION VERIFICATION COMPLETED ####################################
########################### MERGE  FILES START ####################################################

$CSVFiles ="ProductVersioningWindows.csv" ,"ProductVersioningLinux.csv","ProductVersioningAppliances.csv"
$OutputFile = "ProductVersioningDTA.csv"           
           
$Output = @();            
foreach($CSV in $CSVFiles) {            
    if(Test-Path $CSV) {            
                    
                 
        $temp = Import-CSV -Path $CSV | select  SerialNumber,AppName,DTAHost,DTAAppVersion,ProductionHost,ProdVersion,Status           
        $Output += $temp            
            
    } else {            
        Write-Warning "$CSV : No such file found"            
    }            
            
}  
Write-Output $Output 
#$Output = $Output | Sort DTAHost

#Write-Output $Output 
$Output | Export-Csv -Path $OutputFile -NoTypeInformation            
Write-Output "$OutputFile successfully created"



########################### MERGE TWO FILES COMPLETE ####################################################

########################### CONVERSION FROM CSV TO EXCEL STARTING ####################################

Write-Host "CONVERSION FROM CSV TO EXCEL STARTING"
$FileName = $PWD.ToString()+"\ProductVersioningDTA"
$FileNameExcel="$FileName"+$(get-date -f MM-dd-yyyy_HH_mm_ss)
$xlExcel8 = 56
$excel = New-Object -ComObject Excel.Application 
$excel.Visible = $true
$excel.DisplayAlerts = $false
$excel.Workbooks.Open("$FileName.csv").SaveAs("$FileNameExcel.xls",$xlExcel8) 
#$excel.Workbooks.Open().SaveAs("$FileNameExcel.xls",$xlExcel8) 

$excel.Quit()
Write-Host "CONVERSION FROM CSV TO EXCEL COMPLETED"


########################### CONVERSION FROM CSV TO EXCEL COMPLETED ####################################

########################### HIGHLIGHTING THE MATCH AND UNMATCH STATUS IN EXCEL ####################################

Write-Host "HIGHLIGHTING THE MATCH AND UNMATCH STATUS IN EXCEL"
$infoProperties=@()
$infoProperties=Import-Csv -Path '.\ProductVersioningDTA.csv'

function Release-Ref ($ref) { 
([System.Runtime.InteropServices.Marshal]::ReleaseComObject( 
[System.__ComObject]$ref) -gt 0) 
[System.GC]::Collect() 
[System.GC]::WaitForPendingFinalizers() 
} 
# ----------------------------------------------------- 
 
$objExcel = new-object -comobject excel.application
$objExcel.Visible = $true
$objExcel.DisplayAlerts = $false
$objWorkbook = $objExcel.Workbooks.Open($FileNameExcel+".xls") 
$objWorksheet = $objWorkbook.Worksheets.Item(1)
$totalcol=7
for($j=1;$j -le $totalcol;$j++)
{
$objWorksheet.Cells.Item(1, $j).Interior.ColorIndex = 6
}
 
for($i=0;$i -lt@( $infoProperties).Length;$i++)
{
    if($infoProperties[$i].Status -eq "Match")
    {
        $objWorksheet.Cells.Item($i+2, 7).Interior.ColorIndex = 10
    }
    elseif($infoProperties[$i].Status -eq "NO Match")
    {
        $objWorksheet.Cells.Item($i+2, 7).Interior.ColorIndex = 3
    }
	else{
	 $objWorksheet.Cells.Item($i+2, 7).Interior.ColorIndex = 16
	}
}

$objWorkbook.Save()
$objWorkbook.Close()
$objExcel.Quit()

$a = Release-Ref($objWorksheet)
$a = Release-Ref($objWorkbook)
$a = Release-Ref($objExcel) 

Write-Host "HIGHLIGHTING THE MATCH AND UNMATCH STATUS IN EXCEL COMPLETED"

########################### HIGHLIGHTING THE MATCH AND UNMATCH STATUS IN EXCEL COMPLETED ####################################


