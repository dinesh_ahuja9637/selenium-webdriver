@echo off
SET _Inputfile=ProductVersioningLinux.csv
(
FOR /F "tokens=1-18* delims=," %%A IN (%_InputFile%) DO (
if not "%%~A" == "SerialNumber" (
echo %%~H
plink -ssh -l %%~I -P 22 -pw %%~J %%~H command %%~K
)
)
)