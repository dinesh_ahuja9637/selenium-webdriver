@echo off
SET _Inputfile=ProductVersioningLinux.csv
(
FOR /F "tokens=1-18* delims=," %%A IN (%_InputFile%) DO (
if not "%%~A" == "SerialNumber" (
echo %%~C
plink -ssh -l %%~D -P 22 -pw %%~E %%~C command %%~F
)
)
)