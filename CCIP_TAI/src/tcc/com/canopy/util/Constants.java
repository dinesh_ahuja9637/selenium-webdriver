/* 
==============================================================
 Author : IAAS Automation Test Team
 Purpose : Constants initializations
 Date : 10/03/2016
 Test Case Name : 
 
 #======================================================================
 */
package tcc.com.canopy.util;

public class Constants {
	
	public static xls_reader excel = new xls_reader(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\Suite.xls");
	public static xls_reader vmDeatils = new xls_reader(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls");

	
}
