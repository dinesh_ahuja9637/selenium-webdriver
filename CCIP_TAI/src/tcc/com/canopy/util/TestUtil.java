/* 
==============================================================
 Author : IAAS Automation Test Team
 Purpose : Utility class for run mode
 Date : 10/03/2016
 Test Case Name : 
 
 #======================================================================
 */
package tcc.com.canopy.util;


import tcc.com.canopy.base.*;
import tcc.com.canopy.util.Constants;


public class TestUtil extends Constants {

	
	public static boolean isExecutable(String tcid)
	{
		for(int rowNum=2; rowNum<=vmDeatils.getRowCount("Suite"); rowNum++)
		{
			if(vmDeatils.getCellData("Suite", "TSID", rowNum).equals(tcid))
			{
				if(vmDeatils.getCellData("Suite", "RunMode", rowNum).equalsIgnoreCase("Y"))
				{
					return true;
					
				}
				else
				{	
					return false;
				}
			
			}
			
		}
		
		return false;
	}



	public static boolean isSuiteExecutable(String suiteName)
	{
		boolean suiteResult=false;
	
		for(int rowNum=2; rowNum<=excel.getRowCount("Suite"); rowNum++)
		{
			if(excel.getCellData("Suite", "TSID", rowNum).equals(suiteName))
			{
				if(excel.getCellData("Suite", "RunMode", rowNum).equalsIgnoreCase("Y"))
				{
					suiteResult =  true;
				}
				
				else
				{
					suiteResult =  false;
				}
		
			}
		}
	
		return suiteResult;

	}
	
	public static void setPassResult(String sheetName,String colName, int rowNum, String screenshot)
	{
		vmDeatils.setCellData(sheetName, colName, rowNum, screenshot);
		//vmDeatils.setCellData("Red_Hat_Enterprise_Linux", "Destroy_Screenshot", 2, excelScreenshotPath);
	}
	
	public static void setFailureResult()
	{
		vmDeatils.setCellData("Red_Hat_Enterprise_Linux", "Destroy_Result", 2, "PASS");
		
	}


}
