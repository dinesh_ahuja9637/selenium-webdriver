/* 
==============================================================
 Author : IAAS Automation Test Team
 Purpose : Test Base Class
 Date : 10/03/2016
 Test Case Name : 
 
 #======================================================================
 */

package tcc.com.canopy.base;

import java.io.FileInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.http.impl.conn.SystemDefaultDnsResolver;
import org.apache.poi.xssf.usermodel.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import tcc.com.canopy.util.ErrorUtil;
import tcc.com.canopy.util.xls_reader;

public class TestBase {
	 public int excelRow=1 ;
	 public int 	 excelCol=0;
	private static final int Integer = 0;
	private static final String String = null;
	public  WebDriver driver =null;
	public static Properties OR=null;
	public static boolean isInitalized=false;
	public static xls_reader test_data_xls=new xls_reader(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls");
	public static boolean isBrowserOpened=false;
	public static boolean isURLOpened=false;
	public static int machine_id = 0 ;
	public static int row = 0;
	public static int columns=0;
	String time;
	public String screenshotPath;

	public void openBrowser()
	{
		if(!isBrowserOpened)
		{
			if(fn_getproperty("browserType").equals("MOZILLA"))
				driver = new FirefoxDriver();
			else if (fn_getproperty("browserType").equals("IE"))
				driver = new InternetExplorerDriver();
			else if (fn_getproperty("browserType").equals("CHROME"))
			{
				System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\chromedriver.exe" );
				driver = new ChromeDriver();
			}

			isBrowserOpened=true;
			String waitTime=fn_getproperty("default_implicitWait");
			driver.manage().timeouts().implicitlyWait(Long.parseLong(waitTime), TimeUnit.SECONDS);
			driver.manage().window().maximize();
		}

	}

	// close browser
	public void closeBrowser()
	{
		driver.quit();
		isBrowserOpened=false;
	}

	public static String fn_getproperty(String propertyname)
	{
		Properties prop = new Properties();
		InputStream input = null;
		try 
		{
			input = new FileInputStream(System.getProperty("user.dir")+"//src//tcc//com//canopy//config//config.properties");
			// load a properties file
			prop.load(input);
		} 

		catch (IOException ex) 
		{
			ex.printStackTrace();
		} 

		finally 
		{
			if (input != null) 
			{
				try 
				{
					input.close();
				}

				catch (IOException e) 
				{
					e.printStackTrace();
				}

			}
		}

		//Return the property value
		return prop.getProperty(propertyname);
	}


	public static String or_getproperty(String propertyname)
	{
		Properties prop = new Properties();
		InputStream input = null;

		try 
		{
			input = new FileInputStream(System.getProperty("user.dir")+"//src//tcc//com//canopy//config//OR.properties");
			// load a properties file
			prop.load(input);
		}

		catch (IOException ex) 
		{
			ex.printStackTrace();
		} 

		finally 
		{
			if (input != null)
			{
				try 
				{
					input.close();
				}

				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}

		//Return the property value
		return prop.getProperty(propertyname);
	}

	public void initialize() throws Exception
	{
		if(!isInitalized)
		{
			test_data_xls = new xls_reader(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\TestData.xls");
			isInitalized=true;
		}
	}

	public void writeVdcDetails(String RequestID, String SubmittedTime , String LastUpdated, String Status) throws IOException, RowsExceededException, WriteException, BiffException
	{
		Workbook workbook1 = Workbook.getWorkbook(new File(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls"));
		WritableWorkbook copy = Workbook.createWorkbook(new File(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls"), workbook1);
		WritableSheet sheet2 = copy.getSheet("VdcDetails") ;
		//System.out.println("I VALUE = "+excelRow+ "");
		 
	      //j =sheet2.getColumns();
	      //System.out.println("J VALUE = "+ excelCol);
	     // excelRow = sheet2.getRows();
	     // System.out.println("i="+ excelRow);
	      WritableCell cell = sheet2.getWritableCell(excelRow, excelCol);
	Label request_id = new Label( excelCol, excelRow, RequestID);
	                sheet2.addCell(request_id );
	                Label sub_time = new Label( excelCol+1, excelRow,SubmittedTime );
	               sheet2.addCell(sub_time);
	               Label last_updated= new Label( excelCol+2, excelRow, LastUpdated);
	                sheet2.addCell( last_updated);
	                Label status_vdc= new Label( excelCol+3, excelRow, Status);
	                sheet2.addCell(status_vdc);
	               
	                
	               // System.out.println("i="+excelRow + " excelCol="+  excelCol);
	      copy.write();
	      copy.close();
	         
	}

	public  WebElement getObject(String xpathKey)
	{
		try
		{
			new FluentWait<WebDriver>(driver)
			.withTimeout(900, TimeUnit.SECONDS)
			.pollingEvery(1, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(or_getproperty(xpathKey))));
			WebElement x = driver.findElement(By.xpath(or_getproperty(xpathKey)));
			return x;
		}

		catch(Throwable t)
		{
			ErrorUtil.addVerificationFailure(t);                   
			System.out.println("Cannot find object with key -- "+xpathKey);
			return null;
		}
	}

	public static boolean compareText(String expectedVal, String actualValue)
	{
		try
		{
			Assert.assertEquals(actualValue,expectedVal);
		}

		catch(Throwable t)
		{
			ErrorUtil.addVerificationFailure(t);                   
			System.out.println("Values do not match");
			return false;
		}

		return true;
	}


	public boolean checkElementPresence(String string)
	{
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + string + "')]"));

		try
		{
			Assert.assertTrue(list.size() > 0, "Text not found!");
		}

		catch(Throwable t)
		{
			ErrorUtil.addVerificationFailure(t);                   
			return false;
		}

		return true;
	}

	public  boolean isClickable(WebElement webE)        
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(webE));
			return true;
		}

		catch (Exception e)
		{
			return false;
		}
	}

	public WebElement Dropdown(String idkey)
	{
		WebElement DropDown = driver.findElement(By.id(idkey));
		return DropDown;
	}

	public boolean openURL() 
	{
		driver.get(TestBase.fn_getproperty("TCC"));

		try 
		{
			System.out.println("URL Opening");
		}

		catch (Throwable t) 
		{
			System.out.println("URL NOt Opening");
			return false;
		}

		isURLOpened=true;
		return isURLOpened;
	}

	public String CaptureScreenshot(String Error) throws IOException

	{

		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss_");

		Calendar cal = Calendar.getInstance();

		time = "" + dateFormat.format(cal.getTime());

		System.out.println("Done with browser part");

		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		// Now you can do whatever you need to do with it, for example copy somewhere

		screenshotPath = System.getProperty("user.dir")+"\\src\\Screenshots\\"+Error+"_"+time+".png";


		FileUtils.copyFile(scrFile, new File(screenshotPath));

		return screenshotPath;
	}
	

	public boolean retryingFindClick(By by) {
		boolean result = false;
		int attempts = 0;
		while(attempts < 2) {
			try {
				driver.findElement(by).click();
				result = true;
				break;
			} catch(StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}  
	
	




}
