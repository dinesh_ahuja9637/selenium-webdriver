/* 
==============================================================
 Author : IAAS Automation Test Team
 Purpose : Class for Verify VDC
 Date : 10/03/2016
 Test Case Name : 
 
 #======================================================================
 */package tcc.com.canopy.verifyVDC;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import tcc.com.canopy.util.ExcelReadAndWrite;
import tcc.com.canopy.util.TestUtil;
import tcc.com.canopy.base.TestBase;
import tcc.com.canopy.util.TestLog;

public class VdcItemVerify  extends VerifyBaseClass{

	boolean	result=false;

	String systemTime;
	int excelRowCount =1;
	String excelScreenshotPath;
	boolean skip=false;
	@BeforeTest
	public void isTestExecutable(){
		if(!TestUtil.isExecutable("TAI_VDC_Verify")){
			TestLog.info("Skipping execution of test case as Run mode is set to No");
			skip=true;
			throw new SkipException("Skipping execution of test case as Run mode is set to No");
		}

	}

		@Test(dataProvider="getVdcDetails")
		public void VdcVerify(String RequestID,
				String SubmittedTime , 
				String LastUpdated,
				String Status,
				String VdcName
				) throws InterruptedException, IOException, ParseException, BiffException, RowsExceededException, WriteException
		{ 
			if(!Status.equals("")){
			if(Status.contains("Successful")){
		
				openBrowser();
				driver.get(fn_getproperty("Outlook"));
				driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
				driver.findElement(By.xpath(or_getproperty("login_box"))).sendKeys(fn_getproperty("UserID"));
		        driver.findElement(By.xpath(or_getproperty("password_box"))).sendKeys(fn_getproperty("UserPassword"));
		        Thread.sleep(15000);
		    	driver.findElement(By.xpath(or_getproperty("login_button"))).click();
		    	Thread.sleep(30000);
		    	
		    	String numberofmails =driver.findElement(By.xpath(or_getproperty("mailitem"))).getText();
		        
		        
		        String array[]=numberofmails.split("\\s+");
		        System.out.println(array[0]); 
		        
		        if(array[0].contains("0")){
		        	System.out.println("All mails deleted");
		        }
		
		
		
		driver.findElement(By.xpath(or_getproperty("inbox"))).click();
		
		String Mailsender  = driver.findElement(By.xpath(or_getproperty("sender_details"))).getText();

		if(Mailsender.contains("Canopy Cloud")){
			
			Thread.sleep(15000);
			String mailTime = driver.findElement(By.xpath(or_getproperty("sender_time"))).getText();
		}

		else{
			System.out.println("sender invalid");
			 Assert.assertTrue(false, "sender invalid");
		}



		Thread.sleep(15000);
	    String vdcName= driver.findElement(By.xpath(or_getproperty("vdc_name_email"))).getText();
		
	       //  System.out.println(vdcName);
	         
	         try 
				{
					Workbook workbook1 = Workbook.getWorkbook(new File("src\\tcc\\com\\canopy\\xls\\test_data.xls"));
					WritableWorkbook copy = Workbook.createWorkbook(new File("src\\tcc\\com\\canopy\\xls\\test_data.xls"), workbook1);
					WritableSheet sheet2 = copy.getSheet("VdcDetails"); 
					int r = 0 , c = 4;
					//System.out.println("r VALUE = "+r+ "");
					//System.out.println("c VALUE = "+c);
					//System.out.println(sheet2.findCell(RequestID).getRow());
					r= sheet2.findCell(RequestID).getRow();
					//System.out.println("r="+ r);
					WritableCell cell = sheet2.getWritableCell(r,c); 
					Label mn1 = new Label(c, r, vdcName); 
		
					sheet2.addCell(mn1); 
				
					copy.write();
					copy.close();
					result=true;
				}
				catch(IOException e)
				{
					excelScreenshotPath = CaptureScreenshot("worksheet not created");
				TestLog.info("worksheet not created");
				  Assert.assertTrue(false, "worksheet not created");
					
				}
		
		}
			else{
				excelScreenshotPath = CaptureScreenshot("Vdc not found");
				TestLog.info("vdc not created");
				  Assert.assertTrue(false, "vdc not found");
			}
			}
			else{
				TestLog.info("vdc status null");
				  Assert.assertTrue(false, "vdc status null");
			}
		closeBrowser();
		}
		
		
		@AfterMethod(alwaysRun=true)
		public void generateExcelReport()
		{

			if (driver!=null)
			{
				closeBrowser();
			}

			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
			Calendar cal = Calendar.getInstance();
			systemTime = ""+dateFormat.format(cal.getTime());
			excelRowCount++;

			if(!skip)
			{
				if(!result)
				{
					test_data_xls.setCellData("Suite", "Result", 5, "FAIL");
					test_data_xls.setCellData("Suite", "Execution Time", 5, systemTime);
					test_data_xls.setCellData("Suite", "Screenshot", 5, excelScreenshotPath);
				}

				else
				{
					test_data_xls.setCellData("Suite", "Result", 5, "PASS");
					test_data_xls.setCellData("Suite", "Execution Time", 5, systemTime);
				}
			}

			else 
			{
				test_data_xls.setCellData("Suite", "Result",5 , "SKIP");
				test_data_xls.setCellData("Suite", "Execution Time", 5, systemTime);
			}


		}

		@AfterTest
	    public void generateBackupReport() throws IOException
	    {
			ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls");
	    }
	@DataProvider(name="getVdcDetails")
	public Object[][] getData()
	{
		Object array[][]=tcc.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls","VdcDetails");
		return array;
	}
	}

