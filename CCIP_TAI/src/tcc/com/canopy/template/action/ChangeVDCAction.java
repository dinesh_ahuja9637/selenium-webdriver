/* 
==============================================================
 Author : IAAS Automation Test Team
 Purpose : Action to change VDC
 Date : 10/03/2016
 Test Case Name : TC_03_ChangeVDC
 
 #======================================================================
 */
package tcc.com.canopy.template.action;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import tcc.com.canopy.base.TestBase;
import tcc.com.canopy.util.ExcelReadAndWrite;
import tcc.com.canopy.util.TestLog;
import tcc.com.canopy.util.TestUtil;
public class ChangeVDCAction extends ActionBaseClass{

	boolean result=false;
	String systemTime;
    int excelRowCount =1;
    String excelScreenshotPath;
	boolean skip=false;
	String regionsGlobal;
	String twinGlobal;
	String siteGlobal;
	String extNetworkGlobal;
	String allocationGlobal;
	String backUpGlobal;
	String antivirusGlobal;
	String ipsGlobal;
	String computerserviceGlobal;
	String storagepolicyGlobal;
	String  virtualsecurityGlobal;
	String computerservicelevelGlobal;
	String packageexpansionGlobal;
	String storageeditionGlobal;
	String removestorageGlobal;
	String changedefaultGlobal;
	String exchangeremoveGlobal;
	@BeforeTest
	public void isTestExecutable(){
		if(!TestUtil.isExecutable("TAI_VDC_Change")){
			TestLog.info("Skipping execution of test case as Run mode is set to No");
			skip=true;
			throw new SkipException("Skipping execution of test case as Run mode is set to No");
		}

	}
	@Test(dataProvider="getVdcData")
	public void ChangeVdc1(String regions,
	String twin,
	String site,
	String extNetwork,
	String allocation,
	String backUp,
	String antivirus,
	String ips,
	String computerservice,
	String storagepolicy,
	String  virtualsecurity,
	String computerservicelevel,
	String packageexpansion,
	String storageedition,
	String removestorage,
	String changedefault,
	String exchangeremove
	) throws InterruptedException, IOException
	{
		
		 regionsGlobal=regions;
		 twinGlobal=twin;
		 siteGlobal=site;
		 extNetworkGlobal=extNetwork;
		 allocationGlobal=allocation;
		 backUpGlobal=backUp;
		 antivirusGlobal= antivirus;
		 ipsGlobal=ips;
		 computerserviceGlobal=computerservice;
	     storagepolicyGlobal= storagepolicy;
		 virtualsecurityGlobal=virtualsecurity;
		 computerservicelevelGlobal=computerservicelevel;
		 packageexpansionGlobal= packageexpansion;
		storageeditionGlobal=storageedition;
		removestorageGlobal=removestorage;
		changedefaultGlobal=changedefault;
		 exchangeremoveGlobal=exchangeremove;
		openBrowser();
		driver.get(fn_getproperty("TCC"));
		//driver.navigate().refresh();
		driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
		driver.findElement(By.xpath(or_getproperty("userName"))).sendKeys(fn_getproperty("requestor_username"));
		driver.findElement(By.xpath(or_getproperty("password"))).sendKeys(fn_getproperty("requestor_passwd"));
		driver.findElement(By.xpath(or_getproperty("login"))).click();
		Thread.sleep(15000);
		driver.findElement(By.xpath(or_getproperty("catalog"))).click();
		Thread.sleep(15000);
	
		List<WebElement> list=driver.findElements(By.tagName("iframe"));
		
	
	
        
        
        Thread.sleep(5000L); 
       driver.switchTo().frame(list.size()-1);

		
		
		Thread.sleep(15000);
		driver.findElement(By.linkText("Change virtual datacenter")).click();
		
		

		Thread.sleep(10000);
		driver.findElement(By.xpath(or_getproperty("vdc_change_request"))).click();
		Thread.sleep(15000);
		
		WebElement region= driver.findElement(By.xpath(or_getproperty("region")));
		region.click();
		Thread.sleep(6000);
		
		region.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ regions+TestBase.or_getproperty("dropDown2"))).click();
		
		Thread.sleep(15000);
		WebElement twinDropDown=driver.findElement(By.xpath(or_getproperty("twin_status_request")));
		twinDropDown.click();
		Thread.sleep(15000);
		twinDropDown.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ twin+TestBase.or_getproperty("dropDown2"))).click();
		
		Thread.sleep(15000);
	}
@Test(dataProvider="getVdcDetails")
	
	public void ChangeVdc2(String RequestID,
			String SubmittedTime , 
			String LastUpdated,
			String Status,
			String VdcName
			) throws InterruptedException, IOException, ParseException, BiffException, RowsExceededException, WriteException
	{ 
	Thread.sleep(15000);
		WebElement abc = driver.findElement(By.xpath(TestBase.or_getproperty("cancelVDC3")));
		abc.click();
		Thread.sleep(15000);
		
		java.util.List dropdown= abc.findElements(By.xpath(TestBase.or_getproperty("cancelVDC2")));
	 
	 int requestsVDCCount=dropdown.size();
	 
		Thread.sleep(15000);
		//ovdc-mec-tcc194 - BASIC
		boolean vdcFound=false;
		for(int i=1;i<=requestsVDCCount ;i++)
			
		{  vdcFound=true;
			String requestedVDC = abc.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+i+TestBase.or_getproperty("dropDown2"))).getText();
			
			 String[] totalPartsDesc  = requestedVDC.split(" ");
	        	
	        	String descPart1 = totalPartsDesc[0];
	        	String descPart3 = totalPartsDesc[2];
	        
	        	Thread.sleep(10000);
	        	if(!VdcName.equals("")){
			if(descPart1.equalsIgnoreCase(VdcName))  //VDC Name match
			{
				if(descPart3.equalsIgnoreCase("BASIC")){
				
				abc.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+i+TestBase.or_getproperty("dropDown2"))).click();
				TestLog.info("Requested vdc:"+requestedVDC);
				Thread.sleep(30000);
				driver.findElement(By.xpath(TestBase.or_getproperty("virtual_security_zone"))).click();
				
				Thread.sleep(15000);
				WebElement noOfSecurityzone=driver.findElement(By.xpath(or_getproperty("no_of_security_zone")));
				noOfSecurityzone.click();
				noOfSecurityzone.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+virtualsecurityGlobal+TestBase.or_getproperty("dropDown2"))).click();
				break;
				}
				
				else{
					Thread.sleep(15000);
					
					abc.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+i+TestBase.or_getproperty("dropDown2"))).click();
					TestLog.info("Requested vdc:"+requestedVDC);
					Thread.sleep(20000);
					driver.findElement(By.xpath(TestBase.or_getproperty("virtual_security_zone"))).click();
					Thread.sleep(15000);
					
					WebElement noOfSecurityzone=driver.findElement(By.xpath(or_getproperty("no_of_security_zone")));
					noOfSecurityzone.click();
					noOfSecurityzone.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+virtualsecurityGlobal+TestBase.or_getproperty("dropDown2"))).click();
					
					Thread.sleep(15000);
				
					 driver.findElement(By.xpath(TestBase.or_getproperty("computer_service_level"))).click();
					Thread.sleep(15000);
					WebElement computer_level=driver.findElement(By.xpath(or_getproperty("no_of_service_level")));
					computer_level.click();
					computer_level.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+computerservicelevelGlobal+TestBase.or_getproperty("dropDown2"))).click();
				
				      Thread.sleep(15000);
					 driver.findElement(By.xpath(TestBase.or_getproperty("expansion_storage_edition"))).click();
					Thread.sleep(15000);
					WebElement compute_package=driver.findElement(By.xpath(or_getproperty("no_of_package_expansion")));
					compute_package.click();
					compute_package.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+packageexpansionGlobal+TestBase.or_getproperty("dropDown2"))).click();
					
					
					
				      Thread.sleep(15000);
					 driver.findElement(By.xpath(TestBase.or_getproperty("exchange_remove"))).click();
					Thread.sleep(15000);
					WebElement expansion_remove=driver.findElement(By.xpath(or_getproperty("no_of_expansion_remove")));
					expansion_remove.click();
					expansion_remove.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+exchangeremoveGlobal+TestBase.or_getproperty("dropDown2"))).click();
					
					 Thread.sleep(15000);
					 driver.findElement(By.xpath(TestBase.or_getproperty("changestorage"))).click();
					Thread.sleep(15000);
					WebElement storage_edit=driver.findElement(By.xpath(or_getproperty("no_of_storage_expansion")));
					storage_edit.click();
					storage_edit.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+storageeditionGlobal+TestBase.or_getproperty("dropDown2"))).click();
					 
					 Thread.sleep(15000);
					 driver.findElement(By.xpath(TestBase.or_getproperty("remove_storage"))).click();
					Thread.sleep(15000);
					WebElement storage_remove=driver.findElement(By.xpath(or_getproperty("no_of_storage_remove")));
					storage_remove.click();
					storage_remove.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+removestorageGlobal+TestBase.or_getproperty("dropDown2"))).click();
					
					Thread.sleep(15000);
					 driver.findElement(By.xpath(TestBase.or_getproperty("change_default_storage"))).click();
					Thread.sleep(15000);
					WebElement default_storage=driver.findElement(By.xpath(or_getproperty("strageprovider")));
					 default_storage.click();
					 default_storage.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+changedefaultGlobal+TestBase.or_getproperty("dropDown2"))).click();
					
					break;
					
				}
				
			}
			 
	        	}
	        	else{
	        		TestLog.info("vdc name null");
					  Assert.assertTrue(false, "vdc name null");
	        	}
           		
		}
		
		
		
     if(vdcFound==false){
			
            excelScreenshotPath = CaptureScreenshot("Vdc not found");
            Assert.assertTrue(false, "Vdc not found");
            TestLog.info("Vdc not found");
		              } 
		
		Thread.sleep(15000);
		driver.findElement(By.xpath(or_getproperty("vdcSubmitBtn"))).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);      
        driver.findElement(By.xpath(TestBase.or_getproperty("confirmation_button"))).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);       
        driver.switchTo().defaultContent();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(TestBase.or_getproperty("request_tab"))).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(3000);
 List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));
		
        Thread.sleep(5000L);
       driver.switchTo().frame(ele1.size()-1);
      
        Thread.sleep(5000L);
        int rowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();
       
       
        
        Thread.sleep(20000L);
        
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement totalPageItems = driver.findElement(By.xpath(TestBase.or_getproperty("total_pages_items")));
		WebElement totalPages= wait.until(ExpectedConditions.visibilityOf(totalPageItems));
		String pageCount= totalPages.getText();
		String[] pageCountParts = pageCount.split(" ");
		
		String pageCountSecond = pageCountParts[1];
		int itemsTabTotalPages = Integer.parseInt(pageCountSecond);


		Outer:for(int k=1;k<=itemsTabTotalPages;k++)
		{
        for(int j=1;j<=rowCount;j++){
            String requestDesc = driver.findElement(By.xpath((TestBase.or_getproperty("item_drop1")+ j +TestBase.or_getproperty("item_drop2")))).getText();
            
            
           
            String[] totalPartsDesc  = requestDesc.split(" ");
        	
        	String descPart1 = totalPartsDesc[0]; 
        
        	Thread.sleep(10000);
        	
           
            //if(driver.findElement(By.xpath((TestBase.or_getproperty("description_drop1")+ i +TestBase.or_getproperty("description_drop2")))).getText() == Description)
            if(descPart1.equalsIgnoreCase("Change"))
            {
            	for( int m=0;m<60;m++ )
            	{
            		  Thread.sleep(50000);
            		driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
            		Thread.sleep(5000L);
            		 List<WebElement> ele2 = driver.findElements(By.tagName("iframe"));
     				
     				
                     Thread.sleep(5000L);
                    driver.switchTo().frame(ele2.size()-1);
                   
                     Thread.sleep(5000L);
                     wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ j +TestBase.or_getproperty("status_drop2"))))));
                    String requestStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ j +TestBase.or_getproperty("status_drop2")))).getText();
                    TestLog.info("Status is:"+requestStatus);
                    if(requestStatus.equalsIgnoreCase("Successful"))
                    {                result=true;
                    TestLog.info("Request Submitted Succesfully");
                                    Assert.assertTrue(true, "Request Submitted Succesfully");
                       break Outer;
                    }else if(requestStatus.equalsIgnoreCase("In Progress")|| requestStatus.contains("Approved")) 
                    {
                                    //Assert.assertTrue(false, "Request is in" +Status+ "status");
                    	TestLog.info("Request is in" +requestStatus+ "status");
                                    
                    }else
                    {                excelScreenshotPath = CaptureScreenshot("Request status");
                                    Assert.assertTrue(false, "Request is in" +requestStatus+ "status");
                                    TestLog.info(requestStatus);
                                   
                                    break;
                    }              
                


                    
            	}
        
            }
        
        }
		}
        closeBrowser();
	}
	
	
	@AfterTest(alwaysRun=true)
	public void generateExcelReport()
	{

		if (driver!=null)
		{
			closeBrowser();
		}

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		systemTime = ""+dateFormat.format(cal.getTime());
		excelRowCount++;

		if(!skip)
		{
			if(!result)
			{
				test_data_xls.setCellData("Suite", "Result", 3, "FAIL");
				test_data_xls.setCellData("Suite", "Execution Time", 3, systemTime);
				test_data_xls.setCellData("Suite", "Screenshot", 3, excelScreenshotPath);
			}

			else
			{
				test_data_xls.setCellData("Suite", "Result", 3, "PASS");
				test_data_xls.setCellData("Suite", "Execution Time", 3, systemTime);
			}
		}

		else 
		{
			test_data_xls.setCellData("Suite", "Result",3 , "SKIP");
			test_data_xls.setCellData("Suite", "Execution Time", 3, systemTime);
		}


	}
	//@AfterTest
	 public void generateBackupReport() throws IOException
	    {
			ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls");
	    }

	 @DataProvider(name="getVdcDetails")
		public Object[][] getDetails()
		{
			Object array[][]=tcc.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls","VdcDetails");
			return array;
		}
	@DataProvider(name="getVdcData")
	public Object[][] getData()
	{
		Object array[][]=tcc.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls","Basic");
		return array;
	}
}
