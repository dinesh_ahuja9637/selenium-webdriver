/* 
==============================================================
 Author : IAAS Automation Test Team
 Purpose : Action to cancel VDC
 Date : 10/03/2016
 Test Case Name : TC_04_CancelVDC
 
 #======================================================================
 */
package tcc.com.canopy.template.action;
 

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import tcc.com.canopy.base.TestBase;
import tcc.com.canopy.util.ExcelReadAndWrite;
import tcc.com.canopy.util.TestLog;
import tcc.com.canopy.util.TestUtil;

public class CancelVDCAction extends ActionBaseClass{
	boolean result=false;
	
	 String systemTime;
    int excelRowCount =1;
    String excelScreenshotPath;
	boolean skip=false;
	@BeforeTest
	public void isTestExecutable(){
		if(!TestUtil.isExecutable("TAI_VDC_Cancel")){
			TestLog.info("Skipping execution of test case as Run mode is set to No");
			skip=true;
			throw new SkipException("Skipping execution of test case as Run mode is set to No");
		}

	}
	@Test(dataProvider="getVdcData")
	public void VdcCancel1(String regions,
			String twin,
			String site,
			String extNetwork,
			String allocation,
			String backUp,
			String antivirus,
			String ips,
			String computerservice,
			String storagepolicy,
			String  virtualsecurity,
			String computerservicelevel,
			String packageexpansion,
			String storageedition,
			String removestorage,
			String changedefault,
			String exchangeremove) throws InterruptedException, IOException
	{
		openBrowser();
		driver.get(fn_getproperty("TCC"));
		//driver.navigate().refresh();
		driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
		driver.findElement(By.xpath(or_getproperty("userName"))).sendKeys(fn_getproperty("requestor_username"));
		driver.findElement(By.xpath(or_getproperty("password"))).sendKeys(fn_getproperty("requestor_passwd"));
		driver.findElement(By.xpath(or_getproperty("login"))).click();
		Thread.sleep(15000);
		driver.findElement(By.xpath(or_getproperty("catalog"))).click();
		Thread.sleep(15000);
	
		List<WebElement> list=driver.findElements(By.tagName("iframe"));





	
        
        
        Thread.sleep(5000L); 
       driver.switchTo().frame(list.size()-1);

		
		
		Thread.sleep(15000);
		driver.findElement(By.linkText("Cancel virtual datacenter")).click();
	
		
		
		
		Thread.sleep(10000);
		driver.findElement(By.xpath(or_getproperty("vdcReqBtn"))).click();
		Thread.sleep(15000);
		
		WebElement region= driver.findElement(By.xpath(or_getproperty("region")));
		region.click();
		Thread.sleep(6000);
		region.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ regions+TestBase.or_getproperty("dropDown2"))).click();
		
		Thread.sleep(15000);
		
		WebElement twinDropDown=driver.findElement(By.xpath(or_getproperty("canceltwin")));
		twinDropDown.click();
		Thread.sleep(15000);
		twinDropDown.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ twin+TestBase.or_getproperty("dropDown2"))).click();
		Thread.sleep(15000);
		
	}
	@Test(dataProvider="getVdcDetails")
	
	public void Vdccancel2(String RequestID,
			String SubmittedTime , 
			String LastUpdated,
			String Status,
			String VdcName
			) throws InterruptedException, IOException, ParseException, BiffException, RowsExceededException, WriteException
	{ 
		Thread.sleep(15000);
	WebElement abc = driver.findElement(By.xpath(TestBase.or_getproperty("cancelVDC3")));
	abc.click();
	Thread.sleep(3000);
	
	java.util.List dropdown= abc.findElements(By.xpath(TestBase.or_getproperty("cancelVDC2")));
 
 int requestsVDCCount=dropdown.size();


		
		//Sayli- Code to match VDC to drop down options start
		boolean vdcFound=false;
		//System.out.println(""+requestsVDCCount);
		for(int i=1;i<=requestsVDCCount ;i++)
		{
			 Thread.sleep(1500);
			String requestedVDC = abc.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+i+TestBase.or_getproperty("dropDown2"))).getText();
			//System.out.println(requestedVDC);
			//System.out.println(VdcName);
			if(!VdcName.equals("")){
			if(requestedVDC.contains(VdcName))  //VDC Name match
			{
				 vdcFound=true;
			
				abc.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+i+TestBase.or_getproperty("dropDown2"))).click();
				TestLog.info("Requested vdc:"+requestedVDC);
				break;
			}
			}
			else{
				TestLog.info("vdc name null");
				  Assert.assertTrue(false, "vdc name null");
			}
			
           		
		}
		if(vdcFound==false){
			
            excelScreenshotPath = CaptureScreenshot("Vdc not found");
            Assert.assertTrue(false, "Vdc not found");
            TestLog.info("Vdc not found");
		}
		
		//Sayli- Code to match VDC to drop down options end
		
		Thread.sleep(15000);
		driver.findElement(By.xpath(or_getproperty("vdcSubmitBtn"))).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);      
        driver.findElement(By.xpath(TestBase.or_getproperty("confirmation_button"))).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);       
        driver.switchTo().defaultContent();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(TestBase.or_getproperty("request_tab"))).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(3000);
        
        List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));
		
        Thread.sleep(5000L);
       driver.switchTo().frame(ele1.size()-1);
      
        Thread.sleep(5000L);
        int rowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();
       
       
        
        Thread.sleep(20000L);
        
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement totalPageItems = driver.findElement(By.xpath(TestBase.or_getproperty("total_pages_items")));
		WebElement totalPages= wait.until(ExpectedConditions.visibilityOf(totalPageItems));
		String pageCount= totalPages.getText();
		String[] pageCountParts = pageCount.split(" ");
		
		String pageCountSecond = pageCountParts[1];
		int itemsTabTotalPages = Integer.parseInt(pageCountSecond);


		Outer:for(int k=1;k<=itemsTabTotalPages;k++)
		{      
            for(int j=1;j<=rowCount;j++){
            String requestDesc = driver.findElement(By.xpath((TestBase.or_getproperty("item_drop1")+ j +TestBase.or_getproperty("item_drop2")))).getText();
            
            
           
            String[] totalPartsDesc  = requestDesc.split(" ");
        	
        	String descPart1 = totalPartsDesc[0]; 
        
        	Thread.sleep(10000);
        	
           
            //if(driver.findElement(By.xpath((TestBase.or_getproperty("description_drop1")+ i +TestBase.or_getproperty("description_drop2")))).getText() == Description)
            if(descPart1.equalsIgnoreCase("Cancel"))
            {
            	for( int m=0;m<60;m++ )
            	{
            		Thread.sleep(50000);
            		driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
            		Thread.sleep(5000L);
            		 List<WebElement> ele2 = driver.findElements(By.tagName("iframe"));
     				
     				
                     Thread.sleep(5000L);
                    driver.switchTo().frame(ele2.size()-1);
                   
                     Thread.sleep(50000);
                     wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ j +TestBase.or_getproperty("status_drop2"))))));
                    String requestStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ j +TestBase.or_getproperty("status_drop2")))).getText();
                    TestLog.info("Status is:"+requestStatus);
                    if(requestStatus.equalsIgnoreCase("Successful"))
                    {                result=true;
                    TestLog.info("Request Submitted Succesfully");
                                    Assert.assertTrue(true, "Request Submitted Succesfully");
                                    break Outer;
                        
                    }else if(requestStatus.equals("In Progress")|| requestStatus.contains("Approved")) 
                    {
                                    //Assert.assertTrue(false, "Request is in" +Status+ "status");
                    	TestLog.info("Request is in" +requestStatus+ "status");
                                    
                    }else
                    {
                    	 excelScreenshotPath = CaptureScreenshot("Request status"+requestStatus);
                                    Assert.assertTrue(false, "Request is in" +requestStatus+ "status");
                                    TestLog.info(requestStatus);
                                                       }              
                    


                    
            	}
        
            }
        
        }
		}
        closeBrowser();
	}
	
	
	@AfterTest(alwaysRun=true)
	public void generateExcelReport()
	{

		if (driver!=null)
		{
			closeBrowser();
		}

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		systemTime = ""+dateFormat.format(cal.getTime());
		excelRowCount++;

		if(!skip)
		{
			if(!result)
			{
				test_data_xls.setCellData("Suite", "Result", 4, "FAIL");
				test_data_xls.setCellData("Suite", "Execution Time", 4, systemTime);
				test_data_xls.setCellData("Suite", "Screenshot", 4, excelScreenshotPath);
			}

			else
			{
				test_data_xls.setCellData("Suite", "Result", 4, "PASS");
				test_data_xls.setCellData("Suite", "Execution Time", 4, systemTime);
			}
		}

		else 
		{
			test_data_xls.setCellData("Suite", "Result",4 , "SKIP");
			test_data_xls.setCellData("Suite", "Execution Time", 4, systemTime);
		}


	}
//	@AfterTest
	 public void generateBackupReport() throws IOException
	    {
			ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls");
	    }


	@DataProvider(name="getVdcDetails")
	public Object[][] getDetails()
	{
		Object array[][]=tcc.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls","VdcDetails");
		return array;
	}
	
	@DataProvider(name="getVdcData")
	public Object[][] getData()
	{
		Object array[][]=tcc.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls","Basic");
		return array;
	}
}
