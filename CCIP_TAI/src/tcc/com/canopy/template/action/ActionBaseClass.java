/* 
==============================================================
 Author : IAAS Automation Test Team
 Purpose : Base class for VDC actions
 Date : 10/03/2016
 Test Case Name : 
 
 #======================================================================
 */
package tcc.com.canopy.template.action;
import java.io.IOException;

import org.apache.log4j.xml.DOMConfigurator;
import org.testng.SkipException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import tcc.com.canopy.util.ExcelReadAndWrite;
import tcc.com.canopy.util.Constants;
import tcc.com.canopy.util.ExcelReadAndWrite;
import tcc.com.canopy.util.TestLog;
import tcc.com.canopy.util.TestUtil;
import tcc.com.canopy.base.TestBase;
public class ActionBaseClass extends TestBase {
	 @BeforeSuite
	 	public void checkSuiteSkip() throws Exception
	 	{
	 		DOMConfigurator.configure("log4j.xml");
	 		
	 		
	 		if(!TestUtil.isSuiteExecutable("TAI_VDC_Action"))
	 		{
	 			TestLog.info("Skipping the execution of TAI_VDC_Action Suite as RunMode is Set to No");
	 			throw new SkipException("Runmode of TAI_VDC_Action suite set to no. So Skipping all tests in TAI_VDC_Action suite");
	 		}
	 		
	 	
	 			

	 		}
	 	
	 	
	 	 @AfterSuite
	      public void generateBackupReports() throws IOException
	      {
	      	ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls");
	      }
	}
