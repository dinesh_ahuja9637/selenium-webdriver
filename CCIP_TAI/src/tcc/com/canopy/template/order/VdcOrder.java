/* 
==============================================================
 Author : IAAS Automation Test Team
 Purpose : Creation VDC request
 Date : 10/03/2016
 Test Case Name : TC_01_OrderVDC
 
 #======================================================================
 */package tcc.com.canopy.template.order;
 

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import tcc.com.canopy.util.ExcelReadAndWrite;
import tcc.com.canopy.util.TestUtil;
import tcc.com.canopy.base.TestBase;
import tcc.com.canopy.util.TestLog;

public class VdcOrder extends OrderBaseClass{
	boolean result;
	 String systemTime;
     int excelRowCount =1;
     String excelScreenshotPath;
	boolean skip=false;
	@BeforeTest
	public void isTestExecutable(){
		if(!TestUtil.isExecutable("TAI_VDC_Order")){
			TestLog.info("Skipping execution of test case as Run mode is set to No");
			skip=true;
			throw new SkipException("Skipping execution of test case as Run mode is set to No");
		}

	}
	
	@Test(dataProvider="getVdcData")
	public void VdcOrder(String regions,
			String twin,
			String site,
			String extNetwork,
			String allocation,
			String backUp,
			String antivirus,
			String ips,
			String computerservice,
			String storagepolicy,
			String  virtualsecurity,
			String computerservicelevel,
			String packageexpansion,
			String storageedition,
			String removestorage,
			String changedefault,
			String exchangeremove) throws InterruptedException, IOException, RowsExceededException, WriteException, BiffException
	{
		result=false;
		openBrowser();
		driver.get(fn_getproperty("TCC"));
		//driver.navigate().refresh();
		driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
		driver.findElement(By.xpath(or_getproperty("userName"))).sendKeys(fn_getproperty("requestor_username"));
		driver.findElement(By.xpath(or_getproperty("password"))).sendKeys(fn_getproperty("requestor_passwd"));
		driver.findElement(By.xpath(or_getproperty("login"))).click();
		Thread.sleep(15000);
		driver.findElement(By.xpath(or_getproperty("catalog"))).click();
		Thread.sleep(15000);
	
		List<WebElement> list=driver.findElements(By.tagName("iframe"));
		
	

		//List<WebElement> ele2 = driver.findElements(By.tagName("iframe")); 
        
        
        Thread.sleep(5000L); 
       driver.switchTo().frame(list.size()-1);

		
		
		Thread.sleep(15000);
		driver.findElement(By.linkText("Order virtual datacenter")).click();
	
		
		
		
		Thread.sleep(10000);
		driver.findElement(By.xpath(or_getproperty("vdcReqBtn"))).click();
		Thread.sleep(15000);
		
		WebElement region= driver.findElement(By.xpath(or_getproperty("region")));
		region.click();
		Thread.sleep(6000);
		
		region.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ regions+TestBase.or_getproperty("dropDown2"))).click();
		
		//region.findElement(By.xpath(TestBase.or_getproperty("dropDown"))).click();
		
		WebElement twinDropDown=driver.findElement(By.xpath(or_getproperty("twin")));
		twinDropDown.click();
		Thread.sleep(15000);
		twinDropDown.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ twin+TestBase.or_getproperty("dropDown2"))).click();
		Thread.sleep(30000);
		WebElement vdcSite=driver.findElement(By.xpath(or_getproperty("site")));
		vdcSite.click();
		Thread.sleep(15000);
		vdcSite.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+site+TestBase.or_getproperty("dropDown2"))).click();
		
		WebElement externalNetwork=driver.findElement(By.xpath(or_getproperty("extNetwork")));
		externalNetwork.click();
		Thread.sleep(15000);
		externalNetwork.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ extNetwork+TestBase.or_getproperty("dropDown2"))).click();
		
		WebElement vdcAllocationModel=driver.findElement(By.xpath(or_getproperty("allocation")));
		vdcAllocationModel.click();
		Thread.sleep(10000);
		vdcAllocationModel.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ allocation+TestBase.or_getproperty("dropDown2"))).click();
		
		 if(allocation.equalsIgnoreCase("2")){
			 
			 Thread.sleep(15000);
			 WebElement services =driver.findElement(By.xpath(or_getproperty("computer_services")));
		     services.click(); 
			
			services.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ computerservice+TestBase.or_getproperty("dropDown2"))).click();
			
			Thread.sleep(15000);
			WebElement storage =driver.findElement(By.xpath(or_getproperty("default_storage")));
			storage.click(); 
			services.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+storagepolicy+TestBase.or_getproperty("dropDown2"))).click();
			
		 }
		 Thread.sleep(15000);
		WebElement backupOption=driver.findElement(By.xpath(or_getproperty("backup")));
		backupOption.click();
		Thread.sleep(6000);
		backupOption.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ backUp+TestBase.or_getproperty("dropDown2"))).click();
		
		WebElement disableAntivirus=driver.findElement(By.xpath(or_getproperty("antivirus")));
		
		
		WebElement numberOfIps=driver.findElement(By.xpath(or_getproperty("noOfIps")));
		
		Actions action1 = new Actions(driver);
		action1.moveToElement(numberOfIps).click().perform();
		
		//numberOfIps.click();
		Thread.sleep(15000);
		numberOfIps.findElement(By.xpath(TestBase.or_getproperty("dropDown1")+ips+ TestBase.or_getproperty("dropDown2"))).click();
		
		Thread.sleep(15000);
		driver.findElement(By.xpath(or_getproperty("vdcSubmitBtn"))).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);      
        driver.findElement(By.xpath(TestBase.or_getproperty("confirmation_button"))).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);       
        driver.switchTo().defaultContent();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(TestBase.or_getproperty("request_tab"))).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(3000);
        
        List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));
		
        Thread.sleep(5000L);
       driver.switchTo().frame(ele1.size()-1);
      
        Thread.sleep(5000L);
        int rowCount = driver.findElements(By.xpath(TestBase.or_getproperty("row_count_desc"))).size();
       
     
        
        Thread.sleep(20000L);
        
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement totalPageItems = driver.findElement(By.xpath(TestBase.or_getproperty("total_pages_items")));
		WebElement totalPages= wait.until(ExpectedConditions.visibilityOf(totalPageItems));
		String pageCount= totalPages.getText();
		String[] pageCountParts = pageCount.split(" ");
		
		String pageCountSecond = pageCountParts[1];
		int itemsTabTotalPages = Integer.parseInt(pageCountSecond);
		Thread.sleep(10000);
	/*
		driver.findElement(By.xpath(TestBase.or_getproperty("last_updated_row"))).click();
		Thread.sleep(10000);
	
		driver.findElement(By.xpath(TestBase.or_getproperty("last_updated_row"))).click();
*/


		Outer:for(int k=1;k<=itemsTabTotalPages;k++)
		{
         for(int j=1;j<=rowCount;j++){
        	 
        	
            String requestDesc = driver.findElement(By.xpath((TestBase.or_getproperty("item_drop1")+ j +TestBase.or_getproperty("item_drop2")))).getText();
            
        	

           
        	Thread.sleep(10000);
        	
           
            //if(driver.findElement(By.xpath((TestBase.or_getproperty("description_drop1")+ i +TestBase.or_getproperty("description_drop2")))).getText() == Description)
            if( requestDesc.contains("Order virtual datacenter"))
            {
            	 
            	 
            	for( int m=0;m<60;m++ )
            	{
            	    Thread.sleep(50000);
            		driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
            		Thread.sleep(5000L);
            		 List<WebElement> ele2 = driver.findElements(By.tagName("iframe"));
     				
     				
                     Thread.sleep(5000L);
                    driver.switchTo().frame(ele2.size()-1);
                   
                     Thread.sleep(60000L);
                    
                    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ j +TestBase.or_getproperty("status_drop2"))))));
                    String requestStatus = driver.findElement(By.xpath((TestBase.or_getproperty("status_drop1")+ j +TestBase.or_getproperty("status_drop2")))).getText();
                    TestLog.info("Status is:"+requestStatus);
                   
                    if(requestStatus.contains("Successful"))
                    {                
                    	result=true;
                    TestLog.info("Request Submitted Succesfully");
                    String RequestId = driver.findElement(By.xpath(TestBase.or_getproperty("requestid_drop1")+ j + TestBase.or_getproperty("requestid_drop2"))).getText();
                    Thread.sleep(15000);
                    String submitted_time= driver.findElement(By.xpath(TestBase.or_getproperty("submitted_time1")+ j + TestBase.or_getproperty("submitted_time2"))).getText();
                    Thread.sleep(15000);
                    String last_updated = driver.findElement(By.xpath(TestBase.or_getproperty("last_updated1")+ j + TestBase.or_getproperty("last_updated2"))).getText();
                    writeVdcDetails(RequestId, submitted_time, last_updated,requestStatus);
                
                                    Assert.assertTrue(true, "Request Submitted Succesfully");
                      break Outer;
                        
                    }else if(requestStatus.contains("In Progress") || requestStatus.contains("Approved")) 
                    {
                                    //Assert.assertTrue(false, "Request is in" +Status+ "status");
                    	
                    	TestLog.info("Request is in" +requestStatus+ "status");
                                    
                    }else
                    {
                                    Assert.assertTrue(false, "Request is in" +requestStatus+ "status");
                                	
                                	excelScreenshotPath = CaptureScreenshot("Request status");
                                    TestLog.info(requestStatus);
                    }              
                    


                    
            	}
        
            }
        
        }
		}
		
        closeBrowser();
	}
	
	
	@AfterMethod(alwaysRun=true)
	public void generateExcelReport()
	{

		if (driver!=null)
		{
			closeBrowser();
		}

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		systemTime = ""+dateFormat.format(cal.getTime());
		excelRowCount++;

		if(!skip)
		{
			if(!result)
			{
				test_data_xls.setCellData("Suite", "Result", 2, "FAIL");
				test_data_xls.setCellData("Suite", "Execution Time", 2, systemTime);
				test_data_xls.setCellData("Suite", "Screenshot", 2, excelScreenshotPath);
			}

			else
			{
				test_data_xls.setCellData("Suite", "Result", 2, "PASS");
				test_data_xls.setCellData("Suite", "Execution Time", 2, systemTime);
			}
		}

		else 
		{
			test_data_xls.setCellData("Suite", "Result",2 , "SKIP");
			test_data_xls.setCellData("Suite", "Execution Time", 2, systemTime);
		}


	}

  @AfterTest
    public void generateBackupReport() throws IOException
    {
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls");
    }

	@DataProvider(name="getVdcData")
	public Object[][] getData()
	{
		Object array[][]=tcc.com.canopy.util.xls_reader.getExcelData(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls","Basic");
		return array;
	}
}
