/* 
==============================================================
 Author : IAAS Automation Test Team
 Purpose : Base class for creation VDC
 Date : 10/03/2016
 Test Case Name : NA
 
 #======================================================================
 */
package tcc.com.canopy.template.order;

import java.io.IOException;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import tcc.com.canopy.util.ExcelReadAndWrite;
import tcc.com.canopy.util.Constants;
import tcc.com.canopy.util.ExcelReadAndWrite;
import tcc.com.canopy.util.TestLog;
import tcc.com.canopy.util.TestUtil;
import tcc.com.canopy.base.TestBase;

public class OrderBaseClass extends TestBase {

	@BeforeSuite
	public void checkSuiteSkip() throws Exception
	{
		DOMConfigurator.configure("log4j.xml");


		if(!TestUtil.isSuiteExecutable("TAI_VDC_Order"))
		{
			TestLog.info("Skipping the execution of TAI_VDC_Order Suite as RunMode is Set to No");
			throw new SkipException("Runmode of TAI_VDC_Order suite set to no. So Skipping all tests in TAI_VDC_Order suite");
		}
		else
		{
			String numbermailtxt;
			openBrowser();
			driver.get(fn_getproperty("Outlook"));
			driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
			driver.findElement(By.xpath(or_getproperty("login_box"))).sendKeys(fn_getproperty("UserID"));
	        driver.findElement(By.xpath(or_getproperty("password_box"))).sendKeys(fn_getproperty("UserPassword"));
	        Thread.sleep(15000);
	    	driver.findElement(By.xpath(or_getproperty("login_button"))).click();
	    	Thread.sleep(30000);
	    	
	    	String numberofmails =driver.findElement(By.xpath(or_getproperty("mailitem"))).getText();
	        
	        
	        String array[]=numberofmails.split("\\s+");
	        System.out.println(array[0]); 
	        
	        if(array[0].contains("0")){
	        	System.out.println("All mails deleted");
	        }

			else
			{

				 Actions action = new Actions(driver);
			    	WebElement inboxClick = driver.findElement(By.xpath(or_getproperty("inbox")));
			    	action.contextClick(inboxClick).perform();
			    	Thread.sleep(5000);
			    	driver.findElement(By.xpath(or_getproperty("empty_inbox"))).click();
			    	Thread.sleep(15000);
			    	driver.findElement(By.xpath(or_getproperty("conformation_delete"))).click();
			    	Thread.sleep(15000);
			    	driver.findElement(By.xpath("//body")).sendKeys(Keys.F5);
				Thread.sleep(15000);

				numbermailtxt =driver.findElement(By.xpath(or_getproperty("mailitem"))).getText();


				if(numbermailtxt.contains("0"))
				{
					TestLog.info("Deleted all the mails");
					Assert.assertTrue(true, "deleted mail");
				}
				else
				{
					TestLog.info("issue with operation");
					Assert.assertTrue(true, "ERROR");
				}
			}
	        driver.findElement(By.xpath(or_getproperty("sign_out_outlook"))).click();
			closeBrowser();


		}
	}


	@AfterSuite
	public void generateBackupReports() throws IOException
	{
		ExcelReadAndWrite.backupReport(System.getProperty("user.dir")+"\\src\\tcc\\com\\canopy\\xls\\test_data.xls");
	}
}
