package com.canopy.suite.registration;

import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.TestUtil;

public class TestCase_C2 extends TestSuiteBase{
	
	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_registration_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_registration_xls, this.getClass().getSimpleName());
	}
	
	
	   @Test(dataProvider="getTestData")
		public void testCaseC2(){
		   

			// test the runmode of current dataset
					count++;
					if(!runmodes[count].equalsIgnoreCase("Y")){
						skip=true;
						throw new SkipException("Runmode for test set data set to no "+count);
					}
			
		   
			System.out.println("Executing Test Case C2");
			
		
		}
	   
	   
	   @AfterMethod
		public void reportDataSetResult(){
			if(skip)
				TestUtil.reportDataSetResult(suite_registration_xls, this.getClass().getSimpleName(), count+2, "SKIP");
			else if(fail){
				isTestPass=false;
				TestUtil.reportDataSetResult(suite_registration_xls, this.getClass().getSimpleName(), count+2, "FAIL");
			}
			else
				TestUtil.reportDataSetResult(suite_registration_xls, this.getClass().getSimpleName(), count+2, "PASS");
			
			skip=false;
			fail=false;
			

		}
		
		@AfterTest
		public void reportTestResult(){
			if(isTestPass)
				TestUtil.reportDataSetResult(suite_registration_xls, "Test Cases", TestUtil.getRowNum(suite_registration_xls,this.getClass().getSimpleName()), "PASS");
			else
				TestUtil.reportDataSetResult(suite_registration_xls, "Test Cases", TestUtil.getRowNum(suite_registration_xls,this.getClass().getSimpleName()), "FAIL");
		
		}
		
		
		@DataProvider
		public Object[][] getTestData(){
			return TestUtil.getData(suite_registration_xls, this.getClass().getSimpleName());
			
		}
	    
	    
	    

}
