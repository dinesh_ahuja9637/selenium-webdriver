package com.canopy.suite.store;

import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;

import com.canopy.base.TestBase;
import com.canopy.util.TestUtil;

public class TestSuiteBase extends TestBase{

	// check if the suite ex has to be skiped
		@BeforeSuite
		public void checkSuiteSkip() throws Exception{
			initialize();
			APP_LOGS.debug("Checking Runmode of Store suite");
			if(!TestUtil.isSuiteRunnable(suiteXls, "Store suite")){
				APP_LOGS.debug("Skipped Store suite as the runmode was set to NO");
				throw new SkipException("RUnmode of Store suite set to no. So Skipping all tests in Store suite");
			}
			
		}
}
