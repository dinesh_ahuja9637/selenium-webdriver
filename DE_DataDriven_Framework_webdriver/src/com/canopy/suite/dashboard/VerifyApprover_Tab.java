package com.canopy.suite.dashboard;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyApprover_Tab extends TestSuiteBase {
	

	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_dashboard_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_dashboard_xls, this.getClass().getSimpleName());
	}
	
	
	
	@Test(dataProvider="getTestData")
	public void testApprover_Tab() throws InterruptedException{
		
		
		// test the runmode of current dataset
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			skip=true;
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		
		APP_LOGS.debug("Executing Test Case testApprover_Tab");
		
		openBrowser();
        //driver.get(CONFIG.getProperty("testSiteUrlName"));
 		driver.get(CONFIG.getProperty("dashboardSiteUrlName"));
 		getObject("dashboardUserName").sendKeys("check1@yopmail.com");
 		getObject("dashboardPassword").sendKeys("Narendra@123$");
 		getObject("dashboardLoginButton").click();
 		
 		Thread.sleep(10000L);
 		String actualText =getObject("dashboardText").getText();
 		
 		if(!compareText("Dashboard", actualText)){
 			fail=true;
 			APP_LOGS.debug("Unable to login successfully into dashboard ");
 			return;
 			
 		}
 		
        WebElement DashboardSideBar= getObject("dashboardLeftBox");
        int size =DashboardSideBar.findElements(By.xpath("//*[@id='sidebar']/li/a")).size() ;
        for(int i=3;i<=size;i++){
        	
             	
        	
        	     if(!driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText().equals("Support")){
        	             driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i)+"]/a")).click();
        	              int sectiontab =driver.findElements(By.xpath("//*[@id='sidebar']/li["+i+"]/ul/li/a")).size();
                	      if(getObject("dashboardPageHeading").getText().equals("Dashboard")){
                		         if(!compareText("Dashboard", getObject("dashboardPageHeading").getText())){
                			         fail=true;
                			         APP_LOGS.debug("Dashboard Page is not opening");
                		           }
        		 
             	             }else if(sectiontab!=0){
             	            	    if(driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText().equals("My Account")){
             	            	    	  if(!compareNumbers(3, sectiontab)){
             	            	    		  fail=true;
             	            	    		  APP_LOGS.debug("My Account section doesnot have expected number of tab "+sectiontab);
             	            	    	  }
             	            	    }
             	            	 
             	            	   if(driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText().equals("Organisation")){
          	            	    	  if(!compareNumbers(4, sectiontab)){
          	            	    		  fail=true;
          	            	    		  APP_LOGS.debug("Organisation section doesnot have expected number of tab "+sectiontab);
          	            	    	  }
          	            	    }
             	            	  /*  
             	            	  if(driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText().equals("Policy Management")){
          	            	    	  if(!compareNumbers(1, sectiontab)){
          	            	    		  fail=true;
          	            	    		  APP_LOGS.debug("Policy Management section doesnot have expected number of tab "+sectiontab);
          	            	    	  }
          	            	    } 
             	            	 
             	            	 if(driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText().equals("Administration")){
         	            	    	  if(!compareNumbers(2, sectiontab)){
         	            	    		  fail=true;
         	            	    		  APP_LOGS.debug("Administration section doesnot have expected number of tab "+sectiontab);
         	            	    	  }
         	            	    }
             	            	  */
             	            	  
              		               for(int j=1;j<=sectiontab;j++){
              		                 try{
             			                     driver.findElement(By.xpath("//*[@id='sidebar']/li["+i+"]/ul/li["+j+"]/a")).click();
             			                     Thread.sleep(5000L);
             			                 }catch(Throwable t){
             			                	 fail=true;
             			        	       APP_LOGS.debug("Unable to click on the link in  "+driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText());
             			           }
              		               
             			           APP_LOGS.debug("Page Open properly "+ getObject("dashboardPageHeading").getText());
             		                  }
              		          Thread.sleep(5000L);
              	             }
             	              else if(getObject("dashboardPageHeading").getText().equals("Solutions")){
             		                  if(!compareText("Solutions", getObject("dashboardPageHeading").getText())){
            			              fail=true;
            			              APP_LOGS.debug("Solutions Page is not opening");
            		                 }

             	            } else if(!driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText().equals("Support") || !driver.findElement(By.xpath("//*[@id='page-heading']/h1/span")).getText().equals("Solutions") || sectiontab!=0){
             		 
             		                  fail=true;
             		                  APP_LOGS.debug("Unable to open the Link");
             		                  return;
             		 
             	            }
        	
        	
        }	
        	     
        	     if(driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText().equals("Support")){
      	     	   driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a")).click();
          	       getObject("signOutDropDown").click();
      	    	   Thread.sleep(30000L);
      	    	   getObject("signoutButton").click();
      	    	   Thread.sleep(20000L);
      	    	   if(compareText("Login to your dashboard", getObject("dashboardloginPageText").getText())){
      	    		 fail=true;
      	    		 APP_LOGS.debug("Unable to Logout from dashboard successfully");
      	    		 
      	    	    }
      	     	
      	     	
              	}
        	     
    }
   

        
        
}
	
	 @AfterMethod
		public void reportDataSetResult(){
			if(skip)
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "SKIP");
			else if(fail){
				isTestPass=false;
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "FAIL");
			}
			else
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "PASS");
			
			skip=false;
			fail=false;
			

		}
		
		@AfterTest
		public void reportTestResult(){
			if(isTestPass)
				TestUtil.reportDataSetResult(suite_dashboard_xls, "Test Cases", TestUtil.getRowNum(suite_dashboard_xls,this.getClass().getSimpleName()), "PASS");
			else
				TestUtil.reportDataSetResult(suite_dashboard_xls, "Test Cases", TestUtil.getRowNum(suite_dashboard_xls,this.getClass().getSimpleName()), "FAIL");
		
		}
		
		
		@DataProvider
		public Object[][] getTestData(){
			return TestUtil.getData(suite_dashboard_xls, this.getClass().getSimpleName());
			
		}
}
