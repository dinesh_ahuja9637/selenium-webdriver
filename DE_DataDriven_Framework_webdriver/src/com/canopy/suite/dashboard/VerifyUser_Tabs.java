package com.canopy.suite.dashboard;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyUser_Tabs extends TestSuiteBase {
	

	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_dashboard_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_dashboard_xls, this.getClass().getSimpleName());
	}
	
	
	
	@Test(dataProvider="getTestData")
	public void testUser_Tabs(String username,
			                  String password,
			                  String role,
			                  String dashboard,
			                  String solution,
			                  String myAccount,
			                  String myaccountCount,
			                  String organisation,
			                  String organisationCount,
			                  String policy,
			                  String policyCount,
			                  String administration,
			                  String administrationCount,
			                  String support
			) throws InterruptedException{
		
		
		// test the runmode of current dataset
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			skip=true;
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		
		APP_LOGS.debug("Executing Test Case VerifyUser_Tabs");
		
		openBrowser();
        //driver.get(CONFIG.getProperty("testSiteUrlName"));
 		driver.get(CONFIG.getProperty("dashboardSiteUrlName"));
 		getObject("dashboardUserName").sendKeys(username);
 		getObject("dashboardPassword").sendKeys(password);
 		getObject("dashboardLoginButton").click();
 		
 		Thread.sleep(5000L);
 		String actualText =getObject("dashboardText").getText();
 		
 		if(!compareText("Dashboard", actualText)){
 			fail=true;
 			APP_LOGS.debug("Unable to login successfully into dashboard ");
 			return;
 			
 		}
 		
        WebElement DashboardSideBar= getObject("dashboardLeftBox");
        int size =DashboardSideBar.findElements(By.xpath("//*[@id='sidebar']/li/a")).size() ;
        
        for(int i=3;i<=size;i++){
        	 boolean ElementCount = driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i)+"]/a")).isDisplayed();
        	 if(ElementCount){
        		 driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i)+"]/a")).click();
        	 } 
        	// driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i)+"]/a")).click();
        	int sectiontab =driver.findElements(By.xpath("//*[@id='sidebar']/li["+i+"]/ul/li/a")).size();
        	
        	if(dashboard.equalsIgnoreCase("Yes") && i==3){
        		if(!compareText("Dashboard", getObject("dashboardPageHeading").getText())){
			         fail=true;
			         APP_LOGS.debug("Dashboard Page is not opening");
		           }
     
        		
        	}
        	
            if(solution.equalsIgnoreCase("Yes") && i==4){
            	 if(!compareText("Solutions", getObject("dashboardPageHeading").getText())){
		              fail=true;
		              APP_LOGS.debug("Solutions Page is not opening");
	                 }
        	}
        	
            if(myAccount.equalsIgnoreCase("Yes") && i==5){
        		
            	if(role.equals("Registered") || role.equals("RegisteredVerified") || role.equals("Approver") || role.equals("Requestor") || role.equals("Admin")){
            		int AccountsectionCount=  Integer.parseInt(myaccountCount);
            		if(!compareNumbers(AccountsectionCount, sectiontab)){
            			fail=true;
            			APP_LOGS.debug("My Account Section doesnot have required number of tabs");
            		}
            		
            		for(int j=1;j<=sectiontab;j++){
 		                       try{
 		                    	   
			                          driver.findElement(By.xpath("//*[@id='sidebar']/li["+i+"]/ul/li["+j+"]/a")).click();
			                          Thread.sleep(2000L);
			                      }catch(Throwable t){
			                	     fail=true;
			        	              APP_LOGS.debug("Unable to click on the link in  "+driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText());
			                       }
 		               
			                       APP_LOGS.debug("Page Open properly "+ getObject("dashboardPageHeading").getText());
		                 }
            		
            	}
            	
            	
            	
        	}
            
            if(organisation.equalsIgnoreCase("Yes") && i==6){
            	int OrganisationsectionCount=  Integer.parseInt(organisationCount);
        		int count=0;
        		for(int j=1;j<=sectiontab;j++)
        		{
		                 try{
		                	 boolean organisationTab = driver.findElement(By.xpath("//*[@id='sidebar']/li["+i+"]/ul/li["+j+"]/a")).isDisplayed();
		                	 if(organisationTab){
		                		 driver.findElement(By.xpath("//*[@id='sidebar']/li["+i+"]/ul/li["+j+"]/a")).click();
		                		count = count +1 ;
		                	 } 
		                     Thread.sleep(2000L);
		                 }catch(Throwable t){
		                	 fail=true;
		        	         APP_LOGS.debug("Unable to click on the link in  "+driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText());
		           }
		               
		                    APP_LOGS.debug("Page Open properly "+ getObject("dashboardPageHeading").getText());
	            }
        		
        		
        		if(!compareNumbers(OrganisationsectionCount, count)){
        			fail=true;
        			APP_LOGS.debug("Organisation Section doesnot have required number of tabs");
        		}
        		
        	}
            
            if(policy.equalsIgnoreCase("Yes") && i==7){
        		
            	int PolicysectionCount=  Integer.parseInt(policyCount);
        		if(!compareNumbers(PolicysectionCount, sectiontab)){
        			fail=true;
        			APP_LOGS.debug("Policy Section doesnot have required number of tabs");
        		}
            	
        		for(int j=1;j<=sectiontab;j++)
        		{
		                 try{
		                     driver.findElement(By.xpath("//*[@id='sidebar']/li["+i+"]/ul/li["+j+"]/a")).click();
		                     Thread.sleep(2000L);
		                 }catch(Throwable t){
		                	 fail=true;
		        	       APP_LOGS.debug("Unable to click on the link in  "+driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText());
		           }
		               
		                    APP_LOGS.debug("Page Open properly "+ getObject("policyHeading").getText());
	            }
            	
        	}
            
           if(administration.equalsIgnoreCase("Yes") && (i+1)==8){
        		
        	 driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+1)+"]/a")).click();  
        	 sectiontab =driver.findElements(By.xpath("//*[@id='sidebar']/li["+(i+1)+"]/ul/li/a")).size();
        	 int AdminsectionCount=  Integer.parseInt(administrationCount);
       		if(!compareNumbers(AdminsectionCount, sectiontab)){
       			fail=true;
       			APP_LOGS.debug("Administration Section doesnot have required number of tabs");
       		}
        	   
       		for(int j=1;j<=sectiontab;j++)
    		{
	                 try{
	                     driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+1)+"]/ul/li["+j+"]/a")).click();
	                     Thread.sleep(2000L);
	                 }catch(Throwable t){
	                	 fail=true;
	        	       APP_LOGS.debug("Unable to click on the link in  "+driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a/span")).getText());
	           }
	               
	                    APP_LOGS.debug("Page Open properly "+ getObject("dashboardPageHeading").getText());
            }
       		
       		
        	}
           
           if(support.equalsIgnoreCase("Yes") && (i+2)==9){
      		 driver.findElement(By.xpath("//*[@id='sidebar']/li["+(i+2)+"]/a")).click();

       		
        	   if(!compareText("Dashboard", getObject("dashboardPageHeading").getText())){
		              fail=true;
		              APP_LOGS.debug("Dashboard Page is not opening");
	                 }
        	   
        	   getObject("signOutDropDown").click();
	    	   getObject("signoutButton").click();
	    	   if(!compareText("Login to your dashboard", getObject("dashboardloginPageText").getText())){
	    		 fail=true;
	    		 APP_LOGS.debug("Unable to Logout from dashboard successfully");
	    		 
	    	    }
	    	   Thread.sleep(5000L);
	    	   //driver.close();
        	   
        	   
       	    }
           
        }
     
        
}
	
	 @AfterMethod
		public void reportDataSetResult(){
			if(skip)
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "SKIP");
			else if(fail){
				isTestPass=false;
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "FAIL");
			}
			else
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "PASS");
			
			skip=false;
			fail=false;
			

		}
		
		@AfterTest
		public void reportTestResult(){
			if(isTestPass)
				TestUtil.reportDataSetResult(suite_dashboard_xls, "Test Cases", TestUtil.getRowNum(suite_dashboard_xls,this.getClass().getSimpleName()), "PASS");
			else
				TestUtil.reportDataSetResult(suite_dashboard_xls, "Test Cases", TestUtil.getRowNum(suite_dashboard_xls,this.getClass().getSimpleName()), "FAIL");
		
		}
		
		
		@DataProvider
		public Object[][] getTestData(){
			return TestUtil.getData(suite_dashboard_xls, this.getClass().getSimpleName());
			
		}
}
