package com.canopy.suite.dashboard;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.xmlbeans.impl.xb.xmlconfig.ConfigDocument.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;
import com.canopy.util.Xls_Reader;

public class VerifyReset extends TestSuiteBase{
	
	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	private WebElement product;
	
	
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_dashboard_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_dashboard_xls, this.getClass().getSimpleName());
	}
	
	@Test(dataProvider="getTestData")
	public void testReset() 
					throws IOException, InterruptedException{
		
		// test the runmode of current dataset
				count++;
				if(!runmodes[count].equalsIgnoreCase("Y")){
					skip=true;
					throw new SkipException("Runmode for test set data set to no "+count);
				}
		
		APP_LOGS.debug("Executing Test Case ProfileReset");
		openBrowser();
		openBrowser();
        //driver.get(CONFIG.getProperty("testSiteUrlName"));
 		driver.get(CONFIG.getProperty("dashboardSiteUrlName"));
 		getObject("dashboardUserName").sendKeys(CONFIG.getProperty("ApproverUserName"));
 		getObject("dashboardPassword").sendKeys(CONFIG.getProperty("ApproverPassword"));
 		getObject("dashboardLoginButton").click();
 		
 		Thread.sleep(5000L);
 		String actualText =getObject("dashboardText").getText();
 		
 		if(!compareText("Dashboard", actualText)){
 			fail=true;
 			APP_LOGS.debug("Unable to login successfully into dashboard ");
 			return;
 			
 		}
		Thread.sleep(5000);
		if(getObject("ProfileDashboard").isEnabled())  
	    getObject("ProfileDashboard").click(); //add to Or properties
		Thread.sleep(1000);
		getObject("MyAccount").click(); //add to Or properties
		String parentid= driver.getWindowHandle();
		getObject("ResetLinkProfile").click();  //add to Or properties
		Thread.sleep(3000);
		
		Set<String> windIds= driver.getWindowHandles();
	    Iterator<String> it= windIds.iterator();
	    String homepage= it.next();
	    String firstPopup=it.next();
        driver.switchTo().window(firstPopup);

/*
		for (String winHandle : driver.getWindowHandles()) {
                	driver.switchTo().window(winHandle); 
              	
		}
*/		
		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\Authentication.exe");
	   
		if(getObject("forgot_password_email_sumbit").isEnabled())  
		   getObject("forgot_password_email_sumbit").click();
		
	    String expectedResetText="An email containing a link to reset your password has been sent to your registered email address. Please check your email address.";
		  
		String Actualmessage =getObject("reset_password_message").getText();
		
		if(!compareText(expectedResetText,Actualmessage)){
			fail=true;
  			APP_LOGS.debug("Unable to Send Reset Password ");
		}
    	driver.close();
		driver.switchTo().window(parentid);
		Thread.sleep(2000);	
		 getObject("signOutDropDown").click();
    	   Thread.sleep(30000L);
    	   getObject("signoutButton").click();
    	   Thread.sleep(20000L);
    	   if(compareText("Login to your dashboard", getObject("dashboardloginPageText").getText())){
    		 fail=true;
    		 APP_LOGS.debug("Unable to Logout from dashboard successfully");
    		 
    	    }
		driver.get("http://www.yopmail.com/en/");
		getObject("yopmail_username").sendKeys(CONFIG.getProperty("ApproverUserName"));
		getObject("yopmail_sumbit").click();                
	   
		int size = driver.findElements(By.tagName("iframe")).size();
        driver.switchTo().frame("ifmail"); 
        String cid=driver.getWindowHandle();
        getObject("yopmail_email_body_reset_link").click();
        Thread.sleep(1000);
       // Runtime.getRuntime().exec("User.dir"+"\\Authentication.exe");

        for (String winHandle : driver.getWindowHandles()) {
        	 driver.switchTo().window(winHandle); 
        }
         getObject("reset_passowrd").sendKeys("Narendra@123!");
         getObject("reset_password_confirm").sendKeys("Narendra@123!");
         getObject("reset_password_sumbit").click();
         String actualtext= getObject("marketPlace").getText();
         if(!compareText("Market Place", actualtext)){
         	fail=true;
         	APP_LOGS.debug("Password is not reset Successfully");
         	driver.quit();
         	return;
         }
         
         driver.quit();
	
}
	
		  					
            
	
	
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "SKIP");
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "FAIL");
		}
		else
			TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "PASS");
		
		skip=false;
		fail=false;
		

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(suite_dashboard_xls, "Test Cases", TestUtil.getRowNum(suite_dashboard_xls,this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(suite_dashboard_xls, "Test Cases", TestUtil.getRowNum(suite_dashboard_xls,this.getClass().getSimpleName()), "FAIL");
	
	}
	
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(suite_dashboard_xls, this.getClass().getSimpleName());
		
	}
	

}
