package com.canopy.suite.dashboard;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyUserProfile extends TestSuiteBase {
	

	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_dashboard_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_dashboard_xls, this.getClass().getSimpleName());
	}
	
	
	
	@Test(dataProvider="getTestData")
	public void testUser_Tabs(String username,
			                  String password,
			                  String role,
			                  String emailVerified,
			                  String custId,
			                  String userProfile,
			                  String companyProfile,
			                  String profileEditable,
			                  String organisation,
			                  String organisationEditable
			                  
			) throws InterruptedException{
		
		
		// test the runmode of current dataset
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			skip=true;
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		
		APP_LOGS.debug("Executing Test Case testUserProfile");
		
		openBrowser();
        //driver.get(CONFIG.getProperty("testSiteUrlName"));
 		driver.get(CONFIG.getProperty("dashboardSiteUrlName"));
 		getObject("dashboardUserName").sendKeys(username);
 		getObject("dashboardPassword").sendKeys(password);
 		getObject("dashboardLoginButton").click();
 		
 		Thread.sleep(5000L);
 		String actualText =getObject("dashboardText").getText();
 		
 		if(!compareText("Dashboard", actualText)){
 			fail=true;
 			APP_LOGS.debug("Unable to login successfully into dashboard ");
 			return;
 			
 		}
 		
       
        getObject("dashboardMyAccount").click();
        getObject("dashboardMyAccountProfile").click();
        if((emailVerified.equalsIgnoreCase("Yes") && custId.equalsIgnoreCase("No")) || (emailVerified.equalsIgnoreCase("Yes") && custId.equalsIgnoreCase("Yes")) || (emailVerified.equalsIgnoreCase("No") && custId.equalsIgnoreCase("No") ) || (emailVerified.equalsIgnoreCase("No") && custId.equalsIgnoreCase("Yes")) ){
        String [] profileHedaer = getObject("userProfileBox").getText().split("Email");
        if(!compareText("Profile Info", profileHedaer[0].trim())){
        	fail=true;
        	APP_LOGS.debug("Expected Text is not Present " +profileHedaer );
        }
        
        String Profiledisabled = getObject("userProfileBox").getAttribute("disabled");
        if((emailVerified.equalsIgnoreCase("Yes") && custId.equalsIgnoreCase("No")) ){
                if(!compareText("true", Profiledisabled)){
        	       fail=true;
        	        APP_LOGS.debug("Expected fields are not disabled " +Profiledisabled );
                   }
        }
        WebElement userprofilebox= getObject("userProfileBox");
        List<WebElement> userprofilefiedls =userprofilebox.findElements(By.xpath(OR.getProperty("profileFields")));
        System.out.println("***************"+userprofilefiedls.size());
        if(!compareNumbers(7,userprofilefiedls.size())){
        	fail=true;
        	APP_LOGS.debug("Expected Number of fields are  not Present " +userprofilefiedls.size() );
        }
        for(int i=0;i<=userprofilefiedls.size()-1;i++){
        	System.out.println(userprofilefiedls.get(i).getText());
        }
        
     }   
        if((emailVerified.equalsIgnoreCase("Yes") && custId.equalsIgnoreCase("No")) || (emailVerified.equalsIgnoreCase("No") && custId.equalsIgnoreCase("No"))){
        
        String []companyHedaer =getObject("companyBox").getText().split("Company Name");
        if(!compareText("Company Details", companyHedaer[0].trim())){
        	fail=true;
        	APP_LOGS.debug("Expected Text is not Present " +companyHedaer );
        }
		
        String dashboardProfile= getObject("companyBox").getAttribute("disabled");
        if((emailVerified.equalsIgnoreCase("Yes") && custId.equalsIgnoreCase("No")) ){
                if(!compareText("true", dashboardProfile)){
        	        fail=true;
        	        APP_LOGS.debug("Expected fields are not disabled  " +dashboardProfile );
            }

       }
        if((emailVerified.equalsIgnoreCase("Yes") && custId.equalsIgnoreCase("No")) || (emailVerified.equalsIgnoreCase("No") && custId.equalsIgnoreCase("No")) ){
        WebElement Companybox= getObject("companyBox");
        List<WebElement> companyfiedls =Companybox.findElements(By.xpath(OR.getProperty("companyFields")));
        System.out.println("***************"+companyfiedls.size());
        if(!compareNumbers(9,companyfiedls.size())){
        	fail=true;
        	APP_LOGS.debug("Expected Number of fields are  not Present " +companyfiedls.size() );
        }
        
        for(int i=0;i<=companyfiedls.size()-1;i++){
        	System.out.println(companyfiedls.get(i).getText());
        }
    } 
        if((emailVerified.equalsIgnoreCase("Yes") && custId.equalsIgnoreCase("No")) || (emailVerified.equalsIgnoreCase("Yes") && custId.equalsIgnoreCase("Yes")) ){
        String savebuttonDisabled= getObject("profileSavebutton").getAttribute("disabled");
             if(!compareText("true", savebuttonDisabled)){
        	fail=true;
        	APP_LOGS.debug("Save button is not disabled");
            }
        
        getObject("profileSavebutton").click();

        if(!compareText("true", savebuttonDisabled)){
        	fail=true;
        	APP_LOGS.debug("Save button is not disabled after clicking on Save button for registered verified user");
        }
     }
        
        if((emailVerified.equalsIgnoreCase("No") && custId.equalsIgnoreCase("No")) || (emailVerified.equalsIgnoreCase("No") && custId.equalsIgnoreCase("Yes"))){
        	String savebuttonDisabled= getObject("profileSavebutton").getAttribute("disabled");
        	getObject("profileSavebutton").click();

            if(!compareText("true", savebuttonDisabled)){
            	fail=true;
            	APP_LOGS.debug("Save button is not disabled after clicking on Save button for registered verified user");
            }
            getObject("dashboardProfileCity").clear();
            getObject("dashboardProfileCity").sendKeys("Pune");
            getObject("profileSavebutton").click();
        	String profileSuccessfullyText=getObject("userProfileSaveText").getText();
        	if(!compareText("Your profile has been updated successfully.", profileSuccessfullyText)){
        		APP_LOGS.debug("Unable to save the user Profile by registered user before email verification");
        	}
        }
   }
        
        if((emailVerified.equalsIgnoreCase("Yes") && custId.equalsIgnoreCase("Yes") && organisationEditable.equalsIgnoreCase("Yes"))){
        getObject("dashbordOrganisation").click();
        getObject("organisationEditTab").click();
        Thread.sleep(3000L);
        WebElement orgBox= getObject("organisationBox");
        List<WebElement> organisationfiedls = orgBox.findElements(By.xpath(OR.getProperty("organisationFeilds")));
        for(int i=0;i<=organisationfiedls.size()-1;i++){
        	System.out.println(organisationfiedls.get(i).getText());
        }
        
        getObject("organisationSaveButton").click();

       String OrganisationEditSuccessfully= getObject("organisationEditSaveText").getText().trim();
        
        if(!compareText("Your organisation details have been updated successfully.", OrganisationEditSuccessfully)){
        	fail=true;
        	APP_LOGS.debug("Approver is not able to edit the organisation" +OrganisationEditSuccessfully );
        }

    }
        
       getObject("signOutDropDown").click();
 	   getObject("signoutButton").click();
 	   Thread.sleep(3000L);
 	   if(!compareText("Login to your dashboard", getObject("dashboardloginPageText").getText())){
 		 fail=true;
 		 APP_LOGS.debug("Unable to Logout from dashboard successfully");
 		 
 	    }
 	   
        
	}
        

	
	 @AfterMethod
		public void reportDataSetResult(){
			if(skip)
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "SKIP");
			else if(fail){
				isTestPass=false;
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "FAIL");
			}
			else
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "PASS");
			
			skip=false;
			fail=false;
			

		}
		
		@AfterTest
		public void reportTestResult(){
			if(isTestPass)
				TestUtil.reportDataSetResult(suite_dashboard_xls, "Test Cases", TestUtil.getRowNum(suite_dashboard_xls,this.getClass().getSimpleName()), "PASS");
			else
				TestUtil.reportDataSetResult(suite_dashboard_xls, "Test Cases", TestUtil.getRowNum(suite_dashboard_xls,this.getClass().getSimpleName()), "FAIL");
		
		}
		
		
		@DataProvider
		public Object[][] getTestData(){
			return TestUtil.getData(suite_dashboard_xls, this.getClass().getSimpleName());
			
		}
}
