package com.canopy.suite.dashboard;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyBuyMoreProduct extends TestSuiteBase {
	

	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_dashboard_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_dashboard_xls, this.getClass().getSimpleName());
	}
	
	
	
	@Test(dataProvider="getTestData")
	public void testBuyMoreProduct(
			) throws InterruptedException, IOException{
		
		
		// test the runmode of current dataset
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			skip=true;
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		
		APP_LOGS.debug("Executing Test Case testBuyMoreProduct");
		
		openBrowser();
 		driver.get(CONFIG.getProperty("dashboardSiteUrlName"));
 		getObject("dashboardUserName").sendKeys(CONFIG.getProperty("ApproverUserName"));
 		getObject("dashboardPassword").sendKeys(CONFIG.getProperty("ApproverPassword"));
 		getObject("dashboardLoginButton").click();
 		
 		Thread.sleep(5000L);
 		String actualText =getObject("dashboardText").getText();
 		
 		if(!compareText("Dashboard", actualText)){
 			fail=true;
 			APP_LOGS.debug("Unable to login successfully into dashboard ");
 			return;
 			
 		}
 		
 		getObject("buyMoreSolution").click();
 		Set<String> windIds= driver.getWindowHandles();
	    Iterator<String> it= windIds.iterator();
	    String homepage= it.next();
	    String firstPopup=it.next();
        driver.switchTo().window(firstPopup);
		
		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\Authentication.exe");
		
	    String ActualLogoutText = getObject("LogoutButtonText").getText();
	    if(!compareText("LOGOUT",ActualLogoutText)){
			fail=true;
  			APP_LOGS.debug("Unable to Open Market Place with SSO Feature");
		}
		
	    getObject("LogoutButtonText").click();
	    
	    String ActualLoginText = getObject("LogoutButtonText").getText();
	    if(!compareText("Login",ActualLoginText)){
			fail=true;
  			APP_LOGS.debug("Unable to Logout from Market Place");
		}
		        
}
	
	 @AfterMethod
		public void reportDataSetResult(){
			if(skip)
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "SKIP");
			else if(fail){
				isTestPass=false;
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "FAIL");
			}
			else
				TestUtil.reportDataSetResult(suite_dashboard_xls, this.getClass().getSimpleName(), count+2, "PASS");
			
			skip=false;
			fail=false;
			

		}
		
		@AfterTest
		public void reportTestResult(){
			if(isTestPass)
				TestUtil.reportDataSetResult(suite_dashboard_xls, "Test Cases", TestUtil.getRowNum(suite_dashboard_xls,this.getClass().getSimpleName()), "PASS");
			else
				TestUtil.reportDataSetResult(suite_dashboard_xls, "Test Cases", TestUtil.getRowNum(suite_dashboard_xls,this.getClass().getSimpleName()), "FAIL");
		
		}
		
		
		@DataProvider
		public Object[][] getTestData(){
			return TestUtil.getData(suite_dashboard_xls, this.getClass().getSimpleName());
			
		}
}
