package com.canopy.suite.cart;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyCheckout extends TestSuiteBase{

	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_cart_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_cart_xls, this.getClass().getSimpleName());
	}
	
	
	@Test(dataProvider="getTestData")
	public void testCheckout(
			                 String userName ,
			                 String passWord ,
			                 String configurations_emailids,
			                 String Product  ,
			                 String contract_duration ,
			                 String configurations_capacity ,
			                 String level_of_support ,
			                 String addedProductDesc
			) throws InterruptedException, IOException{
		
		// test the runmode of current dataset
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			skip=true;
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		
          APP_LOGS.debug("Executing Test Case testCheckout");
		
          openBrowser();
          if(!isURLOpened){
          	openURL();
          }
         
  		driver.get(CONFIG.getProperty("testSiteUrlName"));
  		
  	    
  		
          getObject("login").click();
          Thread.sleep(10000L);

          getObject("userName").sendKeys(userName);
          getObject("password").sendKeys(passWord);
          
         if(!checkElementPresence("loginSumbit")){
      	   fail=true;
      	   APP_LOGS.debug("Login Submit button is not avilable");
      	   return;
         }
          
  		
          getObject("loginSumbit").click();
          	
          if(!checkElementPresence("myDashboard")){
          		fail=true;
          		APP_LOGS.debug("My Dashboard Links is not avilable");
          		return ;
          	}
          
          getObject("search_input").sendKeys(Product);
          getObject("searchButton").click();
          WebElement result_grid= driver.findElement(By.className("holder"));
          result_grid.findElement(By.tagName("a")).click();
          Thread.sleep(10000L);
		  getObject("buyProductButton").click();
		  Thread.sleep(5000L);
          getObject("mycart").click();
          getObject("level_of_support").sendKeys(level_of_support);
          getObject("configurations_capacity").sendKeys(configurations_capacity);
          getObject("contract_duration").sendKeys(contract_duration);
          getObject("configurations_emailids").sendKeys(configurations_emailids);
        
        Thread.sleep(5000L);
        String actualTotalAmount = getObject("topTotalAmmount").getText();
        String [] amount =actualTotalAmount.split("�");
        
        Thread.sleep(20000L);
        getObject("checkoutProduct").click();
        Thread.sleep(5000L);
        
        String reviewOrder = getObject("reviewOrder").getText();
        
        if(!compareText("Review your order", reviewOrder)){
        	fail=true;
        	APP_LOGS.debug("Review Order Page is not Opened");
        	getObject("logout").click();
        	return;
        }
        
        if(!checkElementPresence("returnToCart")){
        	fail=true;
        	APP_LOGS.debug("Return to Cart button is not Present");
        	getObject("logout").click();
        	return;
        }
        
        if(!checkElementPresence("validate")){
        	fail=true;
        	APP_LOGS.debug("Validate button is not Present");
        	getObject("logout").click();
        	return;
        }

        
         String reviewItem = getObject("reviewItemDesc").getText();

         if(!compareText(addedProductDesc, reviewItem)){
        	 fail=true;
         	APP_LOGS.debug("Added Product Description not matched");
         	getObject("logout").click();
         	return;
         }
        

        String totalReview=getObject("reviewTotal").getText();
        String [] reviewamount =totalReview.split("�");
        System.out.println(reviewamount[0]);
        System.out.println(reviewamount[1]);
    /*   
        if(!compareText(reviewamount[1], amount[0])){
          	fail=true;
          	APP_LOGS.debug("Amount doesnot match on Cart and Review order page");
          	
          }
		*/
        getObject("returnToCart").click();
        Thread.sleep(10000L);
        
        driver.findElement(By.linkText("Remove from Cart")).click();
		
		Alert al = driver.switchTo().alert();
		al.accept();
		Thread.sleep(10000L);
		
		String actualRemovedText =getObject("removedProductCart").getText();
		
		 if(!compareText("An item has been removed from cart", actualRemovedText)){
		    	fail=true;
		    	APP_LOGS.debug("Product is not removed from cart");
		    	getObject("logout").click();
		    	return;
		    }

		 getObject("logout").click();
	}
	
		
	 @AfterMethod
		public void reportDataSetResult(){
			if(skip)
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "SKIP");
			else if(fail){
				isTestPass=false;
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "FAIL");
			}
			else
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "PASS");
			
			skip=false;
			fail=false;
			

		}
		
		@AfterTest
		public void reportTestResult(){
			if(isTestPass)
				TestUtil.reportDataSetResult(suite_cart_xls, "Test Cases", TestUtil.getRowNum(suite_cart_xls,this.getClass().getSimpleName()), "PASS");
			else
				TestUtil.reportDataSetResult(suite_cart_xls, "Test Cases", TestUtil.getRowNum(suite_cart_xls,this.getClass().getSimpleName()), "FAIL");
		
		}
		
		
		@DataProvider
		public Object[][] getTestData(){
			return TestUtil.getData(suite_cart_xls, this.getClass().getSimpleName());
			
		}
	    
}
