package com.canopy.suite.cart;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyTotatAmount extends TestSuiteBase{

	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	static double finalTotal;
	static double topTotal;
	static double bottomTotal;
	static double reviewTotal;
	static int countItem ; 
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_cart_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_cart_xls, this.getClass().getSimpleName());
	}
	

	@Test(dataProvider="getTestData")
	public void testAddProduct(
			String countItem,
			String userName ,
            String passWord ,
            String configurations_emailids,
            String Product  ,
            String contract_duration ,
            String configurations_capacity ,
            String level_of_support ,
            String expectedTotal
			) throws InterruptedException, IOException{
		
		// test the runmode of current dataset
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			skip=true;
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		
          APP_LOGS.debug("Executing Test Case VerifyMutipleProductCart");
		
          openBrowser();
          if(!isURLOpened){
          	openURL();
          }
         
  		driver.get(CONFIG.getProperty("testSiteUrlName"));
  		
  	    
  		
          getObject("login").click();
          Thread.sleep(10000L);

          getObject("userName").sendKeys(userName);
          getObject("password").sendKeys(passWord);
          
         if(!checkElementPresence("loginSumbit")){
      	   fail=true;
      	   APP_LOGS.debug("Login Submit button is not avilable");
      	   return;
         }
          
  		
          getObject("loginSumbit").click();
          	
          if(!checkElementPresence("myDashboard")){
          		fail=true;
          		APP_LOGS.debug("My Dashboard Links is not avilable");
          		return ;
          	}
          
          getObject("search_input").sendKeys(Product);
          getObject("searchButton").click();
          WebElement result_grid= driver.findElement(By.className("holder"));
          result_grid.findElement(By.tagName("a")).click();
          Thread.sleep(10000L);
		  getObject("buyProductButton").click();
		  Thread.sleep(5000L);
          getObject("mycart").click();
          Thread.sleep(5000L);
          getObject("logout").click();
          Thread.sleep(5000L);
          
                }
	


@Test(dependsOnMethods={"testAddProduct"},priority = 1)

	public void testDoLogin() throws InterruptedException{
		
		 openBrowser();
         if(!isURLOpened){
         	openURL();
         }
        
 		 driver.get(CONFIG.getProperty("testSiteUrlName"));
         getObject("login").click();
         Thread.sleep(10000L);
         getObject("userName").sendKeys("check1@yopmail.com");
         getObject("password").sendKeys("Narendra@123$");
         
        if(!checkElementPresence("loginSumbit")){
     	   fail=true;
     	   APP_LOGS.debug("Login Submit button is not avilable");
     	   return;
        }
         getObject("loginSumbit").click();
         Thread.sleep(10000L);
         getObject("mycart").click();
         Thread.sleep(10000L);
         
	}
	
	
	@Test(dependsOnMethods={"testDoLogin"},dataProvider="getTestData" ,priority = 1)
	public void configureMultipleProducts(
			String countItem,
			String userName ,
            String passWord ,
            String configurations_emailids,
            String Product  ,
            String contract_duration ,
            String configurations_capacity ,
            String level_of_support ,
            String expectedTotal
            ) throws InterruptedException{
		
		     WebElement box  = driver.findElement(By.xpath("//*[@id='CartForm']/div[2]"));
		     List<WebElement> items =box.findElements(By.xpath("//*[@id='CartForm']/div[2]/div/div/div[2]"));
          	driver.findElement(By.xpath("html/body/div[1]/div[1]/section[2]/form/div[2]/div["+countItem+"]/div/div[2]/div[1]/table/tbody/tr[3]/td[2]/select")).sendKeys(configurations_capacity);
        	driver.findElement(By.xpath("html/body/div[1]/div[1]/section[2]/form/div[2]/div["+countItem+"]/div/div[2]/div[1]/table/tbody/tr[2]/td[2]/select")).sendKeys(level_of_support);
        	
        	driver.findElement(By.xpath("html/body/div[1]/div[1]/section[2]/form/div[2]/div["+countItem+"]/div/div[2]/div[1]/table/tbody/tr[4]/td[2]/select")).sendKeys(contract_duration);
        	driver.findElement(By.xpath("html/body/div[1]/div[1]/section[2]/form/div[2]/div["+countItem+"]/div/div[2]/div[1]/table/tbody/tr[5]/td[2]/input")).sendKeys(configurations_emailids);
        	driver.findElement(By.xpath("html/body/div[1]/div[1]/section[2]/form/div[2]/div["+countItem+"]/div/div[2]/div[1]/table/tbody/tr[2]/td[2]/select")).sendKeys(level_of_support);
        

	}
		
	@Test(dependsOnMethods={"configureMultipleProducts"},dataProvider="getTestData",priority = 1)
	public void calculateToatl(
			String countItem,
			String userName ,
            String passWord ,
            String configurations_emailids,
            String Product  ,
            String contract_duration ,
            String configurations_capacity ,
            String level_of_support ,
            String expectedTotal
			
			) throws InterruptedException{
	 
		     Thread.sleep(30000L);
		     double expectedIndividalPrice=  Double.parseDouble(expectedTotal);
		     
		     String IndividualProductPrice =driver.findElement(By.xpath("//*[@id='CartForm']/div[2]/div["+countItem+"]/div/div[1]/div[3]/div[1]/span")).getText();
	         String [] priceItem= IndividualProductPrice.split("�");
	         double productPrice =Double.parseDouble(priceItem[1].replace(",",""));
	         finalTotal = finalTotal+productPrice;
	        
	        if(!compareNumbersDouble(expectedIndividalPrice, productPrice)){
		    	 
		    	 fail=true;
		    	 APP_LOGS.debug("Individual Prices are not matching "+expectedIndividalPrice);

		     }
		     
		     
		     
		     
	}
	
	@Test(dependsOnMethods={"calculateToatl"},priority = 1)
	public void compareTotal() throws InterruptedException{
		
		    String dispalyTopProduct = driver.findElement(By.xpath("//*[@id='total-top']/span")).getText();
	        String [] displayTotalTopPrice= dispalyTopProduct.split("�");
	        topTotal= Double.parseDouble(displayTotalTopPrice[1].replace(",",""));
	        String displayBottomProduct = driver.findElement(By.xpath("//*[@id='total-top']/span")).getText();
	        String [] displayToatlPriceBottom= displayBottomProduct.split("�");
	        bottomTotal= Double.parseDouble(displayToatlPriceBottom[1].replace(",",""));
		   
		
		if(compareNumbersDouble(finalTotal, topTotal)){
			 
	    	 fail=true;
	    	 APP_LOGS.debug("Final Prices are not matching with topTotal amount "+topTotal);
			
		}
		
		if(compareNumbersDouble(finalTotal, bottomTotal)){
			 
	    	 fail=true;
	    	 APP_LOGS.debug("Final Prices are not matching with bottomTotal amount "+bottomTotal);
			
		}
		
		 getObject("checkoutProduct").click();
	     Thread.sleep(30000L);    	
    	//String totalReview = driver.findElement(By.xpath("//*[@id='main']/section[1]/div/table/tfoot/tr/td/strong")).getText();
    	String totalReview = getObject("reviewTotal").getText();
    	String [] totalAmountReview= totalReview.split("\\)");
    	System.out.println(totalAmountReview[1].split("�")[1]);
    	reviewTotal= Double.parseDouble(totalAmountReview[1].split("�")[1].replace(",",""));
		
    	if(compareNumbersDouble(topTotal, reviewTotal)){
			 
	    	 fail=true;
	    	 APP_LOGS.debug("Total cost on Mycart page is not same as on review order page "+topTotal);
			
		}
            getObject("returnToCart").click();
            Thread.sleep(10000L);
            WebElement CartItems =getObject("cartProductsBox") ;
            int totalsize =CartItems.findElements(By.xpath("//*[contains(@id ,'lnk')]")).size();
            int count=totalsize ;
            for(int i=1;i<=totalsize;i++){
            	WebElement itemBox =driver.findElement(By.xpath("//*[@id='CartForm']/div[2]/div["+count+"]/div/div[1]"));
        		itemBox.findElement(By.xpath("//*[contains(@id ,'lnk')]")).click();
        		Alert al = driver.switchTo().alert();
        		al.accept();
        		Thread.sleep(10000L);
        		count-- ;
        		 String actualRemovedText =getObject("removedProductCart").getText();
        		 if(!compareText("An item has been removed from cart", actualRemovedText)){
        		    	fail=true;
        		    	APP_LOGS.debug("Product is not removed from cart");
        		    	getObject("logout").click();
        		    	return;
        		    }
            }
            
            getObject("logout").click();
            Thread.sleep(10000L);
	}
	
	
	 @AfterMethod
		public void reportDataSetResult(){
			if(skip)
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "SKIP");
			else if(fail){
				isTestPass=false;
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "FAIL");
			}
			else
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "PASS");
			
			skip=false;
			fail=false;
			

		}
		
		@AfterTest
		public void reportTestResult(){
			if(isTestPass)
				TestUtil.reportDataSetResult(suite_cart_xls, "Test Cases", TestUtil.getRowNum(suite_cart_xls,this.getClass().getSimpleName()), "PASS");
			else
				TestUtil.reportDataSetResult(suite_cart_xls, "Test Cases", TestUtil.getRowNum(suite_cart_xls,this.getClass().getSimpleName()), "FAIL");
		
		}
		
		
		@DataProvider
		public Object[][] getTestData(){
			return TestUtil.getData(suite_cart_xls, this.getClass().getSimpleName());
			
		}
	    
}
