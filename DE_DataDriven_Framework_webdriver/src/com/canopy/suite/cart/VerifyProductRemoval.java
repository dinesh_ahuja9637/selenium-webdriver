package com.canopy.suite.cart;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyProductRemoval extends TestSuiteBase {
	

	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_cart_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_cart_xls, this.getClass().getSimpleName());
	}
	
	
	
	@Test(dataProvider="getTestData")
	public void testProductRemoval(
			                       String userName,
			                       String passWord,
			                       String searchProduct,
			                       String Product,
			                       String type
			) throws InterruptedException{
		
		
		// test the runmode of current dataset
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			skip=true;
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		
		APP_LOGS.debug("Executing Test Case testProductRemoval");
		
		
		openBrowser();
        if(!isURLOpened){
        	openURL();
        }
       
		driver.get(CONFIG.getProperty("testSiteUrlName"));
        getObject("login").click();
        Thread.sleep(20000L);
       /* 
        if(! compareText("Sign in",driver.findElement(By.xpath(OR.getProperty("signText"))).getText())){
      	   fail=true;
      	   APP_LOGS.debug("Login Page doesnot Open");
      	   return;
         }
        */
        getObject("userName").sendKeys(userName);
        getObject("password").sendKeys(passWord);
        
       if(!checkElementPresence("loginSumbit")){
    	   fail=true;
    	   APP_LOGS.debug("Login Submit button is not avilable");
    	   return;
       }
        
		
        getObject("loginSumbit").click();
        	
        if(!checkElementPresence("myDashboard")){
        		fail=true;
        		APP_LOGS.debug("My Dashboard Links is not avilable");
        		return ;
        	}
        
	    
	   getObject("homelink").click();	
		
		
	     getObject("search_input").sendKeys(searchProduct);
	     getObject("searchButton").click();
   		if(type.equals("try")){
   			WebElement result_grid= driver.findElement(By.className("holder"));
   			result_grid.findElement(By.tagName("a")).click();
   	       // result_grid.findElement(By.linkText(Product)).click();
   	        Thread.sleep(10000L);
			
			String TryButton=getObject("tryButton").getAttribute("value");
		    if(!compareText("Try it", TryButton)){
		    	fail=true;
		    	APP_LOGS.debug("Try button is not avilable");
		    	getObject("logout").click();
		    	return;
		    }
		    
		    getObject("tryButton").click();
		    
		    Thread.sleep(1000L);
		    
		    String actualAddedText =getObject("addToCartMessage").getText();
		    if(!compareText("Added!", actualAddedText)){
		    	fail=true;
		    	APP_LOGS.debug("Not able to add product to cart");
		    	getObject("logout").click();
		    	return;
		    }
		    
		    getObject("logout").click();
		    
		} else if(type.equals("buy")){
			
			WebElement result_grid= driver.findElement(By.className("holder"));
			result_grid.findElement(By.tagName("a")).click();
   	        //result_grid.findElement(By.linkText(Product)).click();
   	        Thread.sleep(10000L);
			
			String BuyButton=getObject("buyProductButton").getAttribute("value");
		    if(!compareText("Buy it", BuyButton)){
		    	fail=true;
		    	APP_LOGS.debug("Buy button is not avilable");
		    	getObject("logout").click();
		    	return;
		    }
		    
		    getObject("buyProductButton").click();
		    
		    Thread.sleep(1000L);
		    
		    String actualAddedText =getObject("addToCartMessage").getText();
		    if(!compareText("Added!", actualAddedText)){
		    	fail=true;
		    	APP_LOGS.debug("Not able to add product to cart");
		    	getObject("logout").click();
		    	return;
		    }
		    
		    	
		    getObject("logout").click();
		} 



	}
	
	@Test(dependsOnMethods={"testProductRemoval"})
	public void testremoveProducts() throws InterruptedException{
		
		 openBrowser();
         if(!isURLOpened){
         	openURL();
         }
        
 	     driver.get(CONFIG.getProperty("testSiteUrlName"));
         getObject("login").click();
         Thread.sleep(10000L);

         getObject("userName").sendKeys("check1@yopmail.com");
         getObject("password").sendKeys("Yopmail@123$");
         
        if(!checkElementPresence("loginSumbit")){
     	   fail=true;
     	   APP_LOGS.debug("Login Submit button is not avilable");
     	   return;
        }
         
 		
         getObject("loginSumbit").click();
        		
         getObject("mycart").click();
         
         WebElement CartItems =getObject("cartProductsBox") ;
         int totalsize =CartItems.findElements(By.xpath("//*[contains(@id ,'lnk')]")).size();
        
         int count=totalsize ;
         for(int i=1;i<=totalsize;i++){
         	
         	
         	WebElement itemBox =driver.findElement(By.xpath("//*[@id='CartForm']/div[2]/div["+count+"]/div/div[1]"));
     		itemBox.findElement(By.xpath("//*[contains(@id ,'lnk')]")).click();
         	
         	
     		Alert al = driver.switchTo().alert();
     		al.accept();
     		Thread.sleep(10000L);
     		count-- ;

     		 String actualRemovedText =getObject("removedProductCart").getText();
     		 if(!compareText("An item has been removed from cart", actualRemovedText)){
     		    	fail=true;
     		    	APP_LOGS.debug("Product is not removed from cart");
     		    	getObject("logout").click();
     		    	return;
     		    }
     		 
        }
     		    

         getObject("logout").click();
		
	}

	
	
	
	 @AfterMethod
		public void reportDataSetResult(){
			if(skip)
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "SKIP");
			else if(fail){
				isTestPass=false;
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "FAIL");
			}
			else
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "PASS");
			
			skip=false;
			fail=false;
			

		}
		
		@AfterTest
		public void reportTestResult(){
			if(isTestPass)
				TestUtil.reportDataSetResult(suite_cart_xls, "Test Cases", TestUtil.getRowNum(suite_cart_xls,this.getClass().getSimpleName()), "PASS");
			else
				TestUtil.reportDataSetResult(suite_cart_xls, "Test Cases", TestUtil.getRowNum(suite_cart_xls,this.getClass().getSimpleName()), "FAIL");
		
		}
		
		
		@DataProvider
		public Object[][] getTestData(){
			return TestUtil.getData(suite_cart_xls, this.getClass().getSimpleName());
			
		}
}
