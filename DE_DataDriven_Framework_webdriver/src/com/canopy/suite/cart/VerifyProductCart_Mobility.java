package com.canopy.suite.cart;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyProductCart_Mobility extends TestSuiteBase{

	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	static WebElement CartItems ;
	static WebElement itemBox ;
	static int  rows;
	static int cols ;
	static int countItem ; 
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_cart_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_cart_xls, this.getClass().getSimpleName());
	}
	
	
	@Test(dataProvider="getTestData")
	public void testMutipleProductCart(
			                 String countItem,
			                 String userName ,
			                 String passWord ,
			                 String configurations_emailids,
			                 String Product  ,
			                 String contract_duration ,
			                 String configurations_capacity ,
			                 String level_of_support ,
			                 String addedProducts ,
			                 String customName,
			                 String total
			) throws InterruptedException, IOException{
		
		// test the runmode of current dataset
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			skip=true;
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		
          APP_LOGS.debug("Executing Test Case VerifyMutipleProductCart");
		
          openBrowser();
          if(!isURLOpened){
          	openURL();
          }
         
  		driver.get(CONFIG.getProperty("testSiteUrlName"));
  		
  	    
  		
          getObject("login").click();
          Thread.sleep(10000L);

          getObject("userName").sendKeys(userName);
          getObject("password").sendKeys(passWord);
          
         if(!checkElementPresence("loginSumbit")){
      	   fail=true;
      	   APP_LOGS.debug("Login Submit button is not avilable");
      	   return;
         }
          
  		
          getObject("loginSumbit").click();
          	
          if(!checkElementPresence("myDashboard")){
          		fail=true;
          		APP_LOGS.debug("My Dashboard Links is not avilable");
          		return ;
          	}
          
          getObject("search_input").sendKeys(Product);
          getObject("searchButton").click();
          WebElement result_grid= driver.findElement(By.className("holder"));
          result_grid.findElement(By.tagName("a")).click();
          Thread.sleep(10000L);
		  getObject("buyProductButton").click();
		  Thread.sleep(5000L);
          getObject("mycart").click();
          Thread.sleep(5000L);
          getObject("logout").click();
          Thread.sleep(5000L);
          
                }
	


@Test(dependsOnMethods={"testMutipleProductCart"},priority = 1)

	public void testDoLogin() throws InterruptedException{
		
		 openBrowser();
         if(!isURLOpened){
         	openURL();
         }
        
 		 driver.get(CONFIG.getProperty("testSiteUrlName"));
         getObject("login").click();
         Thread.sleep(10000L);
         getObject("userName").sendKeys("check1@yopmail.com");
         getObject("password").sendKeys("Narendra@123$");
         
        if(!checkElementPresence("loginSumbit")){
     	   fail=true;
     	   APP_LOGS.debug("Login Submit button is not avilable");
     	   return;
        }
         getObject("loginSumbit").click();
         Thread.sleep(10000L);
         getObject("mycart").click();
         Thread.sleep(10000L);
         
	}
	
	
	@Test(dependsOnMethods={"testDoLogin"},dataProvider="getTestData" ,priority = 1)
	public void configureMultipleProducts(
			String countItem,
			String userName ,
            String passWord ,
            String configurations_emailids,
            String Product  ,
            String contract_duration ,
            String configurations_capacity ,
            String level_of_support ,
            String addedProducts ,
            String customName,
            String total) throws InterruptedException{
		
		WebElement box  = driver.findElement(By.xpath("//*[@id='CartForm']/div[2]"));
		List<WebElement> items =box.findElements(By.xpath("//*[@id='CartForm']/div[2]/div/div/div[2]"));
        
           
        	driver.findElement(By.xpath("html/body/div[1]/div[1]/section[2]/form/div[2]/div["+countItem+"]/div/div[2]/div[1]/table/tbody/tr[3]/td[2]/select")).sendKeys(configurations_capacity);
        	driver.findElement(By.xpath("html/body/div[1]/div[1]/section[2]/form/div[2]/div["+countItem+"]/div/div[2]/div[1]/table/tbody/tr[2]/td[2]/select")).sendKeys(level_of_support);
        	driver.findElement(By.xpath("html/body/div[1]/div[1]/section[2]/form/div[2]/div["+countItem+"]/div/div[2]/div[1]/table/tbody/tr[4]/td[2]/select")).sendKeys(contract_duration);
        	driver.findElement(By.xpath("html/body/div[1]/div[1]/section[2]/form/div[2]/div["+countItem+"]/div/div[2]/div[1]/table/tbody/tr[5]/td[2]/input")).sendKeys(configurations_emailids);
        	
        

	}
		
	@Test(dependsOnMethods={"configureMultipleProducts"},dataProvider="getTestData",priority = 1)
	public void reviewMultipleProducts(
			String countItem,
			String userName ,
            String passWord ,
            String configurations_emailids,
            String Product  ,
            String contract_duration ,
            String configurations_capacity ,
            String level_of_support ,
            String addedProducts ,
            String customName,
            String total
			
			) throws InterruptedException{
	 
		if(countItem.equals("1")){
        getObject("checkoutProduct").click();
        Thread.sleep(10000L);
       
         rows = driver.findElements(By.xpath("//*[@id='main']/section[1]/div/table/tbody/tr/td[1]")).size();
         cols = driver.findElements(By.xpath("//*[@id='main']/section[1]/div/table/tbody/tr[1]/td")).size();
          if(!compareNumbers( Integer.parseInt(addedProducts), rows)){
        	  fail=true;
       	   APP_LOGS.debug("Number of Products added are not same as in cart");
       	   return;
          }
         
          if(!compareNumbers( Integer.parseInt(addedProducts), cols)){
        	  fail=true;
       	      APP_LOGS.debug("Number of Products addes are not same as in cart");
       	      return;
          }
		
        
         
		/*
         for(int i=1;i<=rows;i++){
        	 
        	 for(int j=1;j<=cols;j++){
        		 
        		 if(j==1){
        		 String actualProductdesc =driver.findElement(By.xpath("//*[@id='main']/section[1]/div/table/tbody/tr["+i+"]/td["+j+"]")).getText();
        		 
        		 if(!compareText(addedProductDesc, actualProductdesc)){
        			 fail=true;
        			 APP_LOGS.debug("Added Product description are not matching");
        		 }
        		 
        		 
        		 } else if(j==2){
            		 String actualCustomdesc =driver.findElement(By.xpath("//*[@id='main']/section[1]/div/table/tbody/tr["+i+"]/td["+j+"]")).getText();
 
            		 if(!compareText(customName, actualCustomdesc)){
            			 fail=true;
            			 APP_LOGS.debug("customName are not matching");
            		 }
        		 } else if(j==3){
            		 String actualtotaldesc =driver.findElement(By.xpath("//*[@id='main']/section[1]/div/table/tbody/tr["+i+"]/td["+j+"]")).getText();
 
            		 if(!compareText(total, actualtotaldesc)){
            			 fail=true;
            			 APP_LOGS.debug("total are not matching");
            		 }
            		 
        		 }
        		 
        		 
        	 }
        	 
         }
		*/
		if(countItem.equals("1")){
         getObject("returnToCart").click();
         Thread.sleep(10000L);

		}
         WebElement CartItems =getObject("cartProductsBox") ;
         int totalsize =CartItems.findElements(By.xpath("//*[contains(@id ,'lnk')]")).size();
         int count=totalsize ;
         for(int i=1;i<=totalsize;i++){
         	
         	
         	WebElement itemBox =driver.findElement(By.xpath("//*[@id='CartForm']/div[2]/div["+count+"]/div/div[1]"));
     		itemBox.findElement(By.xpath("//*[contains(@id ,'lnk')]")).click();
         	
         	
     		Alert al = driver.switchTo().alert();
     		al.accept();
     		Thread.sleep(10000L);
     		count-- ;

     		 String actualRemovedText =getObject("removedProductCart").getText();
     		 if(!compareText("An item has been removed from cart", actualRemovedText)){
     		    	fail=true;
     		    	APP_LOGS.debug("Product is not removed from cart");
     		    	getObject("logout").click();
     		    	return;
     		    }
     		 
        }
     		    
}
         getObject("logout").click();
         Thread.sleep(10000L);     
       
	
	}
	
	
	 @AfterMethod
		public void reportDataSetResult(){
			if(skip)
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "SKIP");
			else if(fail){
				isTestPass=false;
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "FAIL");
			}
			else
				TestUtil.reportDataSetResult(suite_cart_xls, this.getClass().getSimpleName(), count+2, "PASS");
			
			skip=false;
			fail=false;
			

		}
		
		@AfterTest
		public void reportTestResult(){
			if(isTestPass)
				TestUtil.reportDataSetResult(suite_cart_xls, "Test Cases", TestUtil.getRowNum(suite_cart_xls,this.getClass().getSimpleName()), "PASS");
			else
				TestUtil.reportDataSetResult(suite_cart_xls, "Test Cases", TestUtil.getRowNum(suite_cart_xls,this.getClass().getSimpleName()), "FAIL");
		
		}
		
		
		@DataProvider
		public Object[][] getTestData(){
			return TestUtil.getData(suite_cart_xls, this.getClass().getSimpleName());
			
		}
	    
}
