package com.canopy.suite.cart;

import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;

import com.canopy.base.TestBase;
import com.canopy.util.TestUtil;

public class TestSuiteBase extends TestBase{

	// check if the suite ex has to be skiped
		@BeforeSuite
		public void checkSuiteSkip() throws Exception{
			initialize();
			APP_LOGS.debug("Checking Runmode of Cart suite");
			if(!TestUtil.isSuiteRunnable(suiteXls, "Cart suite")){
				APP_LOGS.debug("Skipped Cart suite as the runmode was set to NO");
				throw new SkipException("RUnmode of Cart suite set to no. So Skipping all tests in Cart suite");
			}
			
		}
}
