package com.canopy.suite.shop;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyFilterByFunctionality extends TestSuiteBase{
	String resultCount=null;
	String [] totalcount=null;
	WebElement catalogBox=null;
	String acutalProductName =null;
	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_shop_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_shop_xls, this.getClass().getSimpleName());
	}
	
	@Test(dataProvider="getTestData")
	public void testFilterByFunctionality(String productType,
			                              String product_1,
			                              String product_2,
			                              String product_3,
			                              String product_4,
			                              String product_5,
			                              String product_6,
			                              String product_7
			) throws IOException, InterruptedException{
		
		// test the runmode of current dataset
				count++;
				if(!runmodes[count].equalsIgnoreCase("Y")){
					skip=true;
					throw new SkipException("Runmode for test set data set to no "+count);
				}
		
		System.out.println("Executing Test Case testFilterByFunctionality");
		
		openBrowser();
        if(!isURLOpened){
        	openURL();
        }
		driver.get(CONFIG.getProperty("testSiteUrlName"));

        
        
        if(productType.equalsIgnoreCase("Software"))
        {
        	
        	getObject("filterBy").click();
            driver.findElement(By.linkText("Software")).click();
            resultCount =getObject("showing_text").getText();
            
            totalcount=resultCount.split(" ");
            int softwareProductCount= Integer.parseInt(totalcount[3]) ;
            
            if(!compareNumbers(7, softwareProductCount)){
            	fail=true;
            	APP_LOGS.debug("Number of Products are less or more than Expected one");
            }
             catalogBox= driver.findElement(By.xpath(OR.getProperty("filtered_cloud_services_catalog")));

            for(int i=1 ;i<=softwareProductCount;i++){
            	 acutalProductName = catalogBox.findElement(By.xpath(OR.getProperty("filtered_catalog_products_link_part1")+i+OR.getProperty("filtered_catalog_products_link_part2"))).getAttribute("data-name");
            	 if(i==1){
            	 if(!compareText(product_1, acutalProductName))
           		  fail=true;
            	 } else if(i==2){
            		 if(!compareText(product_2, acutalProductName))
                  		  fail=true; 
            	 }else if(i==3){
            		 if(!compareText(product_3, acutalProductName))
                 		  fail=true; 
            	 }else if(i==4){
            		 if(!compareText(product_4, acutalProductName))
                		  fail=true; 
           	    }else if(i==5){
        		 if(!compareText(product_5, acutalProductName))
            		  fail=true; 
       	        }else if(i==6){
    		     if(!compareText(product_6, acutalProductName))
        		  fail=true; 
   	            }else if(i==7){
		         if(!compareText(product_7, acutalProductName))
    		     fail=true; 
	      }
            	 
            	 
			}
            getObject("searchButton").click();
            
        }
	
          
            	 
            
            else if(productType.equalsIgnoreCase("Infrastructure")) {
            	 getObject("filterBy").click();
                 driver.findElement(By.linkText("Infrastructure")).click();
                 resultCount =getObject("showing_text").getText();
                 totalcount=resultCount.split(" ");
                 int infrastructureProductCount= Integer.parseInt(totalcount[3]) ;
                 
                 if(!compareNumbers(6, infrastructureProductCount)){
                 	fail=true;
                 	APP_LOGS.debug("Number of Products are less or more than Expected one");
                 }
                  catalogBox= driver.findElement(By.xpath(OR.getProperty("filtered_cloud_services_catalog")));

                 for(int j=1 ;j<=infrastructureProductCount;j++){
                 	 acutalProductName = catalogBox.findElement(By.xpath(OR.getProperty("filtered_catalog_products_link_part1")+j+OR.getProperty("filtered_catalog_products_link_part2"))).getAttribute("data-name");
        
                 	 if(j==1){
                    	 if(!compareText(product_1, acutalProductName))
                   		  fail=true;
                    	 } else if(j==2){
                    		 if(!compareText(product_2, acutalProductName))
                          		  fail=true; 
                    	 }else if(j==3){
                    		 if(!compareText(product_3, acutalProductName))
                         		  fail=true; 
                    	 }else if(j==4){
                    		 if(!compareText(product_4, acutalProductName))
                        		  fail=true; 
                   	    }else if(j==5){
                		 if(!compareText(product_5, acutalProductName))
                    		  fail=true; 
               	        }else if(j==6){
            		     if(!compareText(product_6, acutalProductName))
                		  fail=true; 
           	            }
                    	 
                
                 }
                 
                 getObject("searchButton").click();

            }
            
            else if(productType.equalsIgnoreCase("Platform")){
            	 getObject("filterBy").click();
                 driver.findElement(By.linkText("Platform")).click();
                 resultCount =getObject("showing_text").getText();
                 totalcount=resultCount.split(" ");
                 int platformProductCount= Integer.parseInt(totalcount[3]) ;
                 
                 if(!compareNumbers(2, platformProductCount)){
                 	fail=true;
                 	APP_LOGS.debug("Number of Products are less or more than Expected one");
                 }
                 
                  catalogBox= driver.findElement(By.xpath(OR.getProperty("filtered_cloud_services_catalog")));

                 for(int k=1 ;k<=platformProductCount;k++){
                 	 acutalProductName = catalogBox.findElement(By.xpath(OR.getProperty("filtered_catalog_products_link_part1")+k+OR.getProperty("filtered_catalog_products_link_part2"))).getAttribute("data-name");

                 	if(k==1){
                   	 if(!compareText(product_1, acutalProductName))
                  		  fail=true;
                   	 } else if(k==2){
                   		 if(!compareText(product_2, acutalProductName))
                         		  fail=true; 
                   	 }
                 	 
                 }
            }

                 

                 getObject("searchButton").click();

                 
            }
            
               
	
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "SKIP");
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "FAIL");
		}
		else
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "PASS");
		
		skip=false;
		fail=false;
		

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "FAIL");
	
	}
	
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(suite_shop_xls, this.getClass().getSimpleName());
		
	}
	
	
}
