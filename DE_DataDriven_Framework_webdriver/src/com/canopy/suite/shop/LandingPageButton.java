package com.canopy.suite.shop;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.suite.shop.TestSuiteBase;
import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class LandingPageButton extends TestSuiteBase{

	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_shop_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_shop_xls, this.getClass().getSimpleName());
	}
	
	
	@Test(dataProvider="getTestData")
	public void testLandingPageButton() throws IOException{
		
		
		// test the runmode of current dataset
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			skip=true;
			throw new SkipException("Runmode for test set data set to no "+count);
		}

		
		System.out.println("Executing Test Case LandingPageButton");
		
		
		openBrowser();
		driver.get(CONFIG.getProperty("testSiteUrlName"));
		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\Authentication.exe");
		driver.get(CONFIG.getProperty("testSiteUrlName"));
		
		
		if(!checkElementPresence("login")){
			fail=true;
			APP_LOGS.debug("Login button is not avilable");
			return;
		}
		
		getObject("login").click();
		if(! compareText("Sign in",driver.findElement(By.xpath(OR.getProperty("signText"))).getText())){
     	   fail=true;
     	   APP_LOGS.debug("Login Page doesnot Open");
        }
		
		
		if(!checkElementPresence("registeration")){
			fail=true;
			APP_LOGS.debug("Registeration button is not avilable");
			return;
		}
		getObject("registeration").click();
		
		if(! compareText("Create new account",driver.findElement(By.xpath(OR.getProperty("createNewAccount"))).getText())){
     	   fail=true;
     	   APP_LOGS.debug("Registeration Page doesnot Open");
        }
		
		
	}
	
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "SKIP");
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "FAIL");
		}
		else
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "PASS");
		
		skip=false;
		fail=false;
		

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "FAIL");
	
	}
	
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(suite_shop_xls, this.getClass().getSimpleName());
		
	}
	
}
