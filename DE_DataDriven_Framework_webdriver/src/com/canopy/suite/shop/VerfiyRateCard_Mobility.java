package com.canopy.suite.shop;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerfiyRateCard_Mobility extends TestSuiteBase{
	
	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_shop_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_shop_xls, this.getClass().getSimpleName());
	}
	
	
	
	@Test(dataProvider="getTestData")
	public void VerfiyRateCard( String Product,
			               String rowNumber,
			               String support_capacity,
			               String duration_12,
			               String duration_24,
			               String duration_36
			) throws IOException, InterruptedException{
		
		// test the runmode of current dataset
				count++;
				if(!runmodes[count].equalsIgnoreCase("Y")){
					skip=true;
					throw new SkipException("Runmode for test set data set to no "+count);
				}
		
		System.out.println("Executing Test Case VerfiyRateCard Mobility");
		
		openBrowser();
        if(!isURLOpened){
        	openURL();
        }
       
		driver.get(CONFIG.getProperty("testSiteUrlName"));
		getObject("search_input").sendKeys(Product);
	    getObject("searchButton").click();
	    WebElement result_grid= driver.findElement(By.className("holder"));
	    result_grid.findElement(By.tagName("a")).click();
        Thread.sleep(10000L);
        driver.findElement(By.linkText("Rate card")).click();
        String actualRateCardHeaderText=getObject("rate_card_header").getText();
        if(!compareText("Price Information", actualRateCardHeaderText)){
        	fail=true;
        	APP_LOGS.debug("Rate Card Popup doesnot open");
        	return;
        }
		
		int rows= driver.findElements(By.xpath(OR.getProperty("mobility_rate_card_row"))).size();
		int cols= driver.findElements(By.xpath(OR.getProperty("mobility_rate_card_col"))).size();
		String support_type=getObject("mobility_suppport_type").getText();
		if(support_type.equals("Silver (Support Type)")){
		
			for(int j=1;j<=cols;j++){
				
				String actualRateCardText=driver.findElement(By.xpath("//*[@id='display_pricing_info']/div/div/div[2]/table/tbody/tr["+Integer.parseInt(rowNumber)+"]/td["+j+"]")).getText();
                if(j==1){
                               
                            
				             if(!compareText(String.valueOf(support_capacity),   actualRateCardText)){
					             fail=true;
					             APP_LOGS.debug("Support Capacity doesnot match for "+support_capacity);
				              }
                }else if(j==2){
                
                	if(!compareText(duration_12.trim().replace(",","."), actualRateCardText)){
			             fail=true;
			             APP_LOGS.debug("duration 12 price doesnot match for "+duration_12);
		              }
                	
                }else if(j==3){
                	
                	if(!compareText(duration_24.trim().replace(",","."), actualRateCardText)){
			             fail=true;
			             APP_LOGS.debug("duration 24 price  doesnot match for "+duration_12);
		              }
                	
                }else if(j==4){
                	
                	if(!compareText(duration_36.trim().replace(",","."), actualRateCardText)){
			             fail=true;
			             APP_LOGS.debug("duration 36 price doesnot match for "+duration_36);
		              }
		              
                }
               
			}
			
		}
		
	
		getObject("rate_card_close").click();
		
}
            
               
	
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "SKIP");
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "FAIL");
		}
		else
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "PASS");
		
		skip=false;
		fail=false;
		

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "FAIL");
	
	}
	
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(suite_shop_xls, this.getClass().getSimpleName());
		
	}
	
	
}
