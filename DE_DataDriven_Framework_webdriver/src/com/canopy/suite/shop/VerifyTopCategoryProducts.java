package com.canopy.suite.shop;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyTopCategoryProducts extends TestSuiteBase{
	
	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_shop_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_shop_xls, this.getClass().getSimpleName());
	}
	
	@Test(dataProvider="getTestData")
	public void testTopCategoryProducts() throws IOException, InterruptedException{
		
		// test the runmode of current dataset
				count++;
				if(!runmodes[count].equalsIgnoreCase("Y")){
					skip=true;
					throw new SkipException("Runmode for test set data set to no "+count);
				}
		
		System.out.println("Executing Test Case testTopCategoryProducts");
		

		openBrowser();
		driver.get(CONFIG.getProperty("testSiteUrlName"));
		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\Authentication.exe");
       
        WebElement topProductCategory= getObject("top_product_category_box");
        int totalLinks = topProductCategory.findElements(By.xpath(OR.getProperty("top_products_link"))).size();

        if(!compareNumbers(3, totalLinks)){
        	fail=true;
        	APP_LOGS.debug("Number of Links expected is not matching");
        }
        
        
        for(int i=0;i<=totalLinks-1;i++){
        	 String expectedProductName=topProductCategory.findElement(By.xpath(OR.getProperty("top_product_category_part1")+(i+1)+OR.getProperty("top_product_category_part2"))).getAttribute("data-name");
        	 topProductCategory.findElement(By.xpath(OR.getProperty("top_product_category_part1")+(i+1)+OR.getProperty("top_product_category_part2"))).click();
        	 String actualProductName=getObject("productName").getText();
        	 if(!compareText(expectedProductName, actualProductName)){
        		 fail=true;
        		 APP_LOGS.debug("Product Name doesnot matches with Name Present on Product Description Page");
        	 }
             driver.navigate().back();
             topProductCategory= getObject("top_product_category_box");
             totalLinks = topProductCategory.findElements(By.xpath(OR.getProperty("top_products_link"))).size();

        }
        
        
        getObject("top_product_pagination").click();
         topProductCategory= getObject("top_product_category_box");
         totalLinks = topProductCategory.findElements(By.xpath(OR.getProperty("top_products_link"))).size();
        if(!compareNumbers(1, totalLinks)){
        	fail=true;
        	APP_LOGS.debug("Number of Links expected is not matching");
        }
        
        

       	 String expectedProductName=topProductCategory.findElement(By.xpath(OR.getProperty("last_top_product_category"))).getAttribute("data-name");
       	 topProductCategory.findElement(By.xpath(OR.getProperty("last_top_product_category"))).click();
       	 String actualProductName=getObject("productName").getText();
       	 if(!compareText(expectedProductName, actualProductName)){
       		 fail=true;
       		 APP_LOGS.debug("Product Name doesnot matches with Name Present on Product Description Page");
       	 }
            driver.navigate().back();
           
       
	
	}
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "SKIP");
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "FAIL");
		}
		else
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "PASS");
		
		skip=false;
		fail=false;
		

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "FAIL");
	
	}
	
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(suite_shop_xls, this.getClass().getSimpleName());
		
	}
	
	
}
