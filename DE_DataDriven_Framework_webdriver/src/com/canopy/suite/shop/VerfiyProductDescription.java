package com.canopy.suite.shop;


import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerfiyProductDescription extends TestSuiteBase{
	String runmodes[]=null;
	static int count=-1;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
		@BeforeTest
		public void checkTestSkip(){
			
			if(!TestUtil.isTestCaseRunnable(suite_shop_xls,this.getClass().getSimpleName())){
				APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
				throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
			}
			runmodes=TestUtil.getDataSetRunmodes(suite_shop_xls, this.getClass().getSimpleName());
		}
	
	
	@Test   (dataProvider="getTestData")
	public void testVerfiyProductDescription( String Product ,
			                                  String long_description ,
			                                  String Main_feature_desc ,
			                                  String Benefits_for_IT_desc ,
			                                  String Benefits_for_the_business_desc ,
			                                  String Problems_Solved_desc
) throws InterruptedException, IOException{
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		APP_LOGS.debug(" Executing testVerfiyProductDescription");
		//APP_LOGS.debug(productName +" -- "+name +" -- "+capacity +" -- "+duration );	
		
		//sessionData.put("product_"+count, productName);
		//WebDriver Code

		openBrowser();
		driver.get(CONFIG.getProperty("testSiteUrlName"));
		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\Authentication.exe");
        getObject("homelink").click();
        getObject("search_input").sendKeys(Product);
        getObject("searchButton").click();
        WebElement result_grid= driver.findElement(By.className("holder"));
        result_grid.findElement(By.tagName("a")).click();
        Thread.sleep(10000L);
        String desc =getObject("long_desc").getText();
		//System.out.println(desc);
		String Main_feature = getObject("Main_feature").getText();
		
		String Benefits_for_IT = getObject("Benefits_for_IT").getText();
		
		String Benefits_for_the_business= getObject("Benefits_for_the_business").getText();
		String Problems_Solved =getObject("Problems_Solved").getText();
		//System.out.println(Problems_Solved);
		//System.out.println(Benefits_for_the_business);
		//System.out.println(Benefits_for_IT);
		
		//System.out.println(Main_feature);
		
		if(!compareText(long_description, desc)){
			fail=true;
			APP_LOGS.debug("description doesnot match");
			System.out.println("description doesnot match -- "+desc);
		}
		
		
	//	System.out.println(driver.findElement(By.xpath("//*[@id='main']/div/div/div[2]/div[1]/div/div[1]/div/div[2]/div/div")).getText().contains("Secure access, encryption, data back up and remote wipe capability Secure access to enterprise mail, critical business applications and data, data encryption for on device data, centrally enforced security policies Remote data backup, restore, and wipe-off, if device is lost, stolen or decommissioned Non-disruptive over the Air (OTA) deployment capabilities Deliver fixes, upgrades and refreshes to mobile users in the field with Over-the-Air deployment capabilities � automatically as scheduled or upon request Add, update or remove applications, data and content without the users' involvement Single console device administration Single administrative console to centrally manage, secure and deploy mobile data, applications and devices Information about who owns what, what apps are deployed on which device, which devices are active, inactive and accessing what data and apps are readily visible"));
		
		if(!compareText(Main_feature_desc, Main_feature)){
			fail=true;
			APP_LOGS.debug("Main_feature doesnot match");
			System.out.println("Main_feature doesnot match -- "+Main_feature);
		}
	
		if(!compareText(Benefits_for_IT_desc, Benefits_for_IT)){
			fail=true;
			APP_LOGS.debug("Benefits_for_IT doesnot match");
			System.out.println("Benefits_for_IT doesnot match -- "+Benefits_for_IT);
		}
		
		if(!compareText(Benefits_for_the_business_desc, Benefits_for_the_business)){
			fail=true;
			APP_LOGS.debug("Benefits_for_the_business doesnot match");
			System.out.println("Benefits_for_the_business doesnot match -- "+Benefits_for_the_business);
		}
				
		
		if(!compareText(Problems_Solved_desc, Problems_Solved)){
			fail=true;
			APP_LOGS.debug("Problems_Solved doesnot match");
			System.out.println("Problems_Solved doesnot match -- "+Problems_Solved);
		}

		
	}
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(suite_shop_xls, this.getClass().getSimpleName()) ;
	}
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "SKIP");
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "FAIL");
		}
		else
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "PASS");
		
		skip=false;
		fail=false;
		

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "FAIL");
	
	}
	

}
