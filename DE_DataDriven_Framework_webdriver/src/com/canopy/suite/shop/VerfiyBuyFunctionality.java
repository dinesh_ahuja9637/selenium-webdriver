package com.canopy.suite.shop;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerfiyBuyFunctionality extends TestSuiteBase{
	
	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_shop_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_shop_xls, this.getClass().getSimpleName());
	}
	
	@Test(dataProvider="getTestData")
	public void testBuyFunctionality( 
			               String userName,
			               String passWord,
			               String Product,
			               String type
			               
			) throws IOException, InterruptedException{
		
		// test the runmode of current dataset
				count++;
				if(!runmodes[count].equalsIgnoreCase("Y")){
					skip=true;
					throw new SkipException("Runmode for test set data set to no "+count);
				}
		
		System.out.println("Executing Test Case testBuyFunctionality ");
		
		openBrowser();
        if(!isURLOpened){
        	openURL();
        }
       
		driver.get(CONFIG.getProperty("testSiteUrlName"));
		
	    
		
        getObject("login").click();
        Thread.sleep(10000L);
       /* 
        if(! compareText("Sign in",driver.findElement(By.xpath(OR.getProperty("signText"))).getText())){
      	   fail=true;
      	   APP_LOGS.debug("Login Page doesnot Open");
      	   return;
         }
        */
        getObject("userName").sendKeys(userName);
        getObject("password").sendKeys(passWord);
        
       if(!checkElementPresence("loginSumbit")){
    	   fail=true;
    	   APP_LOGS.debug("Login Submit button is not avilable");
    	   return;
       }
        
		
        getObject("loginSumbit").click();
        	
        if(!checkElementPresence("myDashboard")){
        		fail=true;
        		APP_LOGS.debug("My Dashboard Links is not avilable");
        		return ;
        	}
        
	    getObject("mycart").click();
        String actualEmptyText= getObject("emptyCart").getText();
        if(!compareText("Your cart is empty!", actualEmptyText)){
        	fail=true;
        	APP_LOGS.debug("Cart is not Empty");
        }
        
       String actualCheckoutext= getObject("checkout").getText();
       if(!compareText("Check out", actualCheckoutext)){
       	fail=true;
       	APP_LOGS.debug("Check out button is not avilable");
       	
       }
        

       String actualTotalAmount = getObject("topTotalAmmount").getText();
       String [] amount =actualTotalAmount.split("�");
       if(!compareText("0.00", amount[1])){
          	fail=true;
          	APP_LOGS.debug("Check out button is not avilable");
          	
          }
	   getObject("homelink").click();	
		
		
	     getObject("search_input").sendKeys(Product);
	     getObject("searchButton").click();
   		if(type.equals("buy")){
			
			
			String BuyButton=getObject("buyButton").getText();
		    if(!compareText("sylius.add_to_cart", BuyButton)){
		    	fail=true;
		    	APP_LOGS.debug("Buy button is not avilable");
		    	return;
		    }
		    
		    getObject("buyButton").click();
		    
		    Thread.sleep(1000L);
		    
		    String actualAddedText =getObject("addToCartMessage").getText();
		    if(!compareText("Added!", actualAddedText)){
		    	fail=true;
		    	APP_LOGS.debug("Not able to add product to cart");
		    	return;
		    }
		    
		    getObject("goToCartLink").click();
		    
		    String actualProductHeaderCart=getObject("cartProductHeader").getText();
		    if(!compareText(Product, actualProductHeaderCart)){
		    	fail=true;
		    	APP_LOGS.debug("Product is not added in cart");
		    	return;
		    }
		    
			
			driver.findElement(By.linkText("Remove from Cart")).click();
			
			Alert al = driver.switchTo().alert();
			al.accept();
			Thread.sleep(10000L);
			
			String actualRemovedText =getObject("removedProductCart").getText();
			
			 if(!compareText("An item has been removed from cart", actualRemovedText)){
			    	fail=true;
			    	APP_LOGS.debug("Product is not removed from cart");
			    	return;
			    }
			    
			 getObject("logout").click();
		    
		} else if(type.equals("contactSales")){
			
			getObject("ContactSales").click();
			Thread.sleep(10000L);
			String actualContactSalesText =getObject("ContactSales").getAttribute("class").trim();
			
			
			 if(!compareText("contact-sales  basket-submit", actualContactSalesText)){
			    	fail=true;
			    	APP_LOGS.debug("Contact Sales button is not avilable");
			    	getObject("logout").click();
			    	return;
			    }
			 
			 getObject("logout").click();
			
		}
		

}
            
               
	
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "SKIP");
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "FAIL");
		}
		else
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "PASS");
		
		skip=false;
		fail=false;
		

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "FAIL");
	
	}
	
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(suite_shop_xls, this.getClass().getSimpleName());
		
	}
	
	
}
