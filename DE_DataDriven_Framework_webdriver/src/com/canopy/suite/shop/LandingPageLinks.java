package com.canopy.suite.shop;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class LandingPageLinks extends TestSuiteBase{
	
	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_shop_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_shop_xls, this.getClass().getSimpleName());
	}
	
	@Test(dataProvider="getTestData")
	public void testLandingPageLinks() throws IOException, InterruptedException{
		
		// test the runmode of current dataset
				count++;
				if(!runmodes[count].equalsIgnoreCase("Y")){
					skip=true;
					throw new SkipException("Runmode for test set data set to no "+count);
				}
		
		System.out.println("Executing Test Case testLandingPageLinks");
		

		openBrowser();
		driver.get(CONFIG.getProperty("testSiteUrlName"));
		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\Authentication.exe");

        if(!checkElementPresence("topMenu_left")){
        	fail=true;
        	APP_LOGS.debug("Top Menu Is not avilable");
        	return;
        }
        WebElement topMenu =getObject("topMenu_left");
        int count =topMenu.findElements(By.tagName("a")).size();

        if(!compareNumbers(2, count)){
        	fail=true;
        	APP_LOGS.debug("Expected Number of Links are not avilable on Top Menu");
        	
        }
       
        
        
        for(int i=0 ;i<=count-1;i++){
        	topMenu.findElement(By.xpath(OR.getProperty("topMenu_firstPart")+(i+1)+OR.getProperty("topMenu_secondPart"))).click();
           if(! compareTitle("Canopy Enterprise Store")){
        	   fail=true;
           }
        	topMenu =getObject("topMenu_left");
        	count =topMenu.findElements(By.tagName("a")).size();
        }
       
        Set<String> windIds= driver.getWindowHandles();
        Iterator<String> it= windIds.iterator();
        String homepage= it.next();
        String firstPopup=it.next();
        driver.switchTo().window(firstPopup);
        if(!compareTitle("Canopy")){
    	   fail=true;
       }
        driver.close();
        driver.switchTo().window(homepage);
        driver.close();
       
	}
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "SKIP");
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "FAIL");
		}
		else
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "PASS");
		
		skip=false;
		fail=false;
		

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "FAIL");
	
	}
	
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(suite_shop_xls, this.getClass().getSimpleName());
		
	}
	
	
}
