package com.canopy.suite.shop;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerifyLogin extends TestSuiteBase{
	
	String runmodes[]=null;
	static int count=-1;
	//static boolean pass=false;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	// Runmode of test case in a suite
	@BeforeTest
	public void checkTestSkip(){
		
		if(!TestUtil.isTestCaseRunnable(suite_shop_xls,this.getClass().getSimpleName())){
			APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
			throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
		}
		// load the runmodes off the tests
		runmodes=TestUtil.getDataSetRunmodes(suite_shop_xls, this.getClass().getSimpleName());
	}
	
	@Test(dataProvider="getTestData")
	public void testLogin( String userName,
			               String passWord,
			               String expectedResult
			) throws IOException, InterruptedException{
		
		// test the runmode of current dataset
				count++;
				if(!runmodes[count].equalsIgnoreCase("Y")){
					skip=true;
					throw new SkipException("Runmode for test set data set to no "+count);
				}
		
		System.out.println("Executing Test Case testFilterByFunctionality");
		
		openBrowser();
        if(!isURLOpened){
        	openURL();
        }
       
		driver.get(CONFIG.getProperty("testSiteUrlName"));
		


		
        getObject("login").click();
        
        if(! compareText("Sign in",driver.findElement(By.xpath(OR.getProperty("signText"))).getText())){
      	   fail=true;
      	   APP_LOGS.debug("Login Page doesnot Open");
      	   return;
         }
        
        getObject("userName").sendKeys(userName);
        getObject("password").sendKeys(passWord);
        
       if(!checkElementPresence("loginSumbit")){
    	   fail=true;
    	   APP_LOGS.debug("Login Submit button is not avilable");
    	   return;
       }
        
       if(!checkElementPresence("createAccount")){
    	   fail=true;
    	   APP_LOGS.debug("create Account  button is not avilable");
       } 		
		
        getObject("loginSumbit").click();
        
        if(expectedResult.equalsIgnoreCase("PASS")){
        	
        	if(!checkElementPresence("logout")){
        		fail=true;
        		APP_LOGS.debug("Unable to login");
        		return;
        	}
        	
        	if(!checkElementPresence("myDashboard")){
        		fail=true;
        		APP_LOGS.debug("My Dashboard Links is not avilable");
        		
        	}
        	
        	getObject("logout").click();
        	
        } else if(expectedResult.equalsIgnoreCase("FAIL")){
        	
        	driver.get(CONFIG.getProperty("testSiteUrlName"));
        	
        	if(checkElementPresence("logout")){
        		fail=true;
        		APP_LOGS.debug("Able to login with incorrect credential");
        		return;
        	}
        	
        	if(checkElementPresence("myDashboard")){
        		fail=true;
        		APP_LOGS.debug("Able to login with incorrect credential");
        		
        	}
        	
        }
       
 }
            
               
	
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "SKIP");
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "FAIL");
		}
		else
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "PASS");
		
		skip=false;
		fail=false;
		

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "FAIL");
	
	}
	
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(suite_shop_xls, this.getClass().getSimpleName());
		
	}
	
	
}
