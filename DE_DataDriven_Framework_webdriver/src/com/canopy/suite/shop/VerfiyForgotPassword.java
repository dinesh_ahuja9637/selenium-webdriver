package com.canopy.suite.shop;


import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.canopy.util.ErrorUtil;
import com.canopy.util.TestUtil;

public class VerfiyForgotPassword extends TestSuiteBase{
	String runmodes[]=null;
	static int count=-1;
	static boolean fail=false;
	static boolean skip=false;
	static boolean isTestPass=true;
	static int  number=0;
	// Runmode of test case in a suite
		@BeforeTest
		public void checkTestSkip(){
			
			if(!TestUtil.isTestCaseRunnable(suite_shop_xls,this.getClass().getSimpleName())){
				APP_LOGS.debug("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//logs
				throw new SkipException("Skipping Test Case"+this.getClass().getSimpleName()+" as runmode set to NO");//reports
			}
			runmodes=TestUtil.getDataSetRunmodes(suite_shop_xls, this.getClass().getSimpleName());
		}
	
	
	@Test   (dataProvider="getTestData")
	public void testForgotPassword(
			                                  String userName ,
			                                  String password ,
			                                  String confirmedPassword,
			                                  String expectedResult
			                                   
			                                  
) throws InterruptedException, IOException{
		count++;
		if(!runmodes[count].equalsIgnoreCase("Y")){
			throw new SkipException("Runmode for test set data set to no "+count);
		}
		APP_LOGS.debug(" Executing testForgotPassword");
		//APP_LOGS.debug(productName +" -- "+name +" -- "+capacity +" -- "+duration );	
		
		//sessionData.put("product_"+count, productName);
		//WebDriver Code
		/*
		openBrowser();
		driver.get("https://store.canopy-cloud.com/");
        Runtime.getRuntime().exec(CONFIG.getProperty("authenticatePath"));
		driver.get("https://store.canopy-cloud.com/");
		*/
		openBrowser();
        if(!isURLOpened){
        	openURL();
        }
		driver.get(CONFIG.getProperty("testSiteUrlName"));
        getObject("login").click();
        driver.findElement(By.linkText("Forgot Your Password ?")).click();
 
        String actualText=driver.findElement(By.xpath(OR.getProperty("sumbit_reset_email_address_page"))).getText();
        
        if(!compareText("Canopy-Cloud Password Assistance", actualText)){
        	fail=true;
        	APP_LOGS.debug("Forgot Password Link is not working");
        	return;
        }

        getObject("forgot_password_email").sendKeys(userName);
        getObject("forgot_password_email_sumbit").click();
        
        String actualResetText=getObject("reset_password_message").getText();
        String expectedResetText="An email containing a link to reset your password has been sent to your registered email address. Please check your email address.";
        if(userName !=""){
        if(!compareText(expectedResetText, actualResetText)){
        	fail=true;
        	APP_LOGS.debug("Unable to send reset password Link");
        	return;
        }
      } else if (userName==""){

         if(!checkElementPresence("forgot_password_email_sumbit")){
        	 fail=true;
        	 APP_LOGS.debug("Reset Password is sent");
         }
    	  
    	  
      }
        
        driver.get("http://yopmail.com");
        getObject("yopmail_username").sendKeys(userName);
        Thread.sleep(5000L);
        
        if(number==0){
        getObject("yopmail_sumbit").click();
        number++;
        } else if (number > 0){
        	getObject("yopmail_sumbit_again").click();
        }
        int size =driver.findElements(By.tagName("iframe")).size();
        driver.switchTo().frame("ifmail");
        getObject("yopmail_email_body_reset_link").click();
        driver.switchTo().defaultContent();
        
        Set<String> windIds= driver.getWindowHandles();
        Iterator<String> it= windIds.iterator();

        String homepage= it.next();
        String firstPopup=it.next();

        driver.switchTo().window(firstPopup);
        Thread.sleep(10000L);
        
        getObject("reset_passowrd").sendKeys(password);
        getObject("reset_password_confirm").sendKeys(confirmedPassword);
        getObject("reset_password_sumbit").click();
       // getObject("password_reset_successfullly").getText();
        if(expectedResetText.equalsIgnoreCase("PASS")){
            String actualtext= getObject("marketPlace").getText();

        if(!compareText("Market Place", actualtext)){
        	fail=true;
        	APP_LOGS.debug("Password is not reset Successfully");
        	return;
        }
      }else if (expectedResetText.equalsIgnoreCase("FAIL")){

            if(checkElementPresence("marketPlace")){
            	fail=true;
            	APP_LOGS.debug("Able to reset password with blank or old password");
            }
    	  
      }

       driver.close();
       driver.switchTo().window(homepage);	 
        
        
	}
	
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData(suite_shop_xls, this.getClass().getSimpleName()) ;
	}
	
	@AfterMethod
	public void reportDataSetResult(){
		if(skip)
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "SKIP");
		else if(fail){
			isTestPass=false;
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "FAIL");
		}
		else
			TestUtil.reportDataSetResult(suite_shop_xls, this.getClass().getSimpleName(), count+2, "PASS");
		
		skip=false;
		fail=false;
		

	}
	
	@AfterTest
	public void reportTestResult(){
		if(isTestPass)
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(suite_shop_xls, "Test Cases", TestUtil.getRowNum(suite_shop_xls,this.getClass().getSimpleName()), "FAIL");
	
	}
	

}
