package test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestRateCard {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		 WebDriver driver = new FirefoxDriver();
         driver.get("https://store.canopy-cloud.com/");
         Runtime.getRuntime().exec("C:\\Users\\a585470\\Desktop\\CEP\\de-functional-automation\\DummyProject_webdriver\\DE_DataDriven_Framework_webdriver\\Authentication.exe");
         driver.findElement(By.xpath("//*[@id='main']/div[3]/div/div[1]/div[1]/div/div[1]/div/div[1]/div[3]/div[2]/h3/a")).click();
         Thread.sleep(10000L);
         driver.findElement(By.linkText("Rate card")).click();
         System.out.println(driver.findElement(By.xpath("//*[@id='display_pricing_info']/div/div/div[1]/h4")).getText());
		
		int rows= driver.findElements(By.xpath("//*[@id='display_pricing_info']/div/div/div[2]/table/tbody/tr")).size();
		int cols= driver.findElements(By.xpath("//*[@id='display_pricing_info']/div/div/div[2]/table/tbody/tr[3]/td")).size();
		
		System.out.println("Total Rows"+rows);
		System.out.println("Total Cols"+cols);
		
		for(int i=3;i<=rows;i++){
			for(int j=1;j<=cols;j++){
				System.out.println(driver.findElement(By.xpath("//*[@id='display_pricing_info']/div/div/div[2]/table/tbody/tr["+i+"]/td["+j+"]")).getText());
				
			}
			
		}
		
		driver.findElement(By.xpath("//*[@id='display_pricing_info']/div/div/div[3]/button")).click();
		
	}

}
