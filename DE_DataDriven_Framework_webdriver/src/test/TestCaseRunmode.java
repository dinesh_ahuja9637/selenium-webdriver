package test;

import com.canopy.util.Xls_Reader;

public class TestCaseRunmode {

	public static void main(String[] args) {

    Xls_Reader x = new Xls_Reader(System.getProperty("user.dir")+"\\src\\com\\canopy\\xls\\A suite.xlsx");
    System.out.println(isTestCaseRunnable(x, "TestCase_A1"));
		
	}

	
	public static boolean isTestCaseRunnable(Xls_Reader xls,String testCaseName){
		boolean isExecutable=false;
		
		for(int i=2;i<=xls.getRowCount("Test Cases");i++){
			
			if(xls.getCellData("Test Cases","TCID" , i).equalsIgnoreCase(testCaseName)){
				
				if(xls.getCellData("Test Cases", "Runmode", i).equalsIgnoreCase("Y")){
					isExecutable=true;
				}else{
					isExecutable=false;
				}
			}
		}
		
		xls=null;
		return isExecutable;
		
	}
	
	
	
}
