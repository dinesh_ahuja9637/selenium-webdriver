package test;

import com.canopy.util.Xls_Reader;

public class UpdateTest_Result {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Xls_Reader x = new Xls_Reader(System.getProperty("user.dir")+"\\src\\com\\canopy\\xls\\A suite.xlsx");
		reportDataSetResult(x, "TestCase_A1", 2, "Pass");
		
	}

	public static void reportDataSetResult(Xls_Reader xls,String testCaseName,int rowNum,String result){
		
		xls.setCellData(testCaseName, "Results", rowNum, result);
		
	}
	
	
}
