package test;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ForgotPassword {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {

		 WebDriver driver = new FirefoxDriver();
         driver.get("https://store.canopy-cloud.com/");
         Runtime.getRuntime().exec("C:\\Users\\a585470\\Desktop\\CEP\\de-functional-automation\\DummyProject_webdriver\\DE_DataDriven_Framework_webdriver\\Authentication.exe");
         driver.findElement(By.xpath("//*[@id='header']/div[2]/div/div/a[2]")).click();
         driver.findElement(By.linkText("Forgot Your Password ?")).click();
         System.out.println(driver.findElement(By.xpath("//*[@id='main']/div/div/div[1]/h1")).getText());
         driver.findElement(By.xpath("//*[@id='canopy_forgot_password_email']")).sendKeys("admin-user@yopmail.com");
         driver.findElement(By.xpath("//*[@id='canopy_forgot_password_submit']")).click();
         
         System.out.println(driver.findElement(By.xpath("//*[@id='page']/div[1]")).getText());
         
         
         driver.get("http://yopmail.com");
         driver.findElement(By.xpath("//*[@id='login']")).sendKeys("admin-user@yopmail.com");
         driver.findElement(By.xpath("//*[@id='f']/table/tbody/tr[1]/td[3]/input")).click();
         
         int size =driver.findElements(By.tagName("iframe")).size();
         System.out.println(size);
         driver.switchTo().frame("ifmail");
         
        // System.out.println(driver.findElement(By.xpath("//*[@id='mailhaut']/div[1]")).getText());
         
         driver.findElement(By.xpath("//*[@id='mailmillieu']/div[2]/table/tbody/tr/td/table/tbody/tr[2]/td/a")).click();
         driver.switchTo().defaultContent();
         
         Set<String> windIds= driver.getWindowHandles();
         Iterator<String> it= windIds.iterator();


         
         String homepage= it.next();
         String firstPopup=it.next();

         driver.switchTo().window(firstPopup);
         Thread.sleep(10000L);
         
         driver.findElement(By.xpath("//*[@id='canopy_reset_password_password_first']")).sendKeys("NomqPassword@123$");
         driver.findElement(By.xpath("//*[@id='canopy_reset_password_password_second']")).sendKeys("NomqPassword@123$");
         
         driver.findElement(By.xpath("//*[@id='canopy_reset_password_submit']")).click();

         driver.close();
         driver.switchTo().window(homepage);
         
         driver.get("http://yopmail.com");
         driver.findElement(By.xpath("//*[@id='login']")).sendKeys("admin-user@yopmail.com");
         driver.findElement(By.xpath("html/body/center/div/div/div[3]/table[3]/tbody/tr/td[1]/table/tbody/tr[3]/td/div[1]/form/table/tbody/tr[1]/td[4]/input")).click();
         
	}

}
