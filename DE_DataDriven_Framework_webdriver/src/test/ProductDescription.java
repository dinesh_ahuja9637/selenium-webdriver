package test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ProductDescription {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {

         WebDriver driver = new FirefoxDriver();
         driver.get("https://store.canopy-cloud.com/");
         Runtime.getRuntime().exec("C:\\Users\\a585470\\Desktop\\CEP\\de-functional-automation\\DummyProject_webdriver\\DE_DataDriven_Framework_webdriver\\Authentication.exe");
         driver.findElement(By.linkText("Canopy Cloud Fabric")).click();
         Thread.sleep(5000L);
         System.out.println("Long Description");
         System.out.println(driver.findElement(By.xpath("//*[@id='main']/div/div/div[2]/div[1]/section/div[2]/p")).getText());
         System.out.println("Main Feature Description");
         System.out.println(driver.findElement(By.xpath("//*[@id='main']/div/div/div[2]/div[1]/div/div[1]/div/div[2]/div/div")).getText());
         System.out.println("Benefits for IT");
         System.out.println(driver.findElement(By.xpath("//*[@id='main']/div/div/div[2]/div[1]/div/div[1]/div/div[4]/div/div")).getText());
         System.out.println("Problems Solved");
         System.out.println(driver.findElement(By.xpath("//*[@id='main']/div/div/div[2]/div[1]/div/div[1]/div/div[1]/div/div")).getText());
		 System.out.println("Benefits for the business");
		 System.out.println(driver.findElement(By.xpath("//*[@id='main']/div/div/div[2]/div[1]/div/div[1]/div/div[3]/div/div")).getText());
	}

}
