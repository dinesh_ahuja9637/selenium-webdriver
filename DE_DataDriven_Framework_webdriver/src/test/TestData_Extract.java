package test;

import com.canopy.util.Xls_Reader;

public class TestData_Extract {

	public static void main(String[] args) {

    Xls_Reader x = new Xls_Reader(System.getProperty("user.dir")+"\\src\\com\\canopy\\xls\\A suite.xlsx");
		getData(x, "TestCase_A1");
		
	}

	
	public static Object[][] getData(Xls_Reader xls,String testCaseName){
		
		if(!xls.isSheetExist(testCaseName)){
			xls=null;
			return new Object[1][0];
			
		}
		
		int rows=xls.getRowCount(testCaseName);
		int cols=xls.getColumnCount(testCaseName);
		Object [][] data= new Object[rows-1][cols-3];
		
		for(int rowNum=2;rowNum<=rows;rowNum++){
			
			for(int colNum=0;colNum<cols-3;colNum++){
				
				data[rowNum-2][colNum]=xls.getCellData(testCaseName, colNum, rowNum);
			}
		}
		
		xls=null;
		return data;
		
	}
	
	
	
}
