package test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestAddtoCart {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {

		WebDriver driver = new FirefoxDriver();
        driver.get("https://store.canopy-cloud.com/");
        Runtime.getRuntime().exec("C:\\Users\\a585470\\Desktop\\CEP\\de-functional-automation\\DummyProject_webdriver\\DE_DataDriven_Framework_webdriver\\Authentication.exe");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id='header']/div[2]/div/div/a[2]")).click();
        driver.findElement(By.xpath("//*[@id='username']")).sendKeys("check1@yopmail.com");
        driver.findElement(By.xpath("//*[@id='password']")).sendKeys("Nature@123$");
        driver.findElement(By.xpath("//*[@id='login_form_submit']")).click();
        Thread.sleep(5000L);
        
		driver.findElement(By.xpath("//*[@id='header']/div[2]/div/div/a[1]")).click();
		System.out.println(driver.findElement(By.xpath("//*[@id='main']/section[2]/div[2]/p")).getText());
		
		System.out.println(driver.findElement(By.xpath("//*[@id='main']/section[2]/div[1]/div/button")).getText());
		
		System.out.println(driver.findElement(By.xpath("//*[@id='total-top']/span")).getText());
		
		String totalCart=driver.findElement(By.xpath("//*[@id='total-top']/span")).getText();
		
		String [] amount =totalCart.split("�");
		
		System.out.println(amount[1]);
		
		driver.findElement(By.xpath("//*[@id='nav']/ul/li[1]/a")).click();
		
		driver.findElement(By.xpath("//*[@id='main']/div[2]/form/fieldset/div[1]/input")).sendKeys("Canopy Enterprise Mobile Secure");
		driver.findElement(By.xpath("//*[@id='main']/div[2]/form/fieldset/input")).click();
		
		
		int size= driver.findElements(By.xpath("//*[@id='main']/div[3]/div/div/div/div[2]/div/div[2]/ul/li/form/button")).size();
		System.out.println(size);
		
		System.out.println(driver.findElement(By.xpath("//*[@id='main']/div[3]/div/div/div/div[2]/div/div[2]/ul/li/form/button")).getText());

		driver.findElement(By.xpath("//*[@id='main']/div[3]/div/div/div/div[2]/div/div[2]/ul/li/form/button")).click();
		
        Thread.sleep(1000L);
		
		System.out.println(driver.findElement(By.xpath("//div[@class='tooltip success fade right in']/div/strong")).getText());
		
		driver.findElement(By.xpath("//div[@class='tooltip success fade right in']/div[2]/a")).click();
		
		System.out.println(driver.findElement(By.xpath("//*[@id='CartForm']/div[2]/div[1]/div/div[1]/div[2]/div/h2")).getText());
		
		driver.findElement(By.linkText("Remove from Cart")).click();
		
		Alert al = driver.switchTo().alert();
		al.accept();
		Thread.sleep(10000L);
		System.out.println(driver.findElement(By.xpath("//*[@id='page']/div[1]")).getText());
		
	}

}
