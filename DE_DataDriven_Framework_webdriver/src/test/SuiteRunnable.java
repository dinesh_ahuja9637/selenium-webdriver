package test;

import com.canopy.util.Xls_Reader;

public class SuiteRunnable {

	public static void main(String[] args) {
      Xls_Reader x= new Xls_Reader(System.getProperty("user.dir")+"\\src\\com\\canopy\\xls\\Suite.xlsx");
      System.out.println(isSuiteRunnable(x, "A suite"));
      System.out.println(isSuiteRunnable(x, "B suite"));
      System.out.println(isSuiteRunnable(x, "C suite"));
	}

	public static boolean isSuiteRunnable(Xls_Reader xls,String suiteName){
		
		boolean isExecutable=false;
		
		for(int i=2;i<=xls.getRowCount("Test Suite");i++){
			String suite = xls.getCellData("Test Suite", "TSID", i);
			String runmode=xls.getCellData("Test Suite", "Runmode", i);
			
			if(xls.getCellData("Test Suite", "TSID", i).equalsIgnoreCase(suiteName)){
				if(xls.getCellData("Test Suite", "Runmode", i).equalsIgnoreCase("Y")){
					isExecutable=true;
				} else {
					isExecutable=false;
				}
			}
			
		}
		
		xls=null;
		return isExecutable;
		
	}
	
	
	
	
}
